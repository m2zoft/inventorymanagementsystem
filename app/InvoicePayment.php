<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicePayment extends Model
{

    protected $table = "invoice_payments";

    protected static $invoiceItem = 'App\InvoiceItem';

    protected static $account = 'App\Account';

    protected $fillable = [
        'transaction_number',
        'invoice_item_id',
        'account_id',
        'paid_at',
        'amount',
        'description',
    ];

    protected $dates = ['created_at', 'updated_at', 'paid_at'];

    public function invoice_item()
    {
    	return $this->hasOne(static::$invoiceItem, 'id', 'invoice_item_id');
    }

    public function account()
    {
        return $this->hasOne(static::$account, 'id', 'account_id');
    }
       
}
