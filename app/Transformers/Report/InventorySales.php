<?php namespace App\Transformers\Report;

use App\Transformers\Transformer;
use App\Transformers\Customer\CustomerTransformer;
use App\Transformers\Transaction\TransactionHistoryTransformer;


class InventorySales extends Transformer
{
	protected $customer, $transaction_history;

	function __construct()
	{
		$this->customer = new CustomerTransformer;

		$this->transaction_history = new TransactionHistoryTransformer;
	}

	public function transform($item)
	{
		return [

			'base_unit'		=> $item['base_unit'],

			'base_unit_id'	=> $item['base_unit_id'],

			'discount'		=> $item['discount'],

			'item_name'		=> $item['item_name'],

			'item_variation'	=> $item['item_variation'],

            'inventory'	=> $item['inventory'],

			'item_variation_id'	=> $item['item_variation_id'],

			'item_variation_name'	=> $item['item_variation_name'],

			'quantity'		=> $item['quantity'],

			'total_price'	=> $item['total_price'],

			'element_class' => $item['element_class'],

			'readable' => [

			    'total_beginning_stock' => number_format(($item['inventory']['stocks'] + $item['quantity'])),

				'total_quantity' 	=> number_format($item['quantity']),

                'total_remaining_stock' => number_format($item['inventory']['stocks']),

				'total_price'		=> number_format($item['total_price'], 2),

				'total_discount'	=> number_format($item['discount'], 2),

			],
		];
	}
}

