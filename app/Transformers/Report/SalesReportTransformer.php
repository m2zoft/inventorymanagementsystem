<?php namespace App\Transformers\Report;

use App\Transformers\Transformer;
use App\Transformers\Customer\CustomerTransformer;
use App\Transformers\Transaction\TransactionHistoryTransformer;


class SalesReportTransformer extends Transformer
{
	protected $customer, $transaction_history;

	function __construct(CustomerTransformer $customer, TransactionHistoryTransformer $transaction_history)
	{
		$this->customer = $customer;

		$this->transaction_history = $transaction_history;
	}

	public function transform($item)
	{
		return [
			'id' 				=> $item['id'],
			'account' 			=> $item['account'],
			'created_at' 		=> date('M d,Y H:i A', strtotime($item['created_at'])),
			'entries' 			=> $this->transaction_history->transformCollection($item['entries']->all()),
			'or_number' 		=> $item['or_number'],
			'to_customer' 		=> $this->customer->transform($item['to_customer']),
			'total_balance' 	=> $item['total_balance'],
			'total_items' 		=> $item['total_items'],
			'readable' => [
				'total_balance' 	=> number_format($item['total_balance'], 2),
				'transaction_date' 	=> date('M d,Y', strtotime($item['transaction_date']))
			],
			'transaction_number' => $item['transaction_number'],
			'transaction_type'	 => $item['transaction_type'],
			'element_class' 	 => $item['element_class'],
		];
	}
}