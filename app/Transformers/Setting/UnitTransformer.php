<?php namespace App\Transformers\Setting;

use App\Transformers\Transformer;

class UnitTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			
			'id' 		=> $item['id'],

			'code' 		=> $item['code'],

			'name' 		=> $item['name'],

		];
	}
}