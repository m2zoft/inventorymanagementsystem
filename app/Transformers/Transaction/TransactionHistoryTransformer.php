<?php namespace App\Transformers\Transaction;

use App\Transformers\Transformer;

use App\Transformers\Item\ItemVariationTransformer;

use App\Transformers\Setting\UnitTransformer;

class TransactionHistoryTransformer extends Transformer
{

	protected $item_variation, $unit;

	function __construct()
	{
		$this->item_variation = new ItemVariationTransformer;

		$this->unit = new UnitTransformer;
	}

	public function transform($item)
	{

		return [

			'id' 				=> $item['id'],

			'created_at' 		=> date('M d,Y H:i A', strtotime($item['created_at'])),

			'discount' 			=> $item['discount'],

			'item_variation' 	=> $this->item_variation->transform($item['item_variation']),

			'base_unit'		 	=> $this->unit->transform($item['base_unit']),

			'transaction_id' 	=> $item['transaction_id'],

			'price' 			=> $item['price'],

			'quantity' 			=> $item['quantity'],

			'total_price' 		=> $item['total_price'],

			'base_price' 		=> $item['base_price'],

            'item_name'         => $item['item_name'],

            'item_variation_name' => $item['item_variation_name'],

			'readable' 			=> [

				'price' 		=> number_format($item['price'], 2),

				'total_price' 	=> number_format($item['total_price'], 2),

				'base_price' 	=> number_format($item['base_price'], 2),

				'net_sales' 	=> isset($item['net_sales']) ? number_format($item['net_sales'], 2) : 0,

			]

		];
	}
}