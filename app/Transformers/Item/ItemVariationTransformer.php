<?php namespace App\Transformers\Item;

use App\Transformers\Transformer;

use App\Transformers\Setting\UnitTransformer;

use App\Transformers\Item\ItemTransformer;

class ItemVariationTransformer extends Transformer
{

	protected $unit, $item_transformer;

	function __construct()
	{
		$this->unit = new UnitTransformer;

		$this->item_transformer = new ItemTransformer;
	}

	public function transform($item)
	{
		return [
			
			'id' 				=> $item['id'],

			'created_at' 		=> date('M d,Y H:i A', strtotime($item['created_at'])),

			'base_price' 		=> $item['base_price'],

			'selling_price' 	=> $item['selling_price'],

			'base_unit' 		=> $this->unit->transform($item['base_unit']),

			'category_id' 		=> $item['category_id'],

			'group_id' 			=> $item['group_id'],

			'brand_id'			=> $item['brand_id'],

			'item'				=> $this->item_transformer->transform($item['item']),

			'name' 				=> $item['name'],

			'price_margin' 		=> $item['price_margin'],

		];
	}
}