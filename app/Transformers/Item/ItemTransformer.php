<?php namespace App\Transformers\Item;

use App\Transformers\Transformer;

class ItemTransformer extends Transformer
{

	public function transform($item)
	{
		return [
			
			'id' 				=> $item['id'],

			'name' 				=> $item['name'],
			
		];
	}
}