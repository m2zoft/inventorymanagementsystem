<?php namespace App\Transformers\Customer;

use App\Transformers\Transformer;

class CustomerTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'id' => $item['id'],
			'first_name' => $item['first_name'],
			'last_name' => $item['last_name'],
			'mobile_number' => $item['mobile_number'],
			'address' => $item['address'],
			'fullname' => $item['first_name']. ' ' . $item['last_name'],
		];
	}
}