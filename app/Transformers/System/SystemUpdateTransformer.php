<?php namespace App\Transformers\System;

use App\Transformers\Transformer;

class SystemUpdateTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			
			'id' => $item['id'],

			'title' 	=> $item['title'],

			'description' => $item['description'],

			'type' 		  => $item['type'],

			'status' 	  => $item['status'],

			'version' 	  => $item['version'],

			'created_at'  =>  date('M d,Y', strtotime($item['created_at'])),

		];
	}
}