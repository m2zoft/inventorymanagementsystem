<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemVariationHistory extends Model
{
    //
    protected $table = "item_variations_history";

    protected static $unit = 'App\Unit';

	protected $fillable = [
        'item_variation_id',
        'base_unit_id',
        'base_price',
        'selling_price',
        'price_margin',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['created_at', 'updated_at'];    
    
    public function base_unit()
    {
    	return $this->hasOne(static::$unit, 'id', 'base_unit_id');
    }

    // Variation Object
    public function scopeisExist($query, $variation)
    {
        return $query->where(function($q) use ($variation){
            $q->where('item_variation_id', $variation['id']);
            $q->where('base_price', $variation['base_price']);
            $q->where('selling_price', $variation['selling_price']);
        })->count();
    }
}
