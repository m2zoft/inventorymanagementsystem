<?php


namespace App\Http\Controllers\Report;


class Inventory extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.report.inventory');
    }
}