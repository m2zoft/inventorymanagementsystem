<?php

namespace App\Http\Controllers\Relationship;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Repositories\Repository;
use App\Http\Repositories\SupplierRepository;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new Repository(new SupplierRepository))->all();
    }

    public function create()
    {
        return (new Repository(new SupplierRepository(request())))->create();
    }

}
