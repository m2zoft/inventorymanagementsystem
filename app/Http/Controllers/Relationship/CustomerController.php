<?php

namespace App\Http\Controllers\Relationship;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Repositories\Repository;
use App\Http\Repositories\CustomerRepository;

use App\Http\Requests\CreateCustomerRequest;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new Repository(new CustomerRepository))->all();
    }

    public function paginate()
    {
        return (new Repository(new CustomerRepository))->all();
    }    

    public function create(CreateCustomerRequest $request)
    {
        return (new Repository(new CustomerRepository($request->all())))->create();
    }

}
