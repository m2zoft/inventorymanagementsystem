<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Repositories\Repository;
use App\Http\Repositories\InventoryRepository;

use App\Transaction;
use App\TransactionEntry;
use App\Inventory;
use App\ItemVariation;
use App\ItemVariationHistory;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new Repository(new InventoryRepository(request())))->paginate();
    }

}
