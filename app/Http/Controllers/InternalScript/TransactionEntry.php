<?php

namespace App\Http\Controllers\InternalScript;

use App\Http\Controllers\Controller;
use App\ItemVariation;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class TransactionEntry extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        Log::info('Transaction Entry Script Executed...');

        \App\TransactionEntry::whereNull('item_name')->chunk(100, function($transaction_entries){

            foreach ($transaction_entries as $transaction_entry) {
                DB::beginTransaction();
                try{
                    $item_variation_found = ItemVariation::find($transaction_entry->item_variation_id);

                    $item_name = $item_variation_found->item->name;
                    $item_variant_name = $item_variation_found->name;

                    $transaction_entry->item_name = $item_name;
                    $transaction_entry->item_variation_name = $item_variant_name;
                    $transaction_entry->save();

                    Log::info('Transaction Entry ID ('.$transaction_entry->id.'): Successfully Updated.');

                    DB::commit();
                }catch(\Exception $e){
                    Log::info('Transaction Entry ID ('.$transaction_entry->id.'): Failed to update.');
                    Log::info($e->getMessage() . ' - Line: ' . $e->getLine());
                    DB::rollback();
                }
            }

        });

        Log::info('Transaction Entry Script Terminated.');
    }
}
