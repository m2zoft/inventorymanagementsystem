<?php

namespace App\Http\Controllers\InternalScript;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan as ArtisanCommand;

class Artisan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function migrate()
    {
        ArtisanCommand::call('migrate');
        return 'php artisan migrate was executed';
    }

    public function processTransactionEntry()
    {
        ArtisanCommand::call('transaction-entry:migrate');
        return 'php artisan transaction-entry:migrate was executed';
    }

}
