<?php

namespace App\Http\Controllers\Sales;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Repositories\Repository;
use App\Http\Repositories\SalesRepository;

use App\Inventory;
use App\Transaction;
use App\TransactionEntry;

use App\Http\Requests\CreateOrderTransactionRequest;

use App\Http\Controllers\ApiRespondController;

class SalesController extends ApiRespondController
{

    public function paginateSalesOrder()
    {
        return (new SalesRepository(request()))->paginateSalesOrder();
    }

    public function createOrderTransaction(CreateOrderTransactionRequest $request)
    {

    	if (Transaction::findByOrNumber($request['or_number'])->count() > 0) {
    		return $this->respondUnprocessable('Your O.R. number is already exist.');
    	}

		$transaction_number = date('ymdhis');

		$total_debit 	= $request['discount'];
		$total_credit 	= $request['grand_total'];
		$total_balance 	= $request['grand_total'];

		// Create Transaction
		$transaction = Transaction::create([
		    'or_number'             => $request['or_number'],
		    'transaction_number'    => $transaction_number,
		    'account_id'            => 1, // Sales
		    'transaction_type_id'   => 2, // MIGRATED --> Changed it into Order
		    'to'                  	=> $request['customer_id'],
		    'total_debit'           => $total_debit,
		    'total_credit'          => $total_credit,
		    'total_balance'         => $total_balance,
		    'transaction_date'      => date('Y-m-d', strtotime($request['transaction_date'])),
		]);

		// Create Transaction Entries
		foreach ($request['items'] as $item) {

		    TransactionEntry::create([
		        'transaction_id'        => $transaction->id,
		        'item_variation_id'     => $item['item_variation_id'],
		        'base_unit_id'          => $item['base_unit']['id'],
		        'quantity'              => $item['quantity'],
		        'base_price'			=> $item['base_price'],
		        'price'                 => $item['selling_price'],
		        'discount' 				=> $item['discount'],
		        'total_price'           => $item['total_price'],
		    ]);

		    \Log::info('Total Stocks: ' . $item['stock'] .'-'. $item['quantity'] . '= '. number_format($item['stock'] - $item['quantity'], 2, '.', ''));
		    $total_stocks   =  $item['stock'] - $item['quantity'];

		    Inventory::where('id', $item['inventory_id'])->update(['stocks' => number_format($total_stocks, 2, '.', '')]);

		}


    }    

}
