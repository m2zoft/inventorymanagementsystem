<?php

namespace App\Http\Controllers\Api\Invoice;

use App\Invoice as Model;
use App\InvoiceItem as InvoiceItemModel;
use App\InvoiceHistory as InvoiceHistoryModel;
use App\InvoiceTotal as InvoiceTotalModel;
use App\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiRespondController;

class Invoice extends ApiRespondController
{

    protected $request, $search, $page, $rows, $offset; 

    function __construct(Request $request)
    {

        $this->request  = $request;

        $this->search   = isset($this->request['search']) ? $this->request['search'] : '';

        $this->page     = isset($this->request['page'])   ? $this->request['page']   : 1;

        $this->rows     = isset($this->request['rows'])  && $this->request['rows'] <= 50 ? $this->request['rows'] : 50;

        $this->offset   = ($this->page - 1) * $this->rows;

    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $total = Model::with(['customer','items','histories','totals'])

                ->whereHas('customer', function($query){ 

                    if ($this->search !== '') {
                        $query->where('first_name', 'LIKE', '%'.$this->search.'%');
                        $query->orWhere('last_name', 'LIKE', '%'.$this->search.'%');
                    }               

                })

                ->orWhere('invoice_number', 'LIKE', '%'.$this->search.'%')

                ->count();


        $data = Model::with(['customer','items' => function($query){
                    $query->with(['item_variation' => function($query){
                        $query->with(['item', 'base_unit']);
                    }]);
                },'histories','totals'])

                ->whereHas('customer', function($query){ 

                    if ($this->search !== '') {
                        $query->where('first_name', 'LIKE', '%'.$this->search.'%');
                        $query->orWhere('last_name', 'LIKE', '%'.$this->search.'%');
                    }               

                })

                ->orWhere('invoice_number', 'LIKE', '%'.$this->search.'%')

                ->skip($this->offset)

                ->take($this->rows)

                ->get();

        return $this->respondWithSuccessWithData('Request Granted', ['rows' => $data, 'total' => $total]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = 'INV-';

        // Generate Invoice Number
        $invoice_number =  $code . str_pad(is_null($request->invoice_number) ? Model::count() + 1 : $request->invoice_number,9,"0",STR_PAD_LEFT);

        $empty_items = 0;

        if ($request->customer_id <= 0) {
            return $this->respondUnprocessable('Select your Customer.');
        }

        if (count($request->items) > 0) {
            foreach ($request->items as $item) {
                $empty_items += $item['inventory_id'] <= 0 ? 1 : 0;
            }
        }

        if ($empty_items >= 1) {
            return $this->respondUnprocessable('One of the items has an incorrect data.');
        }


        // Create Invoice
        $invoice = new Model;
        $invoice->invoice_number = $invoice_number;
        $invoice->description = 'Sales Invoice';
        $invoice->invoiced_at = date('Y-m-d h:i:s',strtotime($request->transaction_date));
        $invoice->due_at      = date('Y-m-d h:i:s',strtotime($request->due_date));
        $invoice->amount      = $request->grand_total;
        $invoice->customer_id = $request->customer_id;
        $invoice->status_code = 'approved';
        $invoice->save();

        // History
        $invoice_history = new InvoiceHistoryModel;
        $invoice_history->invoice_id = $invoice->id;
        $invoice_history->description = 'Approved the Invoice';
        $invoice_history->status_code = 'approved';
        $invoice_history->save();

        // Create Items
        foreach ($request->items as $item) {
            $invoice_item = new InvoiceItemModel;
            $invoice_item->invoice_id           = $invoice->id;
            $invoice_item->item_variation_id    = $item['item_variation_id'];
            $invoice_item->name                 = $item['name'];
            $invoice_item->description          = '';
            $invoice_item->unit_id              = $item['base_unit']['id'];
            $invoice_item->quantity             = $item['quantity'];
            $invoice_item->base_price           = $item['base_price'];
            $invoice_item->selling_price        = $item['selling_price'];
            $invoice_item->total_price          = $item['total_price'];
            $invoice_item->discount             = $item['discount'];
            $invoice_item->save();

            // Update Inventory
            $inventory = Inventory::find($item['inventory_id']);
            $inventory->stocks = ($inventory->stocks - $item['quantity']);
            $inventory->save();

        }

        // SubTotal
        $invoice_total = new InvoiceTotalModel;
        $invoice_total->invoice_id  = $invoice->id;
        $invoice_total->code        = 'ST';
        $invoice_total->name        = 'Sub Total';
        $invoice_total->amount      = $request->sub_total;
        $invoice_total->sort_order  = 1;
        $invoice_total->save();

        // Discount Total
        $invoice_total = new InvoiceTotalModel;
        $invoice_total->invoice_id  = $invoice->id;
        $invoice_total->code        = 'D';
        $invoice_total->name        = 'Discount';
        $invoice_total->amount      = $request->discount;
        $invoice_total->sort_order  = 2;
        $invoice_total->save();


        // Grand Total
        $invoice_total = new InvoiceTotalModel;
        $invoice_total->invoice_id  = $invoice->id;
        $invoice_total->code        = 'GT';
        $invoice_total->name        = 'Grand Total';
        $invoice_total->amount      = $request->grand_total;
        $invoice_total->sort_order  = 3;
        $invoice_total->save();
        
        return $this->respondWithSuccessWithData('Invoice Created.', []);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
