<?php

namespace App\Http\Controllers\Api\Invoice;

use App\Invoice;
use App\InvoicePayment;
use Illuminate\Http\Request;
use App\Http\Requests\PayInvoiceRequest;
use App\Repositories\InvoiceHistoryRepository;
use App\Http\Controllers\ApiRespondController;
use App\Contracts\InvoiceHistoryContract;

class Pay extends ApiRespondController
{

    protected $invoice_history_repository;

    function __construct(InvoiceHistoryContract $invoice_history_repository)
    {
        $this->invoice_history_repository = $invoice_history_repository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PayInvoiceRequest $request)
    {

        $invoice_found = Invoice::with(['payments', 'customer', 'items', 'histories', 'totals'])->find($request->invoice_id);

        $payment_amount = $request->amount;

        $balance        = 0;

        $new_balance    = 0;

        if ($invoice_found) {

            foreach ($invoice_found->items as $item) {
                
                $balance += $item['total_price'];

            }
            
            foreach ($invoice_found->payments as $payment) {
                
                $balance -= $payment['amount'];

            }

            $new_balance = $balance - $payment_amount;

            if ($new_balance <= 0) {
                // FULLY PAID
                $invoice_payment = new InvoicePayment;
                $invoice_payment->invoice_id    = $request->invoice_id;
                $invoice_payment->account_id    = 4;
                $invoice_payment->paid_at       = date('Y-m-d', strtotime($request->transaction_date)) . ' ' .date('H:i:s');
                $invoice_payment->amount        = $payment_amount;
                $invoice_payment->description   = 'Fully Paid';
                $invoice_payment->save();

                $invoice_found->status_code = 'paid';
                $invoice_found->save();

                $this->invoice_history_repository->store([
                    'invoice_id' => $request->invoice_id,
                    'status_code' => 'paid',
                    'description' => 'Fully Paid: Amount of '.number_format($payment_amount,2).'.',
                ]);

            }else{
                
                $invoice_payment = new InvoicePayment;
                $invoice_payment->invoice_id    = $request->invoice_id;
                $invoice_payment->account_id    = 4;
                $invoice_payment->paid_at       = date('Y-m-d', strtotime($request->transaction_date)) . ' ' .date('H:i:s');
                $invoice_payment->amount        = $payment_amount;
                $invoice_payment->description   = 'Partial Payment';
                $invoice_payment->save();

                $invoice_found->status_code = 'partial';
                $invoice_found->save();

                $this->invoice_history_repository->store([
                    'invoice_id' => $request->invoice_id,
                    'status_code' => 'partial',
                    'description' => 'Partial Payment: Amount of '.number_format($payment_amount,2).'.',
                ]);
            }


        }

        return $this->respondSuccessful('Payment added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data =  Invoice::with(['payments', 'customer', 'items', 'histories', 'totals'])->find($id);

        return $this->respondWithSuccessWithData('Request Granted', ['rows' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
