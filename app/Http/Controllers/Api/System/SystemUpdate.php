<?php

namespace App\Http\Controllers\Api\System;

use App\System\SystemUpdate as Model;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiRespondController;

use App\Transformers\System\SystemUpdateTransformer;

class SystemUpdate extends ApiRespondController
{
    
    protected $transformer;

    function __construct(SystemUpdateTransformer $transformer)
    {
        $this->transformer = $transformer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Model::orderBy('created_at', 'DESC')->limit(4)->get();
        return $this->respondWithSuccessWithData('Request Granted', $this->transformer->transformCollection($data->all()));
    }

}
