<?php

namespace App\Http\Controllers\Api\Report;

use DB;
use App\Exports\TransactionExport;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiRespondController;
use App\Transformers\Report\NetSalesReportTransformer;
use Maatwebsite\Excel\Facades\Excel;

class NetSalesRevenue extends ApiRespondController
{
    protected $transformer;

    function __construct(NetSalesReportTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $dates  = explode(' - ', request('daterange'));
        
        $start_date = $dates[0];
        
        $end_date   = $dates[1];

        $total_net_sales = 0;

        $sales = [];

        $expenses = [];

        $transactions = Transaction::with([
            'account',
            'transaction_type',
            'status',
            'entries' => function($query){
                $query->with([
                    'item_variation' => function($query){
                        $query->with(['item', 'base_unit']);
                    }, 
                    'base_unit'
                ]);
            },
            'to_customer',
        ])
        ->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])
        ->where('account_id', 1)
        ->where(function($query){
            $query->where('transaction_type_id', 1);
            $query->orWhere('transaction_type_id', 2);
        })
        ->get();

        $separator = false;

        if (count($transactions) > 0) {
            
            foreach ($transactions as $transaction) {

                if (!$separator) {
                    $transaction['element_class'] = 'bg-white bg-font-white';
                    $separator = true;
                }else{
                    $transaction['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                    $separator = false;
                }

                foreach ($transaction['entries'] as $entry) {
                    
                    $net_sales = ($entry['total_price'] - ($entry['base_price'] * $entry['quantity'])) - $entry['discount'];

                    $entry['net_sales'] = $net_sales;

                    $total_net_sales += $net_sales;
                }

            }

        }

        $data = [
            'rows' => $this->transformer->transformCollection($transactions->all()),
            'total_net_sales' => number_format($total_net_sales, 2),
        ];

        return $this->respondWithSuccessWithData('Request Granted.',  $data);
        
    }


    public function export($date_from, $date_to) 
    {
        return Excel::download(new TransactionExport($date_from, $date_to), 'Net Sales Report - '.date('M d,Y', strtotime($date_from)). ' - ' . date('M d,Y', strtotime($date_to)) . '.xlsx');
    }

    
}
