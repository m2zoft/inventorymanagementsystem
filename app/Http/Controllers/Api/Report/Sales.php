<?php

namespace App\Http\Controllers\Api\Report;

use DB;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiRespondController;

use App\Transformers\Report\SalesReportTransformer;

class Sales extends ApiRespondController
{
    protected $transformer;

    function __construct(SalesReportTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $dates  = explode(' - ', request('daterange'));
        
        $start_date = $dates[0];
        
        $end_date   = $dates[1];

        $grand_total = 0;

        $transactions = Transaction::with([
            'account',
            'transaction_type',
            'status',
            'entries' => function($query){
                $query->with([
                    'item_variation' => function($query){
                        $query->with(['item', 'base_unit']);
                    }, 
                    'base_unit'
                ]);
            },
            'to_customer',
        ])
        // Account ID = 1
        // Transaction Type = 2 (Order)
        // Transaction Type = 6 (Discount)
        // 
        ->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])
        ->where('account_id', 1)
        ->where(function($query){
            $query->where('transaction_type_id', 1);
            $query->orWhere('transaction_type_id', 2);
        })
        ->get();

        $separator = false;

        foreach ($transactions as $transaction) {

            if (!$separator) {
                $transaction['element_class'] = 'bg-white bg-font-white';
                $separator = true;
            }else{
                $transaction['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                $separator = false;
            }

            $transaction['total_items'] = 0;
            foreach ($transaction['entries'] as $entry) {
                $transaction['total_items'] += (double) $entry['quantity'];
            }

            $grand_total += $transaction['total_balance'];

        }

        $data = [
            'rows' => $this->transformer->transformCollection($transactions->all()),
            'grand_total' => number_format($grand_total, 2),
        ];

        return $this->respondWithSuccessWithData('Request Granted.',  $data);
        
    }

    
}
