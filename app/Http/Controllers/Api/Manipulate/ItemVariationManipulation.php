<?php namespace App\Http\Controllers\Api\Manipulate;

use DB;
use App\Transaction;
use App\ItemVariation;
use App\TransactionEntry;
use App\ItemVariationHistory;
use App\Http\Controllers\ApiRespondController;

class ItemVariationManipulation extends ApiRespondController
{

	public function item_variation_history()
	{
		$status = [];

		$item_variations = ItemVariation::get();
		foreach ($item_variations as $variation) {
			
			$history = ItemVariationHistory::isExist($variation);
			if ($history <= 0) {
				array_push($status, ['status' => 'false', 'data' => $variation]);
				ItemVariationHistory::create([
					'item_variation_id' => $variation['id'],
					'base_unit_id' => $variation['base_unit_id'],
					'base_price' => $variation['base_price'],
					'selling_price' => $variation['selling_price'],
					'price_margin' => $variation['price_margin'],
					'created_at' => $variation['created_at'],
					'updated_at' => $variation['updated_at'],
				]);
			}else{
				array_push($status, ['status' => 'true', 'data' => 'Not Migrated.']);
			}

		}

		return $status;
	}

	public function transaction_entries_for_base_price()
	{
		
		$status = [];
		
		$transactions = Transaction::with(['entries'])->where('transaction_type_id', '!=', 4)
		->where(function($query){
			$query->where('id', '>=', 8001);
			$query->orWhere('id', '<=', 9000);
		})
		->get();

		foreach ($transactions as $transaction) {

			foreach ($transaction['entries'] as $entry) {

				$item_variation_history = ItemVariationHistory::where(function($query) use ($entry){

					$query->where('item_variation_id', $entry['item_variation_id']);

					$query->where('selling_price', $entry['price']);

				})->orderBy('id', 'DESC')->first();

				if ($item_variation_history) {
					
					$entry['base_price'] = $item_variation_history['base_price'];
					$entry->save();

					array_push($status, ['status' => 'false', 'data' => $entry]);

				}else{
					array_push($status, ['status' => 'true', 'data' => 'Not Migrated.']);
				}

			}

		}

		return $status;

	}
}