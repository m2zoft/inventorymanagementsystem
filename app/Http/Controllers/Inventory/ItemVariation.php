<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiRespondController;

use App\Item;
use App\ItemVariation as Model;



class ItemVariation extends ApiRespondController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function update($id, Request $request)
    {

        $variation = Model::find($id);
        $variation->name = $request->name;
        $variation->save();

        $item = Item::find($request->item['id']);
        $item->name = $request->item['name'];
        $item->save();

    }

}
