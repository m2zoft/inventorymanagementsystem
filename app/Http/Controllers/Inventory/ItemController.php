<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Repositories\Repository;
use App\Http\Repositories\ItemRepository;

use App\Item;
use App\Inventory;
use App\ItemVariation;
use App\ItemVariationHistory;

class ItemController extends Controller
{

    public function create()
    {
    	$request = request();
        return (new Repository(new ItemRepository))->create();
    }

    public function createItemAndVariation()
    {
    	$request = request();

        $item_exist = Item::where('name', $request['name'])->first();

        if ($item_exist) {

            foreach ($request['item_variations'] as $variation) {

                $item_variation_exist = ItemVariation::where('name', $variation['name'])->first();

                // I-check kung existing na ba ang item variation
                if ($item_variation_exist) {
                    
                    // I-update ang item variation
                    ItemVariation::where('id', $item_variation_exist->id)->update([
                        'base_unit_id' => $variation['base_unit_id'],
                        'base_price' => $variation['base_price'],
                        'selling_price' => $variation['selling_price'],
                    ]);

                    // Hanapin ang Inventory at i-add sa current stocks
                    $inventory = Inventory::where('item_variation_id', $item_variation_exist->id)->first();
                    $updated_stocks = isset($inventory) ? $inventory->stocks + $variation['quantity'] : $variation['quantity'];

                    // I-update ang inventory
                    Inventory::where('id', $inventory->id)->update(['stocks' => $updated_stocks]);

                    // Add Item Variation History
                    ItemVariationHistory::create([
                        'item_variation_id' => $item_variation_exist->id,
                        'base_unit_id' => $variation['base_unit_id'],
                        'base_price' => $variation['base_price'],
                        'selling_price' => $variation['selling_price'],
                        'price_margin' => 0,
                    ]);

                }else{

                    $item_variation = ItemVariation::create([
                        'item_id' => $item_exist->id,
                        'base_unit_id' => $variation['base_unit_id'],
                        'base_price' => $variation['base_price'],
                        'selling_price' => $variation['selling_price'],
                        'name' => $variation['name'],
                    ]);

                    Inventory::create(['item_variation_id' => $item_variation->id, 'stocks' => $variation['quantity']]);

                    // Add Item Variation History
                    ItemVariationHistory::create([
                        'item_variation_id' => $item_variation->id,
                        'base_unit_id' => $variation['base_unit_id'],
                        'base_price' => $variation['base_price'],
                        'selling_price' => $variation['selling_price'],
                        'price_margin' => 0,
                    ]);
                }

            }
            
        }else{

            if ($item = Item::create(['name' => $request['name']])) {

                foreach ($request['item_variations'] as $variation) {

                    $item_variation = ItemVariation::create([
                        'item_id' => $item->id,
                        'base_unit_id' => $variation['base_unit_id'],
                        'base_price' => $variation['base_price'],
                        'selling_price' => $variation['selling_price'],
                        'name' => $variation['name'],
                    ]);

                    Inventory::create(['item_variation_id' => $item_variation->id, 'stocks' => $variation['quantity']]);

                    // Add Item Variation History
                    ItemVariationHistory::create([
                        'item_variation_id' => $item_variation->id,
                        'base_unit_id' => $variation['base_unit_id'],
                        'base_price' => $variation['base_price'],
                        'selling_price' => $variation['selling_price'],
                        'price_margin' => 0,
                    ]);

                }

            }

        }

    	return $request;
    }    

}
