<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUnitRequest;

use App\Http\Repositories\Repository;
use App\Http\Repositories\UnitRepository;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new Repository(new UnitRepository))->all();
    }

    public function create(CreateUnitRequest $request)
    {
        return (new Repository(new UnitRepository(request())))->create();
    }    

}
