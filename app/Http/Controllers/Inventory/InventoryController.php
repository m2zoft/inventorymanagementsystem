<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiRespondController;

use App\Http\Repositories\Repository;
use App\Http\Repositories\InventoryRepository;

use App\Transaction;
use App\TransactionEntry;
use App\Inventory;
use App\ItemVariation;
use App\ItemVariationHistory;


class InventoryController extends ApiRespondController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new Repository(new InventoryRepository(request())))->paginate();
    }

    /**
     * Update the specific item's price.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */    
    public function updatePrice()
    {
        return (new InventoryRepository(request()))->updatePrice();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addStock()
    {
        $request = request();

        $transaction_number = Transaction::generateTransactionNumber();

        $total_debit = 0;
        $total_credit = 0;
        $total_balance = 0;

        foreach ($request['inventory'] as $inventory) {
            $total_debit += $inventory['item_variation']['base_price'] * $inventory['item_variation']['quantity'];
        }

        // Create Transaction
        $transaction = Transaction::create([
            'or_number'             => $request['stock']['or_number'],
            'transaction_number'    => $transaction_number,
            'account_id'            => 2, // Expense
            'transaction_type_id'   => 1, // MIGRATED
            'from'                  => $request['stock']['supplier_id'],
            'total_debit'           => $total_debit,
            'total_credit'          => $total_credit,
            'total_balance'         => abs($total_credit - $total_debit),
            'transaction_date'      => date('Y-m-d', strtotime($request['stock']['transaction_date'])),
        ]);
        // Create Transaction Entries
        foreach ($request['inventory'] as $inventory) {

            TransactionEntry::create([
                'transaction_id'        => $transaction->id,
                'item_variation_id'     => $inventory['item_variation_id'],
                'base_unit_id'          => $inventory['item_variation']['base_unit_id'],
                'quantity'              => $inventory['item_variation']['quantity'],
                'price'                 => $inventory['item_variation']['base_price'],
                'base_price'            => $inventory['item_variation']['base_price'],
                'total_price'           => $inventory['item_variation']['base_price'] * $inventory['item_variation']['quantity'],
            ]);

            $total_stocks   = $inventory['stocks'] + $inventory['item_variation']['quantity'];

            Inventory::where('id', $inventory['id'])->update(['stocks' => $total_stocks]);

            ItemVariation::where('id', $inventory['item_variation_id'])->update([
                'base_price'    => $inventory['item_variation']['base_price'],
                'selling_price' => $inventory['item_variation']['selling_price'],
            ]);

            // Add Item Variation History
            ItemVariationHistory::create([
                'item_variation_id' => $inventory['item_variation_id'],
                'base_unit_id' => $inventory['item_variation']['base_unit_id'],
                'base_price' => $inventory['item_variation']['base_price'],
                'selling_price' => $inventory['item_variation']['selling_price'],
                'price_margin' => 0,
            ]);
        }

        return $request;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function receiveStock(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adjustStock()
    {
        $request = request();

        $transaction_number = Transaction::generateTransactionNumber();

        // Add Transaction
        $transaction = Transaction::create([
            'or_number'             => 0,
            'transaction_number'    => $transaction_number,
            'account_id'            => 3, // Inventory Adjustment
            'transaction_type_id'   => 4, // Stock Adjustment
            'from'                  => 0,
            'total_debit'           => 0,
            'total_credit'          => 0,
            'total_balance'         => 0,
            'transaction_date'      => date('Y-m-d'),
        ]);

        // Add Transaction Entry
        TransactionEntry::create([
            'transaction_id'        => $transaction->id,
            'item_variation_id'     => $request['item_variation_id'],
            'base_unit_id'          => $request['base_unit_id'],
            'quantity'              => $request['quantity'],
            'base_price'            => 0,
            'price'                 => 0,
            'total_price'           => 0,
        ]);

        // Update Inventory
        $inventory = Inventory::find($request['inventory_id']);
        $inventory->stocks = $request['new_quantity'];
        $inventory->save();

        return $this->respondSuccessful('Stock Adjustment has been successful.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        //
    }

}
