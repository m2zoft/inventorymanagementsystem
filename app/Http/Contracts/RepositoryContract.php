<?php namespace App\Http\Contracts;

interface RepositoryContract{

	public function all();
	public function paginate();
	public function create();
	public function edit();
	public function update();
	public function delete();

}