<?php namespace App\Http\Traits\Formatter;

use Carbon\Carbon;

trait Formatter{

	protected $data, $column, $custom_column, $format, $second_level_column;

	protected $formatted = [];

	public function setFormatterData($data = [])
	{
		$this->data = $data;
		return $this;
	}

	public function setFormatterColumn($column = '')
	{
		$this->column = $column;
		return $this;
	}

	public function setFormatterCustomColumn($custom_column)
	{
		$this->custom_column = $custom_column;
		return $this;
	}

	public function setFormatterSecondLevelColumn($second_level_column)
	{
		$this->second_level_column = $second_level_column;
		return $this;
	}

	public function setFormatterFormat($format = 'M d, Y')
	{
		$this->format = $format;
		return $this;
	}

	public function customDate()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				$found = array_search("formatted", [$d]);
				$column = [$this->custom_column => date($this->format, strtotime($d[$this->column]))];
				if ($found) {
					array_push($d[$found], $column);
				}else{
					$d['formatted'] = $column;
				}
				
			}
		}
	}

	public function currency()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				$found = array_search("formatted", [$d]);
				$column = [$d[$this->custom_column] => number_format($d[$this->column],2)];

				if ($found) {
					array_push($d[$found], $column);
				}else{
					$d['formatted'] = $found;
				}
			}
		}
	}
	

}