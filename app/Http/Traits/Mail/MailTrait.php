<?php namespace App\Http\Traits\Mail;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationMail;

trait MailTrait{

	protected $to, $from, $subject, $body;

	public function setTo($to)
	{
		$this->to = $to;

		return $this;
	}

	public function setFrom($from)
	{
		$this->from = $from;

		return $this;
	}

	public function setSubject($subject)
	{
		$this->subject = $subject;

		return $this;
	}

	public function setBody($body)
	{
		$this->body = $body;

		return $this;
	}

	public function send($data)
	{
		Mail::to($this->to)->send(new RegistrationMail($data));
	}

}