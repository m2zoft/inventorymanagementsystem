<?php namespace App\Http\Traits\User;

use App\Http\Models\CampaignModel;
use Location;
use BrowserInfo;
use Activation;
use Sentinel;

trait CampaignTrait{

	public function activate($id)
	{
		return CampaignModel::where('id', $id)->update(['status' => 'active']);
	}

	public function approve($id)
	{
		return $this->activate($id);
	}

	public function reject($id)
	{
		return CampaignModel::where('id', $id)->update(['status' => 'rejected']);
	}

	public function end($id)
	{
		return CampaignModel::where('id', $id)->update(['status' => 'ended']);
	}

	public function disable($id)
	{
		return CampaignModel::where('id', $id)->update(['status' => 'disabled']);
	}


}