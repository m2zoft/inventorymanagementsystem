<?php namespace App\Http\Traits\User;

use App\Http\Models\UserModel;
use App\Http\Models\RoleUserModel;
use App\Http\Models\ActivationModel;
use App\Http\Models\UserVerificationModel;
use App\Http\Models\VerificationTypeModel;
use App\Http\Models\SocialAccountModel;
use App\Http\Models\UserSocialAccountModel;
use Location;
use BrowserInfo;
use Activation;
use Sentinel;

trait UserTrait{

	protected $code;

	public function isAuthenticated($credentials = [])
	{
		return Sentinel::authenticateAndRemember($credentials);
	}

	public function findActivationCode($user_id)
	{
		$activation = ActivationModel::where('user_id', $user_id)->get();

		return $activation->count() > 0 ? $activation['0']['code'] : '';
	}

	public function findEmail($email)
	{
		return UserModel::where('email', $email)->get();
	}

	public function complete($code)
	{
		$find_user = ActivationModel::where('code', $code)->first();

		$user = Sentinel::findById($find_user->user_id);

		UserModel::where('id', $find_user->user_id)->update(['status' => 'active']);

		return Activation::complete($user, $code);
	}

	public function activate($id)
	{
		
		$activation_code = $this->findActivationCode($id);

		$this->complete($activation_code);

		UserModel::where('id', $id)->update(['status' => 'active']);

	}

	public function ban($id)
	{
		return UserModel::where('id', $id)->update(['status' => 'banned']); 
	}

	public function unban($id)
	{
		return UserModel::where('id', $id)->update(['status' => 'active']);
	}

	public function deactivate()
	{
		# code...
	}

	public function getLocation()
	{
		// return $_SERVER['REMOTE_ADDR'];
		return Location::get($_SERVER['REMOTE_ADDR']);
	}

	public function getBrowser()
	{
		return BrowserInfo::getUserAgent();
	}

	public function getRole($user_id)
	{
		return RoleUserModel::where('user_id', $user_id)->with('role')->first();
	}

	public function getUser($user_id)
	{
		return UserModel::where('id', $user_id)->first();
	}

	public function setAccountCode($code)
	{
		$this->code = $code;
		return $this;
	}

	public function getAccountCode()
	{
		return $this->code;
	}

	public function generateAccountNumber()
	{
		$account_number = 0;
		$findLastUserAccountNumber = UserModel::orderBy('id', 'desc')->first();
		if ($findLastUserAccountNumber) {
			$account_number = $findLastUserAccountNumber->account_number + 1;
		}

		return $account_number;
	}

	public function profileCompleteness($id)
	{
		$total_verification_types = VerificationTypeModel::count();
		$total_verified = 0;

		// User Verification
		$verifications = UserVerificationModel::where('user_id', $id)->with('verification_type')->get();
		if ($verifications->count() > 0) {
			foreach ($verifications as $verification) {
				if ($verification['status'] == 'verified') {
					$total_verified += 1;
				}
			}
		}

		return [
			'total_verified' => $total_verified,
			'total_verification_types' => $total_verification_types,
			'types' => $verifications,
			'percentage' => (($total_verified/$total_verification_types) * 100)
		];

	}

	public function generateVerifications($id)
	{
		// Verification Types
		$verification_types = VerificationTypeModel::all();

		if ($verification_types->count() > 0) {

			foreach ($verification_types as $verification_type) {

				$is_verification_exist = UserVerificationModel::where('user_id', $id)->where('verification_type_id', $verification_type['id'])->get();

				if ($is_verification_exist->count() <= 0) {

					UserVerificationModel::create(['user_id' => $id, 'verification_type_id' => $verification_type['id'], 'status' => 'unverified']);

				}

			}

		}

	}

	public function generateSocialAccounts($id)
	{
		// Social Account
		$social_accounts = SocialAccountModel::all();

		if ($social_accounts->count() > 0) {

			foreach ($social_accounts as $social_account) {

				$is_social_account_exists = UserSocialAccountModel::where('user_id', $id)->where('social_account_id', $social_account['id'])->get();

				if ($is_social_account_exists->count() <= 0) {

					UserSocialAccountModel::create(['user_id' => $id, 'social_account_id' => $social_account['id'], 'unique_social_account' => 'https://']);

				}

			}

		}

	}



	public function appendSocialAccounts($data, $column)
	{
		if (count($data) > 0) {
			foreach ($data as $d) {
				if (count($d[$column]) > 0) {
					foreach ($d[$column] as $social_account) {
						$social_account['type'] = SocialAccountModel::where('id', $social_account['social_account_id'])->get()->first();
					}
				}
			}
		}
	}


}