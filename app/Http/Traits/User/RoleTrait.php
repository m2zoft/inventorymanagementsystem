<?php namespace App\Http\Traits\User;

use Sentinel;
use App\Http\Models\RoleModel;
use App\Http\Models\RoleUserModel;

trait RoleTrait{

	public function findRoleBySlugName($slug_name)
	{
		return Sentinel::findRoleBySlug($slug_name);
	}

	public function assignRole($user_id, $slug)
	{
		$user = Sentinel::findById($user_id);

		$role = Sentinel::findRoleBySlug($slug);

		$role->users()->attach($user);
	}

}