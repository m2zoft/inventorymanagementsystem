<?php namespace App\Http\Traits\Filter;

use Carbon\Carbon;

trait ArrayFind{

    function array_find($needle, array $haystack, $column = null) {
        
        if(is_array($haystack[0]) === true) { // check for multidimentional array

            foreach (array_column($haystack, $column) as $key => $value) {
                if (strpos(strtolower($value), strtolower($needle)) !== false) {
                    return $key;
                }
            }

        } else {
            foreach ($haystack as $key => $value) { // for normal array
                if (strpos(strtolower($value), strtolower($needle)) !== false) {
                    return $key;
                }
            }
        }
        return false;
    }

}