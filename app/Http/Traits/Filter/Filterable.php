<?php namespace App\Http\Traits\Filter;

use Carbon\Carbon;

trait Filterable{

	protected $data, $column, $custom_column, $format, $second_level_column;

	public function setData($data = [])
	{
		$this->data = $data;
		return $this;
	}

	public function setColumn($column = '')
	{
		$this->column = $column;
		return $this;
	}

	public function setCustomColumn($custom_column)
	{
		$this->custom_column = $custom_column;
		return $this;
	}

	public function setSecondLevelColumn($second_level_column)
	{
		$this->second_level_column = $second_level_column;
		return $this;
	}

	public function setFormat($format = 'M d, Y')
	{
		$this->format = $format;
		return $this;
	}

	public function filterDate()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				$d[$this->custom_column] = date($this->format, strtotime($d[$this->column]));
			}
		}
	}

	public function secondLevelFilterDate()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				if (count($d[$this->second_level_column]) > 0) {
					foreach ($d[$this->second_level_column] as $second) {
						$second[$this->custom_column] = date($this->format, strtotime($second[$this->column]));
					}
				}
			}
		}
	}

	public function diffForHumanSecondLevel()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				if (count($d[$this->second_level_column]) > 0) {
					foreach ($d[$this->second_level_column] as $second) {
						$second[$this->custom_column] = $second[$this->column]->diffForHumans();
					}
				}
			}
		}		
	}

	public function filterPadding()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				$d[$this->custom_column] = str_pad($d[$this->column],9,"0",STR_PAD_LEFT);
			}
		}
		
	}

	public function formatToCurrency()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				$d[$this->custom_column] = number_format($d[$this->column],2);
			}
		}
	}

	public function totalRaisedPerCampaign()
	{
		if (count($this->data) > 0) {
			foreach ($this->data as $d) {
				$total_raised = 0;
				if (count($d[$this->column]) > 0) {
					foreach ($d[$this->column] as $val) {
						if ($val['transaction_status']['name'] == 'approved' && $val['transaction_type']['name'] == 'Donate') {
							$total_raised += $val['amount'];
						}
					}
				}

				$d[$this->custom_column] = number_format($total_raised, 2);
				$d['percentage_raised'] = number_format(($total_raised/$d['target_amount']) * 100);
			}
		}	
	}
	

}