<?php namespace App\Http\Repositories;

use App\Http\Contracts\RepositoryContract;

class Repository
{
	
	protected $repo;

	function __construct(RepositoryContract $repo)
	{
		$this->repo = $repo;
	}

	public function all(){
		return $this->repo->all();
	}

	public function paginate(){
		return $this->repo->paginate();
	}

	public function create(){
		return $this->repo->create();
	}

	public function edit(){
		return $this->repo->edit();
	}

	public function update(){
		return $this->repo->update();
	}

	public function delete(){
		return $this->repo->delete();
	}
}