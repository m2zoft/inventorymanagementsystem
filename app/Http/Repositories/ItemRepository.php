<?php namespace App\Http\Repositories;

use Response;
use App\Item;

/* Traits */
use App\Http\Traits\Filter\Filterable;
use App\Http\Traits\Formatter\Formatter;

use App\Http\Contracts\RepositoryContract;

class ItemRepository implements RepositoryContract
{

	use Filterable, Formatter;

	protected $request, $search, $page, $rows, $offset;	

	function __construct($request = [])
	{

		$this->request 	= $request;

		$this->search   = isset($this->request['search']) ? $this->request['search'] : '';

		$this->page     = isset($this->request['page'])   ? $this->request['page']   : 1;

		$this->rows     = isset($this->request['rows'])  && $this->request['rows'] <= 50 ? $this->request['rows'] : 50;

		$this->offset 	= ($this->page - 1) * $this->rows;

	}

	public function all(){

	}

	public function paginate(){
	}

	public function create(){
		
	}

	public function edit(){
		
	}

	public function update(){
		
	}

	public function delete(){
		
	}
}