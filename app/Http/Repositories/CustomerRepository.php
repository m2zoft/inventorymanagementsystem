<?php namespace App\Http\Repositories;

use Response;
use App\Customer;

/* Traits */
use App\Http\Traits\Filter\Filterable;
use App\Http\Traits\Formatter\Formatter;

use App\Http\Contracts\RepositoryContract;

class CustomerRepository implements RepositoryContract
{

	use Filterable, Formatter;

	protected $request, $search, $page, $rows, $offset;	

	function __construct($request = [])
	{

		$this->request 	= $request;

		$this->search   = isset($this->request['search']) ? $this->request['search'] : '';

		$this->page     = isset($this->request['page'])   ? $this->request['page']   : 1;

		$this->rows     = isset($this->request['rows'])  && $this->request['rows'] <= 50 ? $this->request['rows'] : 50;

		$this->offset 	= ($this->page - 1) * $this->rows;

	}

	public function all(){
		return Customer::all();
	}

	public function paginate(){

	}

	public function create(){
		Customer::create([
			'first_name' => $this->request['first_name'],
			'last_name' => $this->request['last_name'],
			'address' => $this->request['address'],
			'mobile_number' => $this->request['mobile_number'],
		]);
		return $this->all();
	}

	public function edit(){
		return $this->repo->edit();
	}

	public function update(){
		return $this->repo->update();
	}

	public function delete(){
		return $this->repo->delete();
	}
}