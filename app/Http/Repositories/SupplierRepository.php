<?php namespace App\Http\Repositories;

use Response;
use App\Supplier;

/* Traits */
use App\Http\Traits\Filter\Filterable;
use App\Http\Traits\Formatter\Formatter;

use App\Http\Contracts\RepositoryContract;

class SupplierRepository implements RepositoryContract
{

	use Filterable, Formatter;

	protected $request, $search, $page, $rows, $offset;	

	function __construct($request = [])
	{

		$this->request 	= $request;

		$this->search   = isset($this->request['search']) ? $this->request['search'] : '';

		$this->page     = isset($this->request['page'])   ? $this->request['page']   : 1;

		$this->rows     = isset($this->request['rows'])  && $this->request['rows'] <= 50 ? $this->request['rows'] : 50;

		$this->offset 	= ($this->page - 1) * $this->rows;

	}

	public function all(){
		return Supplier::all();
	}

	public function paginate(){

		$total = Inventory::with(['item_variation' => function($query){
					$query->with(['item', 'base_unit']);
				}])

				->with(['item_variation_history' => function($query){
					$query->with(['base_unit']);
				}])

				->whereHas('item_variation', function($query){ 

					if ($this->search !== '') {
						$query->Where('name', 'LIKE', '%'.$this->search.'%');
						$query->orWhereHas('item', function($q){
							$q->Where('name', 'LIKE', '%'.$this->search.'%');
						});
						
					}				

				})	

				->count();


		$data = Inventory::with(['item_variation' => function($query){
					$query->with(['item', 'base_unit']);
				}])

				->with(['item_variation_history' => function($query){
					$query->with(['base_unit']);
				}])

				->whereHas('item_variation', function($query){ 

					if ($this->search !== '') {
						$query->Where('name', 'LIKE', '%'.$this->search.'%');
						$query->orWhereHas('item', function($q){
							$q->Where('name', 'LIKE', '%'.$this->search.'%');
						});
						
					}				

				})		

				->skip($this->offset)

				->take($this->rows)

				->get();

		// Custom Date
		$this->setData($data)->setColumn($column = 'created_at')->setCustomColumn($custom_column = 'formatted_created_at')->setFormat($format = 'M d, Y h:i A')->filterDate();
		
	    return Response()->json(['total' => $total, 'rows' => $data], 200);
	}

	public function create(){
		Supplier::create(['code' => $this->request['code'], 'name' => $this->request['name'], 'status' => 'active']);
		return $this->all();
	}

	public function edit(){
		return $this->repo->edit();
	}

	public function update(){
		return $this->repo->update();
	}

	public function delete(){
		return $this->repo->delete();
	}
}