<?php namespace App\Http\Repositories;

use Response;
use App\Customer;
use App\Inventory;
use App\ItemVariation;
use App\ItemVariationHistory;

use App\Transaction;
use App\TransactionEntry;

use Carbon\Carbon;
use DB;

/* Statistics */
use App\Statistics\Statistic;
use App\Statistics\SalesStatistic;


/* Traits */
use App\Http\Traits\Filter\Filterable;
use App\Http\Traits\Formatter\Formatter;

use App\Http\Contracts\RepositoryContract;

class SalesRepository implements RepositoryContract
{

	use Filterable, Formatter;

	protected $request, $search, $page, $rows, $offset;	

	function __construct($request = [])
	{

		$this->request 	= $request;

		$this->search   = isset($this->request['search']) ? $this->request['search'] : '';

		$this->page     = isset($this->request['page'])   ? $this->request['page']   : 1;

		$this->rows     = isset($this->request['rows'])  && $this->request['rows'] <= 50 ? $this->request['rows'] : 50;

		$this->offset 	= ($this->page - 1) * $this->rows;

		$this->date_range = isset($this->request['daterange']) ? $this->request['daterange'] : NULL;

	}

	public function all(){
		return Customer::all();
	}

	public function paginate(){

	}

	public function paginateSalesOrder()
	{

		$exploded_date = explode(' - ', $this->date_range);

		$date_from = date('Y-m-d', strtotime($exploded_date[0]));

		$date_to = date('Y-m-d', strtotime($exploded_date[1]));

		$total = Transaction::with(['entries' => function($query){
					$query->with(['item_variation' => function($query){
						$query->with(['item', 'base_unit']);
					}, 'base_unit']);
				}])

				->with(['to_customer'])

				
				->orWhere(function($query){ 

					$query->where('or_number', 'LIKE', '%'.$this->search.'%');
					$query->orWhere('transaction_number', 'LIKE', '%'.$this->search.'%');					
		
					$query->orWhereHas('to_customer', function($query){
						$query->where('first_name', 'LIKE', '%'.$this->search.'%');
						$query->orWhere('last_name', 'LIKE', '%'.$this->search.'%');

					});

				})					

				->where(function($query){ 
					$query->where('transaction_type_id', 1);
					$query->orWhere('transaction_type_id', 2);
				})
			
				->whereBetween(DB::raw('DATE(transaction_date)'), [$date_from, $date_to])	

				->where('account_id', 1)

				->count();


		$data = Transaction::with(['entries' => function($query){
					$query->with(['item_variation' => function($query){
						$query->with(['item', 'base_unit']);
					}, 'base_unit']);
				}])
				->with(['to_customer'])

				->orWhere(function($query){ 

					$query->where('or_number', 'LIKE', '%'.$this->search.'%');
					$query->orWhere('transaction_number', 'LIKE', '%'.$this->search.'%');					
		
					$query->orWhereHas('to_customer', function($query){
						$query->where('first_name', 'LIKE', '%'.$this->search.'%');
						$query->orWhere('last_name', 'LIKE', '%'.$this->search.'%');

					});

				})					

				->where(function($query){ 
					$query->where('transaction_type_id', 1);
					$query->orWhere('transaction_type_id', 2);
				})
			
				->whereBetween(DB::raw('DATE(transaction_date)'), [$date_from, $date_to])	

				->where('account_id', 1)

				->skip($this->offset)

				->take($this->rows)

				->orderBy('transaction_date', 'DESC')

				->get();

		// Get the total items
		foreach ($data as $d) {
			$total_items = 0;
			foreach ($d['entries'] as $entry) {
				$total_items += intval($entry['quantity']);
			}

			$d['total_items'] = $total_items;
		}			


		// Custom Date
		$this->setData($data)->setColumn($column = 'created_at')->setCustomColumn($custom_column = 'formatted_created_at')->setFormat($format = 'M d, Y h:i A')->filterDate();

		$this->setData($data)->setColumn($column = 'transaction_date')->setCustomColumn($custom_column = 'formatted_transaction_date')->setFormat($format = 'M d, Y')->filterDate();
		
	    return Response()->json(['total' => $total, 'rows' => $data, 'statistics' => (new Statistic(new SalesStatistic([
	    	'start_date' => $date_from,
			'end_date' => $date_to,
	    ])))->total()], 200);
	}

	public function create(){

	}


	public function edit(){
		return $this->repo->edit();
	}

	public function update(){
		return $this->repo->update();
	}

	public function delete(){
		return $this->repo->delete();
	}
}