<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customers";

    protected $fillable = ['first_name', 'last_name', 'address', 'mobile_number'];

    protected $dates = ['created_at', 'updated_at'];
}
