<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceHistory extends Model
{

    protected $table = "invoice_histories";

    protected static $invoice = 'App\Invoice';

    protected $fillable = [
        'invoice_id',
        'status_code',
        'description',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function invoice()
    {
    	return $this->hasOne(static::$invoice, 'id', 'invoice_id');
    }

}
