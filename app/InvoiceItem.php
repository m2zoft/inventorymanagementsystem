<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{

    protected $table = "invoice_items";

    protected static $invoice = 'App\Invoice';

    protected static $item_variation = 'App\ItemVariation';

    protected static $unit = 'App\Unit';

    protected $fillable = [
        'invoice_id',
        'item_variation_id',
        'name',
        'description',
        'unit_id',
        'quantity',
        'base_price',
        'selling_price',
        'discount',
        'total_price',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function invoice()
    {
    	return $this->hasOne(static::$invoice, 'id', 'invoice_id');
    }

    public function item_variation()
    {
        return $this->hasOne(static::$item_variation, 'id', 'item_variation_id');
    }

    public function unit()
    {
        return $this->hasOne(static::$unit, 'id', 'unit_id');
    }
       
}
