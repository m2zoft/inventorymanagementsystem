<?php

namespace App\Exports;

use DB;
use App\Exports\TransactionExport;
use App\Transaction;
use App\TransactionEntry;
use App\Transformers\Report\InventorySales as InventorySalesTransformer;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class InventorySummaryExport implements FromView
{
	
	function __construct($date_from, $date_to)
	{

		$this->transformer = new InventorySalesTransformer;

		$this->date_from 	= $date_from;

		$this->date_to 		= $date_to;
	}


	public function view(): View
    {
    	$start_date = $this->date_from;
        
        $end_date   = $this->date_to;

        $total_sales = 0;

        $transaction_entries = TransactionEntry::with(['item_variation', 'base_unit'])
            ->join('transactions as T', 'T.id', '=', 'transaction_entries.transaction_id')
            ->join('item_variations', 'item_variations.id', '=', 'transaction_entries.item_variation_id')
            ->join('items', 'items.id', '=', 'item_variations.item_id')
            ->leftJoin('units', 'units.id', '=', 'transaction_entries.base_unit_id')
            ->whereBetween(DB::raw('DATE(T.transaction_date)'), [$start_date, $end_date])
//            ->groupBy('transaction_entries.item_variation_id')
            ->groupBy('transaction_entries.item_name')
            ->groupBy('transaction_entries.item_variation_name')
            ->selectRaw('
                transaction_entries.item_variation_id, 
                sum(transaction_entries.quantity) as quantity, 
                sum(transaction_entries.total_price) as total_price, 
                sum(transaction_entries.discount) as discount, 
                transaction_entries.item_variation_id, 
                transaction_entries.item_variation_name as item_variation_name, 
                transaction_entries.item_name as item_name,
                item_variations.base_unit_id
            ')
            ->orderBy('item_name', 'ASC')
            ->orderBy('item_variation_name', 'ASC')
            ->get();

        if ($transaction_entries->count() > 0) {

            $separator = false;

            foreach ($transaction_entries as $transaction_entry) {
                
                if (!$separator) {
                    $transaction_entry['element_class'] = 'bg-white bg-font-white';
                    $separator = true;
                }else{
                    $transaction_entry['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                    $separator = false;
                }

                $transaction_entry['inventory'] = \App\Inventory::where('item_variation_id', $transaction_entry['item_variation_id'])->first();
                $total_sales += $transaction_entry['total_price'];

            }

        }

        $data = [
            'rows' => $this->transformer->transformCollection($transaction_entries->all()),
            'total_sales' => number_format($total_sales, 2),
        ];

        return view('app.exports.inventory-summary', [
            'data' => $data
        ]);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        
        $start_date = $this->date_from;
        
        $end_date   = $this->date_to;

        $total_sales = 0;

        $transaction_entries = TransactionEntry::with(['item_variation', 'base_unit'])
            ->join('transactions as T', 'T.id', '=', 'transaction_entries.transaction_id')
            ->join('item_variations', 'item_variations.id', '=', 'transaction_entries.item_variation_id')
            ->join('items', 'items.id', '=', 'item_variations.item_id')
            ->leftJoin('units', 'units.id', '=', 'transaction_entries.base_unit_id')
            ->whereBetween(DB::raw('DATE(T.transaction_date)'), [$start_date, $end_date])
            ->groupBy('transaction_entries.item_variation_id')
            ->selectRaw('
                transaction_entries.item_variation_id, 
                sum(transaction_entries.quantity) as quantity, 
                sum(transaction_entries.total_price) as total_price, 
                sum(transaction_entries.discount) as discount, 
                transaction_entries.item_variation_id, 
                item_variations.name as item_variation_name, 
                items.name as item_name,
                item_variations.base_unit_id
            ')
            ->orderBy('item_name', 'ASC')
            ->orderBy('item_variation_name', 'ASC')
            ->get();

        if ($transaction_entries->count() > 0) {

            $separator = false;
            
            foreach ($transaction_entries as $transaction_entry) {
                
                if (!$separator) {
                    $transaction_entry['element_class'] = 'bg-white bg-font-white';
                    $separator = true;
                }else{
                    $transaction_entry['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                    $separator = false;
                }

                $total_sales += $transaction_entry['total_price'];

            }

        }

        $data = [
            'rows' => $this->transformer->transformCollection($transaction_entries->all()),
            'total_sales' => number_format($total_sales, 2),
        ];

        return $this->transformer->transformCollection($transaction_entries->all());
    }
}

