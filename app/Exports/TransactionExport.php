<?php

namespace App\Exports;

use DB;
use App\Transaction;
use App\Transformers\Report\NetSalesReportTransformer;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class TransactionExport implements FromView
{

    /**
     * @var NetSalesReportTransformer
     */
    private $transformer;
    private $date_from;
    private $date_to;

    function __construct($date_from, $date_to)
	{

		$this->transformer = new NetSalesReportTransformer;

		$this->date_from 	= $date_from;

		$this->date_to 		= $date_to;
	}


	public function view(): View
    {
    	$start_date = $this->date_from;
        
        $end_date   = $this->date_to;

        $total_net_sales = 0;

        $sales = [];

        $expenses = [];

        $transactions = Transaction::with([
            'account',
            'transaction_type',
            'status',
            'entries' => function($query){
                $query->with([
                    'item_variation' => function($query){
                        $query->with(['item', 'base_unit']);
                    }, 
                    'base_unit'
                ]);
            },
            'to_customer',
        ])
        // Account ID = 1
        // Transaction Type = 2 (Order)
        // Transaction Type = 6 (Discount)
        // 
        ->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])
        ->where('account_id', 1)
        ->where(function($query){
            $query->where('transaction_type_id', 1);
            $query->orWhere('transaction_type_id', 2);
        })
        ->get();

        $separator = false;

        if (count($transactions) > 0) {
            
            foreach ($transactions as $transaction) {

                if (!$separator) {
                    $transaction['element_class'] = 'bg-white bg-font-white';
                    $separator = true;
                }else{
                    $transaction['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                    $separator = false;
                }

                foreach ($transaction['entries'] as $entry) {
                    
                    $net_sales = ($entry['total_price'] - ($entry['base_price'] * $entry['quantity'])) - $entry['discount'];

                    $entry['net_sales'] = $net_sales;

                    $total_net_sales += $net_sales;
                }

            }

        }

        $data = [
            'rows' => $this->transformer->transformCollection($transactions->all()),
            'total_net_sales' => number_format($total_net_sales, 2),
        ];

        return view('app.exports.net-sale', [
            'data' => $data
        ]);
    }

    /**
    * @return array
    */
    public function collection()
    {
        
        $start_date = $this->date_from;
        
        $end_date   = $this->date_to;

        $total_net_sales = 0;

        $sales = [];

        $expenses = [];

        $transactions = Transaction::with([
            'account',
            'transaction_type',
            'status',
            'entries' => function($query){
                $query->with([
                    'item_variation' => function($query){
                        $query->with(['item', 'base_unit']);
                    }, 
                    'base_unit'
                ]);
            },
            'to_customer',
        ])
        // Account ID = 1
        // Transaction Type = 2 (Order)
        // Transaction Type = 6 (Discount)
        // 
        ->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])
        ->where('account_id', 1)
        ->where(function($query){
            $query->where('transaction_type_id', 1);
            $query->orWhere('transaction_type_id', 2);
        })
        ->get();

        $separator = false;

        if (count($transactions) > 0) {
            
            foreach ($transactions as $transaction) {

                if (!$separator) {
                    $transaction['element_class'] = 'bg-white bg-font-white';
                    $separator = true;
                }else{
                    $transaction['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                    $separator = false;
                }

                foreach ($transaction['entries'] as $entry) {
                    
                    $net_sales = ($entry['total_price'] - ($entry['base_price'] * $entry['quantity'])) - $entry['discount'];

                    $entry['net_sales'] = $net_sales;

                    $total_net_sales += $net_sales;
                }

            }

        }

        $data = [
            'rows' => $this->transformer->transformCollection($transactions->all()),
            'total_net_sales' => number_format($total_net_sales, 2),
        ];

        return $this->transformer->transformCollection($transactions->all());
    }
}
