<?php

namespace App\Exports;

use App\ItemVariation;
use DB;
use App\TransactionEntry;
use App\Transformers\Report\InventorySales as InventorySalesTransformer;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class InventoryExport implements FromView
{

    /**
     * @var InventorySalesTransformer
     */
    private $transformer;
    private $date;

    function __construct($date)
    {

        $this->transformer = new InventorySalesTransformer;

        $this->date 	= $date;

    }


    public function view(): View
    {
        $failed = [];
        $base_price_total = 0;
        $srp_total = 0;
        $structured_data = [];

        $transaction_entries = ItemVariation::with(['item','base_unit'])
            ->join('items as I', 'I.id', '=', 'item_variations.item_id')
            ->selectRaw('I.name as item_name, item_variations.*')
//        	->groupBy('transaction_entries.item_variation_id')
            ->orderBy('item_name', 'ASC')
            ->get();

        if ($transaction_entries->count() > 0) {

            $separator = false;

            foreach ($transaction_entries as &$transaction_entry) {
                try{

                    if ((empty($transaction_entry['name']) || is_null($transaction_entry['name']) || $transaction_entry['name'] == '') && (empty($transaction_entry['item']['name']) || is_null($transaction_entry['item']['name']) || $transaction_entry['item']['name'] == '')){
                        continue;
                    }

                    if (!$separator) {
                        $transaction_entry['element_class'] = 'bg-white bg-font-white';
                        $separator = true;
                    }else{
                        $transaction_entry['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                        $separator = false;
                    }

                    $transaction_entry['inventory'] = \App\Inventory::where('item_variation_id', $transaction_entry['id'])->first();

                    $inline_base_price_total = ($transaction_entry['inventory']['stocks'] * $transaction_entry['base_price']);
                    $transaction_entry['base_price_total'] = $inline_base_price_total <= 0 ? 0 : $inline_base_price_total;

                    $inline_srp_total = ($transaction_entry['inventory']['stocks'] * $transaction_entry['selling_price']);
                    $transaction_entry['srp_total'] = $inline_srp_total <= 0 ? 0 : $inline_srp_total;

                    $transaction_entry['readable'] = [
                        'base_price_total' => number_format($transaction_entry['base_price_total'], 2),
                        'srp_total' => number_format($transaction_entry['srp_total'], 2),
                        'total_stocks' => number_format($transaction_entry['inventory']['stocks']),
                    ];
                    $base_price_total += $transaction_entry['base_price_total'];
                    $srp_total += $transaction_entry['srp_total'];

                    $structured_data[] = $transaction_entry;

                }catch(\Exception $e){
                    $failed[] = $transaction_entry;
                    continue;
                }

            }

        }

        $data = [
            'rows' => $structured_data,
            'base_price_total' => number_format($base_price_total, 2),
            'srp_total' => number_format($srp_total, 2),
            'failed' => $failed
        ];

        return view('app.exports.inventory', [
            'data' => $data
        ]);
    }

    /**
     * @return array
     */
    public function collection()
    {

        $failed = [];
        $base_price_total = 0;
        $srp_total = 0;
        $structured_data = [];

        $transaction_entries = ItemVariation::with(['item','base_unit'])
            ->join('items as I', 'I.id', '=', 'item_variations.item_id')
            ->selectRaw('I.name as item_name, item_variations.*')
//        	->groupBy('transaction_entries.item_variation_id')
            ->orderBy('item_name', 'ASC')
            ->get();

        if ($transaction_entries->count() > 0) {

            $separator = false;

            foreach ($transaction_entries as &$transaction_entry) {
                try{

                    if ((empty($transaction_entry['name']) || is_null($transaction_entry['name']) || $transaction_entry['name'] == '') && (empty($transaction_entry['item']['name']) || is_null($transaction_entry['item']['name']) || $transaction_entry['item']['name'] == '')){
                        continue;
                    }

                    if (!$separator) {
                        $transaction_entry['element_class'] = 'bg-white bg-font-white';
                        $separator = true;
                    }else{
                        $transaction_entry['element_class'] = 'bg-grey-cararra bg-font-grey-cararra';
                        $separator = false;
                    }

                    $transaction_entry['inventory'] = \App\Inventory::where('item_variation_id', $transaction_entry['id'])->first();

                    $inline_base_price_total = ($transaction_entry['inventory']['stocks'] * $transaction_entry['base_price']);
                    $transaction_entry['base_price_total'] = $inline_base_price_total <= 0 ? 0 : $inline_base_price_total;

                    $inline_srp_total = ($transaction_entry['inventory']['stocks'] * $transaction_entry['selling_price']);
                    $transaction_entry['srp_total'] = $inline_srp_total <= 0 ? 0 : $inline_srp_total;

                    $transaction_entry['readable'] = [
                        'base_price_total' => number_format($transaction_entry['base_price_total'], 2),
                        'srp_total' => number_format($transaction_entry['srp_total'], 2),
                        'total_stocks' => number_format($transaction_entry['inventory']['stocks']),
                    ];
                    $base_price_total += $transaction_entry['base_price_total'];
                    $srp_total += $transaction_entry['srp_total'];

                    $structured_data[] = $transaction_entry;

                }catch(\Exception $e){
                    $failed[] = $transaction_entry;
                    continue;
                }

            }

        }

        $data = [
            'rows' => $structured_data,
            'base_price_total' => number_format($base_price_total, 2),
            'srp_total' => number_format($srp_total, 2),
            'failed' => $failed
        ];

        return $data;
    }
}

