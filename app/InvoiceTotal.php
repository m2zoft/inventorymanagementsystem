<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTotal extends Model
{

    protected $table = "invoice_totals";

    protected static $invoice = 'App\Invoice';

    protected $fillable = [
        'invoice_id',
        'code',
        'name',
        'amount',
        'sort_order',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function invoice()
    {
    	return $this->hasOne(static::$invoice, 'id', 'invoice_id');
    }
       
}
