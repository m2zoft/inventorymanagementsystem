<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateAdminAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:account-generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = [
            [
                'name' => 'Nico',
                'password' => '11223344**',
                'email' => 'admin@abays-store.sproutspayments.com',
            ]
        ];

        foreach ($users as $user) {
            try {
                User::updateOrCreate([
                    'email' => 'admin@abays-store.sproutspayments.com',
                ], [
                    'name' => 'Nico',
                    'email' => 'admin@abays-store.sproutspayments.com',
                    'password' => bcrypt('11223344**'),
                ]);
                Log::info('User has been created.');
            } catch (\Exception $e) {
                Log::info('Failed to create user.');
            }
        }

    }
}
