<?php

namespace App\Console\Commands;

use App\Account;
use App\TransactionType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RunDefaultValues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:defaults';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = [
            [
                'code' => 'S',
                'name' => 'Sales',
                'type' => 'credit'
            ],
            [
                'code' => 'EX',
                'name' => 'Expense',
                'type' => 'debit'
            ],
            [
                'code' => 'IA',
                'name' => 'Inventory Adjustments',
                'type' => 'credit'
            ],
            [
                'code' => 'AR',
                'name' => 'Receivable',
                'type' => 'credit'
            ]
        ];

        $transaction_types = [
            [
                'code' => 'MIG',
                'name' => 'Migrate',
            ],
            [
                'code' => 'O',
                'name' => 'Order',
            ],
            [
                'code' => 'R',
                'name' => 'Return',
            ],
            [
                'code' => 'SA',
                'name' => 'Stock Adjustment',
            ],
            [
                'code' => 'RE',
                'name' => 'Received',
            ],
            [
                'code' => 'D',
                'name' => 'Discounts',
            ]
        ];


        foreach ($accounts as $account) {
            try {
                Account::updateOrCreate([
                    'code' => $account['code'],
                ], $account);
            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getLine() . ' | ' . $e->getFile());
            }
        }

        foreach ($transaction_types as $type) {
            try {
                TransactionType::updateOrCreate([
                    'code' => $type['code'],
                ], $type);
            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getLine() . ' | ' . $e->getFile());
            }
        }
    }
}
