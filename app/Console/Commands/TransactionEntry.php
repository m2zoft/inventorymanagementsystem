<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\InternalScript\TransactionEntry as TransactionEntryController;

class TransactionEntry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction-entry:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migration of transaction entry for item names';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new TransactionEntryController)->index();
        return 'Transaction Entry migration is now processing...';
    }
}
