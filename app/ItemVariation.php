<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemVariation extends Model
{
    //
    protected $table = "item_variations";

    protected static $item = 'App\Item';

    protected static $unit = 'App\Unit';

    protected $fillable = [
        'item_id',
        'base_unit_id',
        'base_price',
        'selling_price',
        'price_margin',
        'name',
        'category_id',
        'group_id',
        'brand_id',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function item()
    {
    	return $this->hasOne(static::$item, 'id', 'item_id');
    }  

    public function base_unit()
    {
    	return $this->hasOne(static::$unit, 'id', 'base_unit_id');
    }

    public function inventory()
    {
        return $this->belongsTo(\App\Inventory::class);
    }
}
