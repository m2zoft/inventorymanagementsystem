<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandManufacturer extends Model
{
    protected $table = "brand_manufacturers";
}
