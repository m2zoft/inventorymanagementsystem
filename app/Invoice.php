<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table = "invoices";

    protected static $customer  = 'App\Customer';

    protected static $item      = 'App\InvoiceItem';

    protected static $history   = 'App\InvoiceHistory';

    protected static $total     = 'App\InvoiceTotal';

    protected static $payment     = 'App\InvoicePayment';

    protected $fillable = [
        'invoice_number',
        'description',
        'invoiced_at',
        'due_at',
        'amount',
        'customer_id',
        'status_code',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function customer()
    {
    	return $this->hasOne(static::$customer, 'id', 'customer_id');
    }

    public function items()
    {
        return $this->hasMany(static::$item, 'invoice_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(static::$history, 'invoice_id', 'id');
    }

    public function totals()
    {
        return $this->hasMany(static::$total, 'invoice_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(static::$payment, 'invoice_id', 'id');
    }
}
