<?php

namespace App\System;

use Illuminate\Database\Eloquent\Model;

class SystemUpdate extends Model
{
    protected $table = "system_updates";
}
