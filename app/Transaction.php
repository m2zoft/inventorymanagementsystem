<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = "transactions";

    protected static $account = 'App\Account';

    protected static $transactionType = 'App\TransactionType';

    protected static $status = 'App\Status';

    protected static $transactionEntry = 'App\TransactionEntry';

    protected static $customer = 'App\Customer';

    protected static $supplier = 'App\Supplier';

    protected $fillable = [
        'or_number',
        'transaction_number',
        'account_id',
        'transaction_type_id',
        'status_id',
        'to',
        'from',
        'total_debit',
        'total_credit',
        'total_balance',
        'transaction_date',
        'created_by',
    ];

    protected $dates = ['created_at', 'updated_at'];    

    public function account()
    {
    	return $this->hasOne(static::$account, 'id', 'account_id');
    }

    public function transaction_type()
    {
    	return $this->hasOne(static::$transactionType, 'id', 'transaction_type_id');
    }

    public function status()
    {
    	return $this->hasOne(static::$status, 'id', 'status_id');
    }

    public function entries()
    {
        return $this->hasMany(static::$transactionEntry, 'transaction_id', 'id');
    }


    public function to_customer()
    {
        return $this->hasOne(static::$customer, 'id', 'to');
    }

    public function to_supplier()
    {
        return $this->hasOne(static::$supplier, 'id', 'to');
    }

    public function from_supplier()
    {
        return $this->hasOne(static::$supplier, 'id', 'from');
    }

    public function from_customer()
    {
        return $this->hasOne(static::$customer, 'id', 'from');
    }

    public static function generateTransactionNumber()
    {
        return date('ymdhis');
    }

    public function scopefindByOrNumber($query, $or_number)
    {
        return $query->where('or_number', $or_number);
    }
}
