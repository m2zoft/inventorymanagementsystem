<?php 

namespace App\Repositories;

use App\InvoiceHistory;
use App\Contracts\InvoiceHistoryContract;
/**
 * Payment History Repository
 */
class InvoiceHistoryRepository implements InvoiceHistoryContract
{

	protected $model;

	function __construct(InvoiceHistory $model)
	{
		$this->model = $model;
	}

	public function store($data = [])
	{
		return $this->model->create($data);
	}
}