<?php namespace App\Statistics;

use App\Statistics\StatisticInterface;
use App\Transaction;
use App\TransactionEntry;
use DB;

class SalesStatistic implements StatisticInterface
{
	protected $parameters;

	function __construct(array $parameters)
	{
		$this->parameters = $parameters;
	}

	private function sales(string $start_date, string $end_date)
	{

		$sales = Transaction::where(function($query){ 
			$query->where('transaction_type_id', 1);
			$query->orWhere('transaction_type_id', 2);
		})
	
		->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])	

		->where('account_id', 1)

		->sum('total_balance');

		return ['formatted' => number_format($sales, 2), 'normal' => $sales];
	}

	private function orders()
	{
		
	}

	private function transactions(string $start_date, string $end_date)
	{
		$transactions = Transaction::where(function($query){ 
			$query->where('transaction_type_id', 1);
			$query->orWhere('transaction_type_id', 2);
		})
	
		->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])	

		->where('account_id', 1)

		->count();

		return ['formatted' => number_format($transactions), 'normal' => $transactions];		
	}	

	private function items(string $start_date, string $end_date)
	{
		$total = 0;

		$items = Transaction::with(['entries'])->where(function($query){ 
			$query->where('transaction_type_id', 1);
			$query->orWhere('transaction_type_id', 2);
		})
	
		->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])	

		->where('account_id', 1)

		->get();

		foreach ($items as $item) {
			foreach ($item['entries'] as $entry) {
				$total += $entry['quantity'];
			}
		}


		return ['formatted' => number_format($total), 'normal' => $total];		
	}

	private function discounts(string $start_date, string $end_date)
	{
		$discounts = Transaction::where(function($query){ 
			$query->where('transaction_type_id', 1);
			$query->orWhere('transaction_type_id', 2);
		})
	
		->whereBetween(DB::raw('DATE(transaction_date)'), [$start_date, $end_date])	

		->where('account_id', 1)

		->sum('total_debit');

		return ['formatted' => number_format($discounts, 2), 'normal' => $discounts];		
	}		

	public function total()
	{
		$sales = $this->sales($this->parameters['start_date'], $this->parameters['end_date']);
		$transactions = $this->transactions($this->parameters['start_date'], $this->parameters['end_date']);
		$items = $this->items($this->parameters['start_date'], $this->parameters['end_date']);
		$discounts = $this->discounts($this->parameters['start_date'], $this->parameters['end_date']);

		return ['total_sales' => $sales, 'total_transactions' => $transactions, 'total_items' => $items, 'total_discounts' => $discounts];
	}
}