<?php namespace App\Statistics;

use  App\Statistics\StatisticInterface;

class Statistic
{
	protected $statistic;

	function __construct(
		StatisticInterface $statistic
	){
		$this->statistic = $statistic;
	}

	public function total()
	{
		return $this->statistic->total();
	}

}