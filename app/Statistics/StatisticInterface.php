<?php namespace App\Statistics;

interface StatisticInterface
{
	public function total();
}
