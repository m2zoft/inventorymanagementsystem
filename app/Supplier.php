<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected $table = "suppliers";

    protected $fillable = ['code', 'name', 'status'];

    protected $dates = ['created_at', 'updated_at'];
}
