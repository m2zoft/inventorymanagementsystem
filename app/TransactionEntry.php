<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionEntry extends Model
{
    //
    protected $table = "transaction_entries";

    protected static $transaction = 'App\Transaction';

    protected static $itemVariation = 'App\ItemVariation';

    protected static $unit = 'App\Unit';    

    protected $fillable = [
        'transaction_id',
        'item_variation_id',
        'item_name',
        'item_variation_name',
        'base_unit_id',
        'quantity',
        'price',
        'total_price',
        'discount',
        'base_price',
    ];

    protected $dates = ['created_at', 'updated_at'];    

    public function transaction()
    {
    	return $this->hasOne(static::$transaction, 'id', 'transaction_id');
    }

    public function item_variation()
    {
    	return $this->hasOne(static::$itemVariation, 'id', 'item_variation_id');
    }

    public function base_unit()
    {
        return $this->hasOne(static::$unit, 'id', 'base_unit_id');
    }

    public function setItemVariationIdAttribute($value)
    {
        $this->attributes['item_variation_id'] = $value;
        $this->attributes['item_name'] = $this->item_variation->item->name;
        $this->attributes['item_variation_name'] = $this->item_variation->name;
    }
    
}
