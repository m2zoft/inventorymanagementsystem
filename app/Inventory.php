<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = "inventories";

    protected static $itemVariation = 'App\ItemVariation';

    protected static $itemVariationHistory = 'App\ItemVariationHistory';

    protected $fillable = [
        'item_variation_id',
        'stocks',
        'reorder_point',
    ];

    protected $dates = ['created_at', 'updated_at'];    

    public function item_variation()
    {
    	return $this->hasOne(static::$itemVariation, 'id', 'item_variation_id');
    }

    public function item_variation_history()
    {
    	return $this->hasMany(static::$itemVariationHistory, 'id', 'item_variation_id');
    }    
    
}
