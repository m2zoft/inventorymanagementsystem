// angular.module('app', [ 'ui.bootstrap', 'ui.router', 'summernote', 'ngFileUpload', 'ngTagsInput', 'ngSanitize', 'User', 'vcRecaptcha', 'Campaign'])

angular.module('auth', ['ui.bootstrap', 'ui.router', 'ngCookies', 'Auth'])

.config(function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
})

.constant('BASE', {
    

    'API_URL': '/',
    
	// 'API_URL': window.location.protocol + '//inventory.local/',

    // 'API_URL': 'http://jmeinventory.tk/',
    
	'ASSETS_URL': 'assets/',
    
	'API_VERSION': 'api/v1',
    
})

.run(['$rootScope', 'BASE', '$state', function ($rootScope, BASE, $state) {


}])


.service('blockUi', [function () {
    this.open = function(element, message){
        App.blockUI({
            target: element,
            boxed: true,
            message: message
        });
    }

    this.close = function(element){
        App.unblockUI(element);        
    }
}])

