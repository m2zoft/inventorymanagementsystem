(function() {
    "use strict";

    angular.module('AngularVersioning', [])

        .factory('preventTemplateCache', function ($injector) {
            // var ENV = $injector.get('env');
            return {
                'request': function (config) {
                    if (config.url.indexOf('templates') !== -1) {
                        config.url = config.url + '?t=' + Date.now();
                    }
                    return config;
                }
            }
        })

})();
