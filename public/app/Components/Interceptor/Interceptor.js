(function(){
  "use strict";

  	angular.module('Interceptor', [])

  	.factory('userInterceptor', userInterceptor)


  	function userInterceptor($q, BASE, $cookies) {

	    var requestInterceptor = {
	    	request: function(config){
	    		if ($cookies.get('token')) {
	    			config.headers.Authorization = 'Bearer ' + $cookies.get('token');
	    		}
	    		return config;
	    	},
	        responseError: function(response) {

	            var deferred = $q.defer();
	            
	            if (response.status === 401) {
	            	window.location.href = BASE.API_URL + 'logout';
	            	deferred.reject(response);
	            }else{
	            	deferred.reject(response);	
	            }

	            return deferred.promise;
	        }
	    };

	    return requestInterceptor;
		
  	}

})();