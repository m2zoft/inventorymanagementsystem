(function(){
  "use strict";

  	angular.module('Auth', [])

  	/**
     *
     * Login User Repository
     *
     */
    
    .factory('LoginRepository', LoginRepository)
    
    .directive('login', loginDirective)
    
    function loginDirective(BASE) {
        return {
            restrict: 'EA',
            controller: loginDirectiveController,
            templateUrl: BASE.API_URL + 'app/Components/Auth/templates/login.html',
            link: function (scope, element, attributes) {  

            }
        }
    }

    function loginDirectiveController($scope, $timeout, LoginRepository, blockUi, $cookies) {

      $scope.isLoading = false;

      $scope.login = function(){

          blockUi.open('.content', 'Authenticating...');

          LoginRepository.authenticate($scope.user).then(function(response){

            $cookies.put('token', response.data.success.token);

            swal("Successful!", "You will redirected to your account. Please wait.", "success");

            $timeout(function(){
              document.location = '/admin/dashboard';
            }, 2000);
            
          }, function(error){

            $timeout(function(){
              swal("Failed!", "Your email/password is invalid. Please try again or contact BIZWEX.", "error");
              blockUi.close('.content');
            }, 2000);
            

          });
          
      }


    }



    /**
     *
     * Repository
     *
     */

    function LoginRepository(BASE, $http) {
        var url = BASE.API_URL;
        var repo = {};

        repo.authenticate = function(params){
            return $http.post(url + 'authenticate', params);
        }

        return repo;
    }

    

})();