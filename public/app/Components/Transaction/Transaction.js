(function(){
  "use strict";

  	angular.module('Transaction', [])

  	/**
     *
     * Registration User Repository
     *
     */
    
    .factory('TransactionRepository', TransactionRepository)
    
    


    /**
     *
     * User Service
     *
     */
    .service('TransactionService', TransactionService)

    /**
     *
     * Directives
     *
     */
    
    .directive('transaction', transactionDirective)
    
    function transactionDirective(BASE) {
      return {
          restrict: 'EA',
          controller: transactionDirectiveController,
          templateUrl: BASE.API_URL + 'app/Components/Transaction/templates/transactions.html',
          link: function (scope, element, attributes) {  

          }
      }
    }

    function transactionDirectiveController($scope, $timeout, TransactionRepository, TransactionService) {

      $scope.isLoading = false;

      $scope.doSearch = function(){
          $scope.data();
      }

      $scope.pagination = {
          totalItems: 0,
          currentPage: 1,
          itemsPerPage: 10,
          maxSize: 5,
          search: '',
          objects: [],
      }


      $scope.setPage = function (pageNo) {
          $scope.pagination.currentPage = pageNo;
      };

      $scope.pageChanged = function() {
          $scope.data();
      };

      $scope.data = function(){

          $scope.isLoading = true;

          let params = {
              page: $scope.pagination.currentPage,
              rows: $scope.pagination.itemsPerPage,
              search: $scope.pagination.search
          };
          
          TransactionRepository.paginate(params).then(function(response){
              $scope.pagination.objects = response.data.rows;
              $scope.pagination.totalItems = response.data.total;
              $scope.isLoading = false;
          });
          
      }

      $scope.setItemsPerPage = function(num) {
          $scope.pagination.itemsPerPage = num;
          $scope.pagination.currentPage = 1; //reset to first paghe
          $scope.data();
      }

      $timeout(function(){
          $scope.data();
      });


  		$scope.updatePrice = function(inventory){
  			TransactionService.updatePrice(inventory, $scope.data);
  		}

      $scope.addOrderTransaction = function(){
       TransactionService.addOrderTransaction($scope.data); 
      }

  		$scope.adjustStock = function(inventory){
  			TransactionService.adjustStock(inventory, $scope.data);
  		}

  		$scope.viewInventory = function(inventory){
  			TransactionService.viewInventory(inventory, $scope.data);
  		}


    }  



    /**
     *
     * Repository
     *
     */

    function TransactionRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/transaction';
        var repo = {};

        repo.paginate = function(params){
            return $http.get(url, {params});
        }

        return repo;
    }
    /**
     *
     * Services
     *
     */
    function TransactionService(InventoryRepository, $uibModal, BASE) {

  		this.updatePrice = function(inventory, callback = false){
  			$uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/update-item-variant-price.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.inventory = inventory;

            $scope.submit = function(){

            	$scope.isLoading = true;

            	InventoryRepository.updatePrice($scope.inventory).then(function(response){

                swal("Price Update Successful", "Updating the price of the item has been successful.", "success");
                
                $uibModalInstance.dismiss('close');

                if (callback) {
                  callback();
                }

                $scope.isLoading = false;
                     		
            	}, function(response_error){
            		swal("Price Update Failed", "Updating the price of the item has been failed.", "error");
            		$scope.isLoading = false;
            	})


            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'md'
        });
  		}

  		this.adjustStock = function(inventory, callback = false){

  		}

  		this.viewInventory = function(inventory, callback = false){

  		}

      this.searchAndSelectItems = function(item_lists, callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/item-list.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository){

            $scope.errors = [];
            $scope.item_lists = item_lists;

            $scope.isLoading = false;

            $scope.doSearch = function(){
                $scope.data();
            }

            $scope.pagination = {
                totalItems: 0,
                currentPage: 1,
                itemsPerPage: 10,
                maxSize: 5,
                search: '',
                objects: [],
            }

            $scope.setPage = function (pageNo) {
                $scope.pagination.currentPage = pageNo;
            };

            $scope.pageChanged = function() {
                $scope.data();
            };

            $scope.data = function(){

                $scope.isLoading = true;

                let params = {
                    page: $scope.pagination.currentPage,
                    rows: $scope.pagination.itemsPerPage,
                    search: $scope.pagination.search
                };
                
                InventoryRepository.paginate(params).then(function(response){
                    $scope.pagination.objects = response.data.rows;
                    $scope.pagination.totalItems = response.data.total;
                    $scope.isLoading = false;
                });
                
            }

            $scope.setItemsPerPage = function(num) {
                $scope.pagination.itemsPerPage = num;
                $scope.pagination.currentPage = 1; //reset to first paghe
                $scope.data();
            }

            $timeout(function(){
                $scope.data();
            });

            $scope.isInTheList = function(inventory_id){

              var selected_items = $filter('filter')($scope.item_lists, {'id': inventory_id}, true);

              var is_exist = false;

              if (selected_items.length > 0) {

                  is_exist = true;

              }

              return is_exist;
            }

            $scope.addToTheList = function(inventory){
              $scope.item_lists.push(inventory);
            }

            $scope.removeToTheList = function(inventory_id){

              let selected_items = $filter('filter')($scope.item_lists, {'id': inventory_id}, true);

              let index = $scope.item_lists.indexOf(selected_items);

              for (let i = 0; i < $scope.item_lists.length; i++) {
                  
                  if (inventory_id === $scope.item_lists[i].id) {

                      $scope.item_lists.splice(i, 1);

                  }

              }              

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

            $scope.addItem = function(){
              InventoryService.addItem($scope.data);
            }

          },
          size: 'lg'
        });

      }

      this.addStock = function(callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/add-stocks.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, UnitRepository, SupplierService, InventoryService){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.inventory = [];

            $scope.stock = {};

            $scope.default = {
              suppliers: [],
              units: []
            }

            $scope.getSuppliers = function(){
              SupplierRepository.get().then(function(response){ 
                $scope.default.suppliers = response.data; 
                $timeout(function(){
                    $('#supplier').selectpicker('refresh');
                    $('#units').selectpicker('refresh');
                });                
              });
            }

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.default.units = response.data; 
                $timeout(function(){
                    $('#units').selectpicker('refresh');
                });                  
              });
            }

            $scope.getSuppliers();
            $scope.getUnits();

            $timeout(function(){
              $('.date-picker').datepicker({
                  rtl: App.isRTL(),
                  orientation: "left",
                  autoclose: true
              });
            })

            $scope.addSupplier = function(getSuppliers){
              SupplierService.addSupplier(getSuppliers);
            }

            $scope.searchItem = function(){
              InventoryService.searchAndSelectItems($scope.inventory);
            }

            $scope.submit = function(){

              let transaction = {
                stock: $scope.stock,
                inventory: $scope.inventory
              }

              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');

              InventoryRepository.addStocks(transaction).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'lg'
        });

      }

      this.addItem = function(callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/add-item.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository, UnitRepository, ItemRepository){

            $scope.errors = [];
            $scope.isLoading = false;

            $scope.defaults = {
              categories: [],
              brands: [],
              groups: [],
              units: [],
            };

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.defaults.units = response.data; 
                $timeout(function(){
                    $('.units').selectpicker('refresh');
                });                  
              });
            }

            $scope.getUnits();            

            $scope.item = {
              name: '',
              item_variations: [{
                temp_id: 1,
                base_unit_id: 0,
                base_price: 0,
                selling_price: 0,
                name: '',
                category_id: 0,
                group_id: 0,
                brand_id: 0,
              }],
            };

            $scope.addRow = function(){
              let total_rows = $scope.item.item_variations.length;
              $scope.item.item_variations.push({
                temp_id: total_rows + 1,
                base_unit_id: 0,
                base_price: 0,
                selling_price: 0,
                name: '',
                category_id: 0,
                group_id: 0,
                brand_id: 0,
              });
            }

            $scope.removeRow = function(index){

              for (let i = 0; i < $scope.item.item_variations.length; i++) {
                  
                  if (index === i) {

                      $scope.item.item_variations.splice(i, 1);

                  }

              }              
            }

            $scope.submit = function(){
              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');              
              ItemRepository.createItemAndVariation($scope.item).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }


          },
          size: 'lg'
        });
      }

  	}
    

})();