(function(){
  "use strict";

  	angular.module('Relationship', [])
  	/**
     *
     * Repository
     *
     */
    
    .factory('CustomerRepository', CustomerRepository)
    .factory('SupplierRepository', SupplierRepository)
    /**
     *
     * Services
     *
     */
    .service('SupplierService', SupplierService)
    .service('CustomerService', CustomerService)


    function CustomerRepository(BASE, $http) {

        let url = BASE.API_URL + BASE.API_VERSION + '/customer';
        let repo = {};

        repo.get = function(){
            return $http.get(url + '/all');
        }        

        repo.paginate = function(params){
            return $http.get(url, {params});
        }

        repo.create = function(params){
            return $http.post(url, params);
        }        

        repo.updatePrice = function(params){
            return $http.post(url + '/update-price', params);
        }

        return repo;
    }

    function SupplierRepository(BASE, $http) {
        let url = BASE.API_URL + BASE.API_VERSION + '/supplier';
        let repo = {};

        repo.get = function(){
            return $http.get(url);
        }        

        repo.paginate = function(params){
            return $http.get(url, {params});
        }

        repo.create = function(params){
            return $http.post(url, params);
        }

        return repo;
    }


    function SupplierService(SupplierRepository, $uibModal, BASE) {
        this.addSupplier = function(callback = false){
            $uibModal.open({
              animation: true,
              templateUrl: BASE.API_URL + 'app/Components/Relationship/templates/add-supplier.html',
              backdrop: 'static',
              controller: function($uibModalInstance, $scope, blockUi){

                $scope.errors = [];
                $scope.isLoading = false;
                $scope.supplier = {};

                $scope.submit = function(){

                    $scope.isLoading = true;

                    SupplierRepository.create($scope.supplier).then(function(response){

                        swal("Success!", "New Supplier has been successfully added.", "success");

                        if (callback) {
                          callback();
                        }

                        $scope.isLoading = false;

                        $uibModalInstance.dismiss();                        
       
                    }, function(response_error){
                        swal("Error!", "There is an error on adding new supplier.", "error");
                        $scope.isLoading = false;
                    })


                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('close');
                }

              },
              size: 'md'
            });
        }
    }  
    function CustomerService(CustomerRepository, $uibModal, BASE) {

        this.addCustomer = function(callback = false){
          $uibModal.open({
            animation: true,
            templateUrl: BASE.API_URL + 'app/Components/Relationship/templates/add-customer.html',
            backdrop: 'static',
            controller: function($uibModalInstance, $scope, blockUi){

              $scope.errors = [];
              $scope.isLoading = false;
              $scope.customer = {
                first_name: '',
                last_name: '',
                mobile_number: '',
                address: '',
              };

              $scope.submit = function(){

                  $scope.isLoading = true;

                  CustomerRepository.create($scope.customer).then(function(response){

                      swal("Success!", "New Customer has been successfully added.", "success");

                      if (callback) {
                        callback();
                      }

                      $scope.isLoading = false;

                      $uibModalInstance.dismiss();                        
     
                  }, function(e){
                      angular.forEach(e.data.errors, function(val, index){
                          $scope.errors.push(val[0]);
                      })                      
                      swal("Error!", "There is an error on adding new customer.", "error");
                      $scope.isLoading = false;
                  })


              }

              $scope.cancel = function () {
                  $uibModalInstance.dismiss('close');
              }

            },
            size: 'md'
          });
        }
    }        

})();