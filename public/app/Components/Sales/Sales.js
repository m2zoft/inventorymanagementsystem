(function(){
  "use strict";

  	angular.module('Sales', ['ngAnimate', 'ui.bootstrap'])

  	/**
     *
     * Registration User Repository
     *
     */
    
    .factory('SalesRepository', SalesRepository)

    .factory('InvoiceRepository', InvoiceRepository)
    
    .factory('PayInvoiceRepository', PayInvoiceRepository)


    /**
     *
     * User Service
     *
     */
    .service('SalesService', SalesService)

    .service('InvoiceService', InvoiceService)

    /**
     *
     * Directives
     *
     */
    
    .directive('sales', salesDirective)
    .directive('invoices', Invoices)
    
    function salesDirective(BASE) {
      return {
        restrict: 'EA',
        controller: salesDirectiveController,
        templateUrl: BASE.API_URL + 'app/Components/Sales/templates/index.html',
        link: function (scope, element, attributes) {  

        }
      }
    }

    function salesDirectiveController($scope, $timeout, SalesRepository, SalesService) {

      $scope.isLoading = false;

      $scope.statistcs = {};

      $scope.doSearch = function(){
          $scope.data();
      }

      $scope.pagination = {
          totalItems: 0,
          currentPage: 1,
          itemsPerPage: 10,
          maxSize: 5,
          search: '',
          objects: [],
      }


      $scope.setPage = function (pageNo) {
          $scope.pagination.currentPage = pageNo;
      };

      $scope.pageChanged = function() {
          $scope.data();
      };

      $scope.data = function(){


          $scope.isLoading = true;

          let params = {
              page: $scope.pagination.currentPage,
              rows: $scope.pagination.itemsPerPage,
              search: $scope.pagination.search,
              daterange: $('#salesreport_date_range').val()
          };
          
          SalesRepository.paginateSalesOrder(params).then(function(response){
              $scope.pagination.objects = response.data.rows;
              $scope.pagination.totalItems = response.data.total;
              $scope.statistics = response.data.statistics;
              $scope.isLoading = false;
          });
          
      }

      $scope.setItemsPerPage = function(num) {
          $scope.pagination.itemsPerPage = num;
          $scope.pagination.currentPage = 1; //reset to first paghe
          $scope.data();
      }

      $timeout(function(){
          $('#reportrange').daterangepicker({
              opens: (App.isRTL() ? 'left' : 'right'),
              startDate: moment(),
              endDate: moment(),
              //minDate: '01/01/2012',
              //maxDate: '12/31/2014',
              dateLimit: {
                  days: 60
              },
              showDropdowns: true,
              showWeekNumbers: true,
              timePicker: false,
              timePickerIncrement: 1,
              timePicker12Hour: true,
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                  'Last 7 Days': [moment().subtract('days', 6), moment()],
                  'Last 30 Days': [moment().subtract('days', 29), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              buttonClasses: ['btn'],
              applyClass: 'green',
              cancelClass: 'default',
              format: 'MM/DD/YYYY',
              separator: ' to ',
              locale: {
                  applyLabel: 'Apply',
                  fromLabel: 'From',
                  toLabel: 'To',
                  customRangeLabel: 'Custom Range',
                  daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                  monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                  firstDay: 1
              }
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('#salesreport_date_range').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                $scope.data();
            }
          );
          //Set the initial state of the picker label
          $('#reportrange span').html(moment().format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
          $('#salesreport_date_range').val(moment().format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'));
          $scope.data();
      });


  		$scope.updatePrice = function(inventory){
  			SalesService.updatePrice(inventory, $scope.data);
  		}

      $scope.addOrderTransaction = function(){
       SalesService.addOrderTransaction($scope.data); 
      }

      $scope.viewOrderTransaction = function(obj){
       SalesService.viewOrderTransaction(obj); 
      }
    }  

    function Invoices(BASE) {
      return {
        restrict: 'EA',
        controller: InvoicesController,
        templateUrl: BASE.API_URL + 'app/Components/Sales/templates/invoice.html',
        link: function (scope, element, attributes) {  

        }
      }
    }

    function InvoicesController($scope, $timeout, InvoiceRepository, InvoiceService) {

      $scope.isLoading = false;

      $scope.doSearch = function(){
          $scope.data();
      }

      $scope.pagination = {
          totalItems: 0,
          currentPage: 1,
          itemsPerPage: 10,
          maxSize: 5,
          search: '',
          objects: [],
      }


      $scope.setPage = function (pageNo) {
          $scope.pagination.currentPage = pageNo;
      };

      $scope.pageChanged = function() {
          $scope.data();
      };

      $scope.data = function(){


          $scope.isLoading = true;

          let params = {
              page: $scope.pagination.currentPage,
              rows: $scope.pagination.itemsPerPage,
              search: $scope.pagination.search,
          };
          
          InvoiceRepository.get(params).then(function(response){
              let data = response.data.success;
              $scope.pagination.objects = data.data.rows;
              $scope.pagination.totalItems = data.data.total;
              $scope.isLoading = false;
          });
          
      }

      $scope.setItemsPerPage = function(num) {
          $scope.pagination.itemsPerPage = num;
          $scope.pagination.currentPage = 1; //reset to first paghe
          $scope.data();
      }

      $timeout(function(){
          $scope.data();
      });

      $scope.addInvoice = function(){
       InvoiceService.addInvoice($scope.data); 
      }

      $scope.viewInvoice = function(obj){
        InvoiceService.viewInvoice(obj, $scope.data); 
      }

      $scope.currency_format = function(n) {
        return parseFloat(n).toFixed(2).replace(/./g, function(c, i, a) {
          return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
      }
    }  

    /**
     *
     * Repository
     *
     */

    function SalesRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/sales';
        var repo = {};

        repo.paginateSalesOrder = function(params){
            return $http.get(url + '/paginate-sales-order', {params});
        }

        repo.createOrderTransaction = function(params){
            return $http.post(url + '/create-order-transaction', params);
        }        

        return repo;
    }

    function InvoiceRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/invoice';
        var repo = {};

        repo.get = function(params){
            return $http.get(url, {params});
        } 

        repo.show = function(params){
            return $http.get(url + '/' + params.id);
        }

        repo.create = function(params){
            return $http.post(url, params);
        }    

        return repo;
    }

    function PayInvoiceRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/invoice/pay';
        var repo = {};

        repo.get = function(params){
            return $http.get(url, {params});
        } 

        repo.show = function(params){
            return $http.get(url + '/' + params.id);
        }

        repo.create = function(params){
            return $http.post(url, params);
        }    

        return repo;
    }


    /**
     *
     * Services
     *
     */
    function SalesService(InventoryRepository, $uibModal, BASE) {

  		this.updatePrice = function(inventory, callback = false){
  			$uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/update-item-variant-price.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.inventory = inventory;

            $scope.submit = function(){

            	$scope.isLoading = true;

            	InventoryRepository.updatePrice($scope.inventory).then(function(response){

                swal("Price Update Successful", "Updating the price of the item has been successful.", "success");
                
                $uibModalInstance.dismiss('close');

                if (callback) {
                  callback();
                }

                $scope.isLoading = false;
                     		
            	}, function(response_error){
            		swal("Price Update Failed", "Updating the price of the item has been failed.", "error");
            		$scope.isLoading = false;
            	})


            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'md'
        });
  		}

  		this.adjustStock = function(inventory, callback = false){

  		}

  		this.viewInventory = function(inventory, callback = false){

  		}

      this.searchAndSelectItems = function(item_lists, callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/item-list.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository){

            $scope.errors = [];
            $scope.item_lists = item_lists;

            $scope.isLoading = false;

            $scope.doSearch = function(){
                $scope.data();
            }

            $scope.pagination = {
                totalItems: 0,
                currentPage: 1,
                itemsPerPage: 10,
                maxSize: 5,
                search: '',
                objects: [],
            }

            $scope.setPage = function (pageNo) {
                $scope.pagination.currentPage = pageNo;
            };

            $scope.pageChanged = function() {
                $scope.data();
            };

            $scope.data = function(){

                $scope.isLoading = true;

                let params = {
                    page: $scope.pagination.currentPage,
                    rows: $scope.pagination.itemsPerPage,
                    search: $scope.pagination.search
                };
                
                InventoryRepository.paginate(params).then(function(response){
                    $scope.pagination.objects = response.data.rows;
                    $scope.pagination.totalItems = response.data.total;
                    $scope.isLoading = false;
                });
                
            }

            $scope.setItemsPerPage = function(num) {
                $scope.pagination.itemsPerPage = num;
                $scope.pagination.currentPage = 1; //reset to first paghe
                $scope.data();
            }

            $timeout(function(){
                $scope.data();
            });

            $scope.isInTheList = function(inventory_id){

              var selected_items = $filter('filter')($scope.item_lists, {'id': inventory_id}, true);

              var is_exist = false;

              if (selected_items.length > 0) {

                  is_exist = true;

              }

              return is_exist;
            }

            $scope.addToTheList = function(inventory){
              $scope.item_lists.push(inventory);
            }

            $scope.removeToTheList = function(inventory_id){

              let selected_items = $filter('filter')($scope.item_lists, {'id': inventory_id}, true);

              let index = $scope.item_lists.indexOf(selected_items);

              for (let i = 0; i < $scope.item_lists.length; i++) {
                  
                  if (inventory_id === $scope.item_lists[i].id) {

                      $scope.item_lists.splice(i, 1);

                  }

              }              

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

            $scope.addItem = function(){
              InventoryService.addItem($scope.data);
            }

          },
          size: 'lg'
        });

      }

      this.addOrderTransaction = function(callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Sales/templates/add-orders.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, CustomerRepository, UnitRepository, CustomerService, InventoryService, SalesRepository, InvoiceService){

            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth()+1;
            let yyyy = today.getFullYear();

            if (dd < 10) { dd = '0'+dd; }
            if (mm < 10) { mm = '0'+mm; }

            today = dd + '-' + mm + '-' + yyyy;

            $scope.isLoading = false;
            $scope.results = [];

            $scope.default = {
              customers: [],
              units: []
            }

            $scope.sales = {
              invoice_number: '',
              transaction_date: today,
              due_date: today,
              customer: {},
              items: [],
              sub_total: 0,
              discount: 0,
              grand_total: 0,
            }

            $scope.item = {
              inventory_id: 0,
              item_variation_id: 0,
              base_unit: {},
              base_price: 0.00,
              selling_price: 0.00,
              item_id: 0,
              stock: 0,
              name: '',
              quantity: 0,
              discount: 0.00,
              total_price: 0.00,
            };

            $scope.getCustomers = function(){
              CustomerRepository.get().then(function(response){ 
                $scope.default.customers = response.data; 
                $timeout(function(){
                    $('#customer').selectpicker('refresh');
                });
              });
            }

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.default.units = response.data; 
                $timeout(function(){
                    $('#units').selectpicker('refresh');
                });                  
              });
            }

            $scope.add_item = function(){
              $scope.sales.items.push($scope.item);
            }

            // Search Items            
            $scope.searchItem = function(index) {

              InvoiceService.searchItem($scope.sales.items, index, function(){
                $scope.computeTotal();
              });

            };

            $scope.removeItemRow = function(index){
              $scope.sales.items.splice(index, 1);
              $scope.computeTotal();
            }

            $timeout(function(){
              $scope.getCustomers();
              $scope.getUnits();
              $scope.add_item();
              $('.date-picker').datepicker({
                  rtl: App.isRTL(),
                  orientation: "left",
                  autoclose: true
              });
            })

            $scope.computeTotal = function(){

              $scope.sales.sub_total    = 0.00;
              $scope.sales.discount     = 0.00;
              $scope.sales.grand_total  = 0.00;

              $.each($scope.sales.items, function(index, val) {

                if (val['stock'] < val['quantity']) {
                  swal("Out of Stock!", 'This item will be out of stock once you input the ' + val['quantity'] + ' quantity. The available stock is ' + val['stock'] + ' only.', "error");
                  val['quantity'] = 1;
                }

                if (val['discount'] > 0) {
                  val['total_price'] = (val['quantity'] * val['selling_price']) - val['discount'];
                }else{
                  val['total_price'] = val['quantity'] * val['selling_price'];
                }

                $scope.sales.sub_total    += val['quantity'] * val['selling_price'];
                $scope.sales.discount     += val['discount'];
                $scope.sales.grand_total  += val['total_price'];
                 
              });
            }

            $scope.addCustomer = function(){
              CustomerService.addCustomer($scope.getCustomers);
            }

            $scope.currency_format = function(n) {
              return n.toFixed(2).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
              });
            }

            $scope.submit = function(){


              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');

              SalesRepository.createOrderTransaction($scope.sales).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", e.data.message, "error");
                blockUi.close('.modal-body');

              });

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }


            

          },
          size: 'lg'
        });

      }

      this.viewOrderTransaction = function(transaction){


        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Sales/templates/view-transaction.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, CustomerRepository, UnitRepository, CustomerService, InventoryService, SalesRepository){

            $scope.transaction = transaction;
            $scope.isLoading = false;

            $scope.total = {
              sub: parseFloat($scope.transaction.total_balance, 2),
              discount: parseFloat($scope.transaction.total_debit, 2),
              grand: parseFloat($scope.transaction.total_balance, 2),
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'lg'
        });
      }

      this.addItem = function(callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/add-item.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository, UnitRepository, ItemRepository){

            $scope.errors = [];
            $scope.isLoading = false;

            $scope.defaults = {
              categories: [],
              brands: [],
              groups: [],
              units: [],
            };

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.defaults.units = response.data; 
                $timeout(function(){
                    $('.units').selectpicker('refresh');
                });                  
              });
            }

            $scope.getUnits();            

            $scope.item = {
              name: '',
              item_variations: [{
                temp_id: 1,
                base_unit_id: 0,
                base_price: 0,
                selling_price: 0,
                name: '',
                category_id: 0,
                group_id: 0,
                brand_id: 0,
              }],
            };

            $scope.addRow = function(){
              let total_rows = $scope.item.item_variations.length;
              $scope.item.item_variations.push({
                temp_id: total_rows + 1,
                base_unit_id: 0,
                base_price: 0,
                selling_price: 0,
                name: '',
                category_id: 0,
                group_id: 0,
                brand_id: 0,
              });
            }

            $scope.removeRow = function(index){

              for (let i = 0; i < $scope.item.item_variations.length; i++) {
                  
                  if (index === i) {

                      $scope.item.item_variations.splice(i, 1);

                  }

              }              
            }

            $scope.submit = function(){
              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');              
              ItemRepository.createItemAndVariation($scope.item).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }


          },
          size: 'lg'
        });
      }

  	}


    function InvoiceService(InventoryRepository, InvoiceRepository, $uibModal, BASE) {
      this.addInvoice = function(callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Sales/templates/create-invoice.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, CustomerRepository, UnitRepository, InvoiceService, SalesRepository, CustomerService){

            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth()+1;
            let yyyy = today.getFullYear();

            if (dd < 10) { dd = '0'+dd; }
            if (mm < 10) { mm = '0'+mm; }

            today = dd + '-' + mm + '-' + yyyy;

            $scope.isLoading = false;
            $scope.results = [];

            $scope.default = {
              customers: [],
              units: []
            }

            $scope.invoice = {
              invoice_number: '',
              transaction_date: today,
              due_date: today,
              customer: {},
              items: [],
              sub_total: 0,
              discount: 0,
              grand_total: 0,
            }

            $scope.item = {
              inventory_id: 0,
              item_variation_id: 0,
              base_unit: {},
              base_price: 0.00,
              selling_price: 0.00,
              item_id: 0,
              stock: 0,
              name: '',
              quantity: 0,
              discount: 0.00,
              total_price: 0.00,
            };

            $scope.getCustomers = function(){
              CustomerRepository.get().then(function(response){ 
                $scope.default.customers = response.data; 
                $timeout(function(){
                    $('#customer').selectpicker('refresh');
                });
              });
            }

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.default.units = response.data; 
                $timeout(function(){
                    $('#units').selectpicker('refresh');
                });                  
              });
            }

            $scope.add_item = function(){
              $scope.invoice.items.push($scope.item);
            }

            // Search Items            
            $scope.searchItem = function(index) {

              InvoiceService.searchItem($scope.invoice.items, index, function(){
                $scope.computeTotal();
              });

            };

            $scope.removeItemRow = function(index){
              $scope.invoice.items.splice(index, 1);
              $scope.computeTotal();
            }

            $timeout(function(){
              $scope.getCustomers();
              $scope.getUnits();
              $scope.add_item();
              $('.date-picker').datepicker({
                  rtl: App.isRTL(),
                  orientation: "left",
                  autoclose: true
              });
            })

            $scope.computeTotal = function(){

              $scope.invoice.sub_total    = 0.00;
              $scope.invoice.discount     = 0.00;
              $scope.invoice.grand_total  = 0.00;

              $.each($scope.invoice.items, function(index, val) {

                if (val['stock'] < val['quantity']) {
                  swal("Out of Stock!", 'This item will be out of stock once you input the ' + val['quantity'] + ' quantity. The available stock is ' + val['stock'] + ' only.', "error");
                  val['quantity'] = 1;
                }

                if (val['discount'] > 0) {
                  val['total_price'] = (val['quantity'] * val['selling_price']) - val['discount'];
                }else{
                  val['total_price'] = val['quantity'] * val['selling_price'];
                }

                $scope.invoice.sub_total    += val['quantity'] * val['selling_price'];
                $scope.invoice.discount     += val['discount'];
                $scope.invoice.grand_total  += val['total_price'];
                 
              });
            }

            $scope.addCustomer = function(){
              CustomerService.addCustomer($scope.getCustomers);
            }

            $scope.currency_format = function(n) {
              return n.toFixed(2).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
              });
            }

            $scope.submit = function(){

              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Loading...');

              InvoiceRepository.create($scope.invoice).then(function(response){
                
                swal("Successful!", response.data.success.message, "success");

                $scope.isLoading = false;
                blockUi.close('.modal-body');

                if (callback) {
                  callback();
                }

                $scope.cancel();

              }, function(error){

                swal("Failed!", error.data.error.message, "error");

                $scope.isLoading = false;
                blockUi.close('.modal-body');

              });

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'lg'
        });

      }

      this.searchItem = function(items, index, callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Sales/templates/search-items.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository, UnitRepository, ItemRepository){

            $scope.item_list = [];

            $timeout(function(){
              $scope.getItems();
            });

            $scope.getItems = function(){

              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Loading...');

              let params = {
                search: $scope.search_input
              };

              InventoryRepository.paginate(params).then(function(response){
                
                $scope.item_list = response.data.rows;

                blockUi.close('.modal-body');
                $scope.isLoading = false;

              });
            }

            $scope.selectItem = function(selected_item){

              if (selected_item.stocks <= 0) {
                swal("Cancelled!", 'Out of stock! You cannot add or select this item to sell.', "error");
              }else{
                items[index] = {
                  inventory_id: selected_item.id,
                  item_variation_id: selected_item.item_variation.id,
                  base_unit: selected_item.item_variation.base_unit,
                  base_price: selected_item.item_variation.base_price,
                  selling_price: selected_item.item_variation.selling_price,
                  item_id: selected_item.item_variation.item.id,
                  stock: selected_item.stocks,
                  name: selected_item.item_variation.name,
                  quantity: 1,
                  total_price: selected_item.item_variation.selling_price,
                  discount: 0.00,
                }

                if (callback) {
                  callback();
                }

                $scope.cancel();
              }

            }

            $scope.submit = function(){
              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');              
              ItemRepository.createItemAndVariation($scope.item).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            }

          },
          size: 'lg'
        });
      }

      this.viewInvoice = function(obj, callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Sales/templates/create-sales-invoice-payment.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, CustomerRepository, UnitRepository, InvoiceService, SalesRepository, CustomerService, PayInvoiceRepository){

            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth()+1;
            let yyyy = today.getFullYear();

            if (dd < 10) { dd = '0'+dd; }
            if (mm < 10) { mm = '0'+mm; }

            today = dd + '-' + mm + '-' + yyyy;

            $scope.isLoading = false;
            $scope.results = [];

            $scope.default = {
              customers: [],
              units: []
            }


            $scope.invoice = {
              id: 0,
              invoice_number: '',
              transaction_date: today,
              due_date: today,
              customer: {},
              items: [],
              payments: [],
              status_code: '',
              sub_total: 0,
              discount: 0,
              grand_total: 0,
              balance: 0,
            }

            $scope.item = {
              inventory_id: 0,
              item_variation_id: 0,
              base_unit: {},
              base_price: 0.00,
              selling_price: 0.00,
              item_id: 0,
              stock: 0,
              name: '',
              quantity: 0,
              discount: 0.00,
              total_price: 0.00,
            };


            $scope.payment = {
              invoice_id: $scope.invoice.id,
              amount: ''
            };

            $scope.calendar = function(){
              $('.date-picker').datepicker({
                  rtl: App.isRTL(),
                  orientation: "left",
                  autoclose: true
              });
            }

            $scope.computeTotal = function(is_callback = false){

              $scope.invoice.sub_total    = 0.00;
              $scope.invoice.discount     = 0.00;
              $scope.invoice.grand_total  = 0.00;
              $scope.invoice.balance      = 0.00;

              $.each($scope.invoice.items, function(index, val) {

                if (val['discount'] > 0) {
                  val['total_price'] = (val['quantity'] * val['selling_price']) - val['discount'];
                }else{
                  val['total_price'] = val['quantity'] * val['selling_price'];
                }

                $scope.invoice.sub_total    += val['quantity'] * val['selling_price'];
                $scope.invoice.discount     += val['discount'];
                $scope.invoice.grand_total  += val['total_price'];
                 
              });

              $scope.invoice.balance = $scope.invoice.grand_total;

              if ($scope.invoice.payments.length > 0) {
                $.each($scope.invoice.payments, function(index, val) {

                  $scope.invoice.balance -= val['amount'];
                   
                });
              }

              console.log('--- Computation of Total ---');
              console.log('Payment Amount:'+$scope.payment.amount);
              console.log('Balance:'+$scope.invoice.balance);
              console.log('--- End of Computation of Total ---');

              if (!is_callback) {
                if ($scope.payment.amount > $scope.invoice.balance) {
                  swal("Over Payment!", 'You can pay exact amount or less than your current balance.', "error");
                  $scope.payment.amount = '';
                }else{
                  $scope.invoice.balance -= $scope.payment.amount;
                }
              }
              
            }

            $scope.currency_format = function(n) {
              return parseFloat(n).toFixed(2).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
              });
            }

            $scope.submit = function(){

              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Loading...');

              $scope.payment.invoice_id = $scope.invoice.id;
              $scope.payment.transaction_date = $scope.invoice.transaction_date;

              PayInvoiceRepository.create($scope.payment).then(function(response){
                
                swal("Successful!", response.data.success.message, "success");

                $scope.isLoading = false;
                blockUi.close('.modal-body');

                if (callback) {
                  callback();
                }

                $scope.loadInvoiceData(true);
                $scope.payment.amount = '';

              }, function(error){

                swal("Failed!", 'Amount is required', "error");

                $scope.isLoading = false;
                blockUi.close('.modal-body');

              });

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

            $scope.loadInvoiceData = function(is_callback){

              PayInvoiceRepository.show({id: obj.id}).then(function(response){

                let response_data = response.data.success.data.rows;
                  $scope.invoice.id             = response_data.id;
                  $scope.invoice.invoice_number = response_data.invoice_number;
                  $scope.invoice.due_date       = response_data.due_at;
                  $scope.invoice.customer       = response_data.customer;
                  $scope.invoice.items          = response_data.items;
                  $scope.invoice.payments       = response_data.payments;
                  $scope.invoice.status_code    = response_data.status_code;
                  $scope.computeTotal(is_callback);
              })
            }

          },
          size: 'lg'
        });

      }
    }
    

})();