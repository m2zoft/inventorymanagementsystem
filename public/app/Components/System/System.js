(function(){
  "use strict";

  	angular.module('System', [])

  	/**
     *
     * Login User Repository
     *
     */
    
    .factory('SystemUpdateRepository', SystemUpdateRepository)
    
    .directive('systemUpdate', SystemUpdate)
    
    function SystemUpdate(BASE) {
        return {
            restrict: 'EA',
            controller: SystemUpdateController,
            templateUrl: BASE.API_URL + 'app/Components/System/templates/index.html',
            link: function (scope, element, attributes) {  

            }
        }
    }

    function SystemUpdateController($scope, $timeout, SystemUpdateRepository, blockUi, $cookies) {

      $scope.isLoading = false;
      $scope.data = [];

      $timeout(function(){
        SystemUpdateRepository.get().then(function(response){
          $scope.data = response.data.success.data;
        });
      });

    }



    /**
     *
     * Repository
     *
     */

    function SystemUpdateRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/system/update';
        var repo = {};

        repo.get = function(){
            return $http.get(url);
        }

        return repo;
    }

    

})();