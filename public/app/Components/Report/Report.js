(function(){
  "use strict";

  	angular.module('Report', [])
    
    .factory('SalesReportRepository', SalesReportRepository)

    .factory('NetSalesRevenueReportRepository', NetSalesRevenueReportRepository)

    .factory('InventorySalesReportRepository', InventorySalesReportRepository)

    .factory('InventoryReportRepository', InventoryReportRepository)
    
    .directive('salesReport', SalesReport)

    .directive('netSalesRevenueReport', NetSalesRevenueReport)

    .directive('inventorySalesReport', InventorySalesReport)

    .directive('inventoryReport', InventoryReport)
    
    function SalesReport(BASE) {
        return {
            restrict: 'EA',
            controller: SalesReportController,
            templateUrl: BASE.API_URL + 'app/Components/Report/templates/sales-report.html',
            link: function (scope, element, attributes) {  

            }
        }
    }

    function SalesReportController($scope, $timeout, SalesReportRepository, blockUi, $cookies) {

      $scope.isLoading = false;
      $scope.data = [];

      $timeout(function(){
        $('#reportrange').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            startDate: moment().subtract('days', 6),
            endDate: moment(),
            //minDate: '01/01/2012',
            //maxDate: '12/31/2014',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
          },
          function (start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              $('#salesreport_date_range').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              $scope.generate();
          }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 6).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#salesreport_date_range').val(moment().subtract('days', 6).format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'));
        
        $scope.generate();
      });

      $scope.load_data = function(){

        function showChartTooltip(x, y, xValue, yValue) {
            $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 40,
                border: '0px solid #ccc',
                padding: '2px 6px',
                'background-color': '#fff'
            }).appendTo("body").fadeIn(200);
        }
        
        //site activities
        var previousPoint2 = null;
        $('#site_activities_loading').hide();
        $('#site_activities_content').show();

        var data1 = $scope.data;

        var plot_statistics = $.plot($("#site_activities"),

          [{
              data: data1,
              lines: {
                  fill: 0.2,
                  lineWidth: 0,
              },
              color: ['#BAD9F5']
          }, {
              data: data1,
              points: {
                  show: true,
                  fill: true,
                  radius: 4,
                  fillColor: "#9ACAE6",
                  lineWidth: 2
              },
              color: '#9ACAE6',
              shadowSize: 1
          }, {
              data: data1,
              lines: {
                  show: true,
                  fill: false,
                  lineWidth: 3
              },
              color: '#9ACAE6',
              shadowSize: 0
          }],

          {

              xaxis: {
                  tickLength: 0,
                  tickDecimals: 0,
                  mode: "categories",
                  min: 0,
                  font: {
                      lineHeight: 18,
                      style: "normal",
                      variant: "small-caps",
                      color: "#6F7B8A"
                  }
              },
              yaxis: {
                  ticks: 5,
                  tickDecimals: 0,
                  tickColor: "#eee",
                  font: {
                      lineHeight: 14,
                      style: "normal",
                      variant: "small-caps",
                      color: "#6F7B8A"
                  }
              },
              grid: {
                  hoverable: true,
                  clickable: true,
                  tickColor: "#eee",
                  borderColor: "#eee",
                  borderWidth: 1
              }
          });

          $("#site_activities").bind("plothover", function(event, pos, item) {
              $("#x").text(pos.x.toFixed(2));
              $("#y").text(pos.y.toFixed(2));
              if (item) {
                  if (previousPoint2 != item.dataIndex) {
                      previousPoint2 = item.dataIndex;
                      $("#tooltip").remove();
                      var x = item.datapoint[0].toFixed(2),
                          y = item.datapoint[1].toFixed(2);
                      showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                  }
              }
          });

          $('#site_activities').bind("mouseleave", function() {
              $("#tooltip").remove();
          });

      }

      $scope.generate = function(){
        let params = {
          daterange: $('#salesreport_date_range').val()
        }
        
        $scope.isLoading = true;

        SalesReportRepository.generate(params).then(function(response){
          let data = response.data;
          $scope.data = data.success.data;
          $scope.isLoading = false;
          $scope.load_data();

        });
      }


    }


    function NetSalesRevenueReport(BASE) {
        return {
            restrict: 'EA',
            controller: NetSalesRevenueReportController,
            templateUrl: BASE.API_URL + 'app/Components/Report/templates/net-sales-revenue-report.html',
            link: function (scope, element, attributes) {  

            }
        }
    }

    function NetSalesRevenueReportController($scope, $timeout, NetSalesRevenueReportRepository, blockUi, $cookies, BASE) {

      $scope.isLoading = false;
      $scope.data = [];

      $timeout(function(){
        $('#reportrange').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            startDate: moment().subtract('days', 6),
            endDate: moment(),
            //minDate: '01/01/2012',
            //maxDate: '12/31/2014',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
          },
          function (start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              $('#salesreport_date_range').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              $scope.generate();
          }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 6).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#salesreport_date_range').val(moment().subtract('days', 6).format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'));
        
        $scope.generate();
      });

      $scope.load_data = function(){

        function showChartTooltip(x, y, xValue, yValue) {
            $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 40,
                border: '0px solid #ccc',
                padding: '2px 6px',
                'background-color': '#fff'
            }).appendTo("body").fadeIn(200);
        }
        
        //site activities
        var previousPoint2 = null;
        $('#site_activities_loading').hide();
        $('#site_activities_content').show();

        var data1 = $scope.data;

        var plot_statistics = $.plot($("#site_activities"),

          [{
              data: data1,
              lines: {
                  fill: 0.2,
                  lineWidth: 0,
              },
              color: ['#BAD9F5']
          }, {
              data: data1,
              points: {
                  show: true,
                  fill: true,
                  radius: 4,
                  fillColor: "#9ACAE6",
                  lineWidth: 2
              },
              color: '#9ACAE6',
              shadowSize: 1
          }, {
              data: data1,
              lines: {
                  show: true,
                  fill: false,
                  lineWidth: 3
              },
              color: '#9ACAE6',
              shadowSize: 0
          }],

          {

              xaxis: {
                  tickLength: 0,
                  tickDecimals: 0,
                  mode: "categories",
                  min: 0,
                  font: {
                      lineHeight: 18,
                      style: "normal",
                      variant: "small-caps",
                      color: "#6F7B8A"
                  }
              },
              yaxis: {
                  ticks: 5,
                  tickDecimals: 0,
                  tickColor: "#eee",
                  font: {
                      lineHeight: 14,
                      style: "normal",
                      variant: "small-caps",
                      color: "#6F7B8A"
                  }
              },
              grid: {
                  hoverable: true,
                  clickable: true,
                  tickColor: "#eee",
                  borderColor: "#eee",
                  borderWidth: 1
              }
          });

          $("#site_activities").bind("plothover", function(event, pos, item) {
              $("#x").text(pos.x.toFixed(2));
              $("#y").text(pos.y.toFixed(2));
              if (item) {
                  if (previousPoint2 != item.dataIndex) {
                      previousPoint2 = item.dataIndex;
                      $("#tooltip").remove();
                      var x = item.datapoint[0].toFixed(2),
                          y = item.datapoint[1].toFixed(2);
                      showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                  }
              }
          });

          $('#site_activities').bind("mouseleave", function() {
              $("#tooltip").remove();
          });

      }

      $scope.generate = function(){
        let params = {
          daterange: $('#salesreport_date_range').val()
        }
        
        $scope.isLoading = true;

        NetSalesRevenueReportRepository.generate(params).then(function(response){
          let data = response.data;
          $scope.data = data.success.data;
          $scope.isLoading = false;
          $scope.load_data();

        });
      }

      $scope.export = function(){
        let date = $('#salesreport_date_range').val();
        let dates = date.split(' - ');
        window.open(BASE.API_URL + 'admin/report/revenue/net-sales/export-to-excel/' + dates[0] + '/' + dates[1], '_blank');
      }
    }

    function InventorySalesReport(BASE) {
        return {
            restrict: 'EA',
            controller: InventorySalesReportController,
            templateUrl: BASE.API_URL + 'app/Components/Report/templates/inventory-sales-report.html',
            link: function (scope, element, attributes) {  

            }
        }
    }

    function InventorySalesReportController($scope, $timeout, InventorySalesReportRepository, blockUi, $cookies, BASE) {

      $scope.isLoading = false;
      $scope.data = [];

      $timeout(function(){
        $('#reportrange').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            startDate: moment().subtract('days', 6),
            endDate: moment(),
            //minDate: '01/01/2012',
            //maxDate: '12/31/2014',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
          },
          function (start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              $('#salesreport_date_range').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              $scope.generate();
          }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 6).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#salesreport_date_range').val(moment().subtract('days', 6).format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'));
        
        $scope.generate();
      });

      $scope.load_data = function(){

        function showChartTooltip(x, y, xValue, yValue) {
            $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 40,
                border: '0px solid #ccc',
                padding: '2px 6px',
                'background-color': '#fff'
            }).appendTo("body").fadeIn(200);
        }
        
        //site activities
        var previousPoint2 = null;
        $('#site_activities_loading').hide();
        $('#site_activities_content').show();

        var data1 = $scope.data;

        var plot_statistics = $.plot($("#site_activities"),

          [{
              data: data1,
              lines: {
                  fill: 0.2,
                  lineWidth: 0,
              },
              color: ['#BAD9F5']
          }, {
              data: data1,
              points: {
                  show: true,
                  fill: true,
                  radius: 4,
                  fillColor: "#9ACAE6",
                  lineWidth: 2
              },
              color: '#9ACAE6',
              shadowSize: 1
          }, {
              data: data1,
              lines: {
                  show: true,
                  fill: false,
                  lineWidth: 3
              },
              color: '#9ACAE6',
              shadowSize: 0
          }],

          {

              xaxis: {
                  tickLength: 0,
                  tickDecimals: 0,
                  mode: "categories",
                  min: 0,
                  font: {
                      lineHeight: 18,
                      style: "normal",
                      variant: "small-caps",
                      color: "#6F7B8A"
                  }
              },
              yaxis: {
                  ticks: 5,
                  tickDecimals: 0,
                  tickColor: "#eee",
                  font: {
                      lineHeight: 14,
                      style: "normal",
                      variant: "small-caps",
                      color: "#6F7B8A"
                  }
              },
              grid: {
                  hoverable: true,
                  clickable: true,
                  tickColor: "#eee",
                  borderColor: "#eee",
                  borderWidth: 1
              }
          });

          $("#site_activities").bind("plothover", function(event, pos, item) {
              $("#x").text(pos.x.toFixed(2));
              $("#y").text(pos.y.toFixed(2));
              if (item) {
                  if (previousPoint2 != item.dataIndex) {
                      previousPoint2 = item.dataIndex;
                      $("#tooltip").remove();
                      var x = item.datapoint[0].toFixed(2),
                          y = item.datapoint[1].toFixed(2);
                      showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                  }
              }
          });

          $('#site_activities').bind("mouseleave", function() {
              $("#tooltip").remove();
          });

      }

      $scope.generate = function(){
        let params = {
          daterange: $('#salesreport_date_range').val()
        }
        
        $scope.isLoading = true;

        InventorySalesReportRepository.generate(params).then(function(response){
          let data = response.data;
          $scope.data = data.success.data;
          $scope.isLoading = false;
          $scope.load_data();

        });
      }

      $scope.export = function(){
        let date = $('#salesreport_date_range').val();
        let dates = date.split(' - ');
        window.open(BASE.API_URL + 'admin/report/sales/inventory-summary/export-to-excel/' + dates[0] + '/' + dates[1], '_blank');
      }
    }

    function InventoryReport(BASE) {
        return {
            restrict: 'EA',
            controller: InventoryReportController,
            templateUrl: BASE.API_URL + 'app/Components/Report/templates/inventory-report.html',
            link: function (scope, element, attributes) {

            }
        }
    }

    function InventoryReportController($scope, $timeout, InventoryReportRepository, blockUi, $cookies, BASE) {

        $scope.isLoading = false;
        $scope.data = [];

        $scope.generate = function(){
            $scope.isLoading = true;
            InventoryReportRepository.generate({}).then(function(response){
                let data = response.data;
                $scope.data = data.success.data;
                $scope.isLoading = false;
            });
        }

        $scope.export = function(){
            let date = new Date();
            window.open(BASE.API_URL + 'admin/report/inventory-summary/export-to-excel', '_blank');
        }
    }


    /**
     *
     * Repository
     *
     */

    function SalesReportRepository(BASE, $http) {
      var url = BASE.API_URL + BASE.API_VERSION + '/report/sales';
      var repo = {};

      repo.generate = function(params){
          return $http.get(url, {params});
      }

      return repo;
    }

    function NetSalesRevenueReportRepository(BASE, $http) {
      var url = BASE.API_URL + BASE.API_VERSION + '/report/revenue/net-sales';
      var repo = {};

      repo.generate = function(params){
          return $http.get(url, {params});
      }

      return repo;
    } 

    function InventorySalesReportRepository(BASE, $http) {
      var url = BASE.API_URL + BASE.API_VERSION + '/report/sales/inventory';
      var repo = {};

      repo.generate = function(params){
          return $http.get(url, {params});
      }

      return repo;
    }    

    function InventoryReportRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/report/inventory';
        var repo = {};

        repo.generate = function(params){
            return $http.get(url, {params});
        }

        return repo;
    }
    

})();