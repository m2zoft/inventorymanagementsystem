(function(){
  "use strict";

  	angular.module('Unit', [])

    /**
     *
     * Service
     *
     */
    
    .service('UnitService', UnitService)


    /**
     *
     * Services Function
     *
     */
    function UnitService(UnitRepository, $uibModal, BASE) {

      this.add = function(callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Settings/Units/templates/add-unit.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, $filter){

            $scope.errors = [];
            $scope.isLoading = false;

            $scope.submit = function(){
              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');

              UnitRepository.create($scope.unit).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }


          },
          size: 'md'
        });
      }

    }    


})();