(function(){
  "use strict";

  	angular.module('Inventory', [])

  	/**
     *
     * Registration User Repository
     *
     */
    
    .factory('InventoryRepository', InventoryRepository)
    .factory('UnitRepository', UnitRepository)
    .factory('ItemRepository', ItemRepository)
    .factory('ItemVariationRepository', ItemVariationRepository)
    /**
     *
     * User Service
     *
     */
    .service('InventoryService', InventoryService)

    /**
     *
     * Directives
     *
     */
    
    .directive('inventory', inventoryDirective)
    
    function inventoryDirective(BASE) {
        return {
            restrict: 'EA',
            controller: inventoryDirectiveController,
            templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/index.html',
            link: function (scope, element, attributes) {  

            }
        }
    }

    function inventoryDirectiveController($scope, $timeout, InventoryRepository, InventoryService) {

      $scope.isLoading = false;

      $scope.doSearch = function(){
          $scope.data();
      }

      $scope.pagination = {
          totalItems: 0,
          currentPage: 1,
          itemsPerPage: 10,
          maxSize: 5,
          search: '',
          objects: [],
      }


      $scope.setPage = function (pageNo) {
          $scope.pagination.currentPage = pageNo;
      };

      $scope.pageChanged = function() {
          $scope.data();
      };

      $scope.data = function(){

          $scope.isLoading = true;

          let params = {
              page: $scope.pagination.currentPage,
              rows: $scope.pagination.itemsPerPage,
              search: $scope.pagination.search
          };
          
          InventoryRepository.paginate(params).then(function(response){
              $scope.pagination.objects = response.data.rows;
              $scope.pagination.totalItems = response.data.total;
              $scope.isLoading = false;
          });
          
      }

      $scope.setItemsPerPage = function(num) {
          $scope.pagination.itemsPerPage = num;
          $scope.pagination.currentPage = 1; //reset to first paghe
          $scope.data();
      }

      $timeout(function(){
          $scope.data();
      });


  		$scope.updatePrice = function(inventory){
  			InventoryService.updatePrice(inventory, $scope.data);
  		}

      $scope.addStock = function(){
       InventoryService.addStock($scope.data); 
      }

  		$scope.adjustStock = function(inventory){
  			InventoryService.adjustStock(inventory, $scope.data);
  		}

  		$scope.viewInventory = function(inventory){
  			InventoryService.viewInventory(inventory, $scope.data);
  		}

      $scope.updateItemInformation = function(item_variation){
        InventoryService.updateItemInformation(item_variation, $scope.data);
      }
    }



    /**
     *
     * Repository
     *
     */

    function InventoryRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/inventory';
        var repo = {};

        repo.paginate = function(params){
            return $http.get(url, {params});
        }

        repo.updatePrice = function(params){
            return $http.post(url + '/update-price', params);
        }

        repo.create = function(params){
            return $http.post(url + '/create', params);
        }

        repo.addStocks = function(params){
            return $http.post(url + '/add-stocks', params);
        }

        repo.adjustStock = function(params){
            return $http.post(url + '/adjust-stock', params);
        }

        return repo;
    }

    function UnitRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/unit';
        var repo = {};

        repo.paginate = function(params){
            return $http.get(url, {params});
        }

        repo.get = function(){
            return $http.get(url + '/all');
        }

        repo.create = function(params){
            return $http.post(url, params);
        }        

        return repo;
    }    

    function ItemRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/item';
        var repo = {};

        repo.paginate = function(params){
            return $http.get(url, {params});
        }

        repo.all = function(){
            return $http.get(url + '/all');
        }

        repo.createItemAndVariation = function(params){
            return $http.post(url + '/create-item-and-variation', params);
        }        

        return repo;
    } 

    function ItemVariationRepository(BASE, $http) {
        var url = BASE.API_URL + BASE.API_VERSION + '/item/variation';
        var repo = {};

        repo.index = function(params){
            return $http.get(url, {params});
        }

        repo.update = function(id, params){
            return $http.patch(url + '/' + id, params);
        }

        return repo;
    }

    /**
     *
     * Services
     *
     */
    function InventoryService(InventoryRepository, ItemVariationRepository, $uibModal, BASE) {

  		this.updatePrice = function(inventory, callback = false){
  			$uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/update-item-variant-price.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.inventory = inventory;

            $scope.submit = function(){

            	$scope.isLoading = true;

            	InventoryRepository.updatePrice($scope.inventory).then(function(response){

                swal("Price Update Successful", "Updating the price of the item has been successful.", "success");
                
                $uibModalInstance.dismiss('close');

                if (callback) {
                  callback();
                }

                $scope.isLoading = false;
                     		
            	}, function(response_error){
            		swal("Price Update Failed", "Updating the price of the item has been failed.", "error");
            		$scope.isLoading = false;
            	})


            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'md'
        });
  		}

  		this.adjustStock = function(inventory, callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/adjust-stock.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, UnitRepository, $timeout){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.inventory = inventory;
            $scope.computation = {
              new_total: 0
            }
            $scope.adjustment = {
              type: 'add',
              base_unit_id: $scope.inventory.item_variation.base_unit_id,
              quantity: 0,
              inventory_id: $scope.inventory.id,
              item_variation_id: $scope.inventory.item_variation.id,
              new_quantity: $scope.computation.new_total
            }

            $scope.defaults = {
              units: [],
            };


            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.defaults.units = response.data; 
                $timeout(function(){
                    $('.units').selectpicker('refresh');
                });                  
              });
            }

            $timeout(function(){
              $scope.getUnits();  
            });   

            $scope.resetNewTotal = function(){
              $scope.computation.new_total = $scope.inventory.stocks;
            }

            $scope.resetNewTotal();

            $scope.computeNewTotal = function(adjustment_type, newVal){

              // Reset
              $scope.resetNewTotal();

              // Add Stock
              if (adjustment_type === 'add') {
                $scope.computation.new_total = parseFloat($scope.inventory.stocks) + parseFloat(newVal);
              }else if (adjustment_type === 'less'){
                $scope.computation.new_total = eval(parseFloat($scope.inventory.stocks) - parseFloat(newVal));
              }

            }       

            $scope.submit = function(){

              $scope.isLoading = true;

              $scope.adjustment.new_quantity = $scope.computation.new_total;

              InventoryRepository.adjustStock($scope.adjustment).then(function(response){

                swal("Successful", response.data.success.message, "success");
                
                $uibModalInstance.dismiss('close');

                if (callback) {
                  callback();
                }

                $scope.isLoading = false;
                        
              }, function(response_error){
                swal("Failed", response_error.data.error.message, "error");
                $scope.isLoading = false;
              })


            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'md'
        });
  		}

  		this.viewInventory = function(inventory, callback = false){

  		}

      this.searchAndSelectItems = function(item_lists, callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/item-list.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository){

            $scope.errors = [];
            $scope.item_lists = item_lists;

            $scope.isLoading = false;

            $scope.doSearch = function(){
                $scope.data();
            }

            $scope.pagination = {
                totalItems: 0,
                currentPage: 1,
                itemsPerPage: 10,
                maxSize: 5,
                search: '',
                objects: [],
            }

            $scope.setPage = function (pageNo) {
                $scope.pagination.currentPage = pageNo;
            };

            $scope.pageChanged = function() {
                $scope.data();
            };

            $scope.data = function(){

                $scope.isLoading = true;

                let params = {
                    page: $scope.pagination.currentPage,
                    rows: $scope.pagination.itemsPerPage,
                    search: $scope.pagination.search
                };
                
                InventoryRepository.paginate(params).then(function(response){
                    $scope.pagination.objects = response.data.rows;
                    $scope.pagination.totalItems = response.data.total;
                    $scope.isLoading = false;
                });
                
            }

            $scope.setItemsPerPage = function(num) {
                $scope.pagination.itemsPerPage = num;
                $scope.pagination.currentPage = 1; //reset to first paghe
                $scope.data();
            }

            $timeout(function(){
                $scope.data();
            });

            $scope.isInTheList = function(inventory_id){

              var selected_items = $filter('filter')($scope.item_lists, {'id': inventory_id}, true);

              var is_exist = false;

              if (selected_items.length > 0) {

                  is_exist = true;

              }

              return is_exist;
            }

            $scope.addToTheList = function(inventory){
              if (inventory.stocks <= 0) {
                swal("Error!", "This item is out of stock!", "error");
              }else{
                $scope.item_lists.push(inventory);  
              }
            }

            $scope.removeToTheList = function(inventory_id){

              let selected_items = $filter('filter')($scope.item_lists, {'id': inventory_id}, true);

              let index = $scope.item_lists.indexOf(selected_items);

              for (let i = 0; i < $scope.item_lists.length; i++) {
                  
                if (inventory_id === $scope.item_lists[i].id) {

                  $scope.item_lists.splice(i, 1);

                }

              }

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

            $scope.addItem = function(){
              InventoryService.addItem($scope.data);
            }

          },
          size: 'lg'
        });

      }

      this.addStock = function(callback = false){

        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/add-stocks.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, UnitRepository, SupplierService, InventoryService){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.inventory = [];

            $scope.stock = {};

            $scope.default = {
              suppliers: [],
              units: []
            }

            $scope.getSuppliers = function(){
              SupplierRepository.get().then(function(response){ 
                $scope.default.suppliers = response.data; 
                $timeout(function(){
                    $('#supplier').selectpicker('refresh');
                    $('#units').selectpicker('refresh');
                });                
              });
            }

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.default.units = response.data; 
                $timeout(function(){
                    $('#units').selectpicker('refresh');
                });                  
              });
            }

            $scope.getSuppliers();
            $scope.getUnits();

            $timeout(function(){
              $('.date-picker').datepicker({
                  rtl: App.isRTL(),
                  orientation: "left",
                  autoclose: true
              });
            })

            $scope.addSupplier = function(getSuppliers){
              SupplierService.addSupplier(getSuppliers);
            }

            $scope.searchItem = function(){
              InventoryService.searchAndSelectItems($scope.inventory);
            }

            $scope.submit = function(){

              let transaction = {
                stock: $scope.stock,
                inventory: $scope.inventory
              }

              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');

              InventoryRepository.addStocks(transaction).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });

            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'lg'
        });

      }

      this.addItem = function(callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/add-item.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi, $timeout, SupplierRepository, SupplierService, InventoryService, $filter, InventoryRepository, UnitRepository, UnitService, ItemRepository){

            $scope.errors = [];
            $scope.isLoading = false;

            $scope.defaults = {
              categories: [],
              brands: [],
              groups: [],
              units: [],
            };

            $scope.getUnits = function(){
              UnitRepository.get().then(function(response){
                $scope.defaults.units = response.data; 
                $timeout(function(){
                    $('.units').selectpicker('refresh');
                });                  
              });
            }

            $scope.getUnits();            

            $scope.item = {
              name: '',
              item_variations: [{
                temp_id: 1,
                base_unit_id: 0,
                base_price: 0,
                selling_price: 0,
                name: '',
                category_id: 0,
                group_id: 0,
                brand_id: 0,
              }],
            };

            $scope.addRow = function(){
              let total_rows = $scope.item.item_variations.length;
              $scope.item.item_variations.push({
                temp_id: total_rows + 1,
                base_unit_id: 0,
                base_price: 0,
                selling_price: 0,
                name: '',
                category_id: 0,
                group_id: 0,
                brand_id: 0,
              });
            }

            $scope.removeRow = function(index){

              for (let i = 0; i < $scope.item.item_variations.length; i++) {
                  
                  if (index === i) {

                      $scope.item.item_variations.splice(i, 1);

                  }

              }              
            }

            $scope.addUnit = function(){
              UnitService.add($scope.getUnits);
            }

            $scope.submit = function(){
              $scope.isLoading = true;
              blockUi.open('.modal-body', 'Processing');              
              ItemRepository.createItemAndVariation($scope.item).then(function(response){

                $scope.isLoading = false;
                blockUi.close('.modal-body');
                $uibModalInstance.dismiss('close'); 

                swal("Good job!", response.data.message, "success");

                if (callback) {
                  callback();
                }

              }, function(e){

                angular.forEach(e.data.errors, function(val, index){
                    $scope.errors.push(val[0]);
                })

                $scope.isLoading = false;
                swal("Failed!", "Kindly check the error message.", "error");
                blockUi.close('.modal-body');

              });
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }


          },
          size: 'lg'
        });
      }


      this.updateItemInformation = function(item_variation, callback = false){
        $uibModal.open({
          animation: true,
          templateUrl: BASE.API_URL + 'app/Components/Inventory/templates/update-item-variation.html',
          backdrop: 'static',
          controller: function($uibModalInstance, $scope, blockUi){

            $scope.errors = [];
            $scope.isLoading = false;
            $scope.item_variation = item_variation;

            $scope.submit = function(){

              $scope.isLoading = true;

              ItemVariationRepository.update($scope.item_variation.id, $scope.item_variation).then(function(response){

                swal("Item Update Successful", "Updating the item has been successful.", "success");
                
                $uibModalInstance.dismiss('close');

                if (callback) {
                  callback();
                }

                $scope.isLoading = false;
                        
              }, function(response_error){
                swal("Item Update Failed", "Updating the item has been failed.", "error");
                $scope.isLoading = false;
              })


            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('close');
            }

          },
          size: 'md'
        });
      }
  	}
    

})();