// angular.module('app', [ 'ui.bootstrap', 'ui.router', 'summernote', 'ngFileUpload', 'ngTagsInput', 'ngSanitize', 'User', 'vcRecaptcha', 'Campaign'])

angular.module('app', [ 'AngularVersioning', 'Interceptor', 'ng-currency', 'ui.bootstrap', 'ui.router', 'summernote', 'ngFileUpload', 'ngTagsInput', 'ngCookies', 'Inventory', 'Relationship', 'Transaction', 'Sales', 'Settings', 'Report', 'System'])

.config(function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
    $httpProvider.interceptors.push('preventTemplateCache');
    $httpProvider.interceptors.push('userInterceptor');
})

.constant('BASE', {
    
    'API_URL': '/',

	// 'API_URL': window.location.protocol + '//inventory.local/',

    // 'API_URL': 'http://jmeinventory.tk/',
    
	'ASSETS_URL': 'assets/',
    
	'API_VERSION': 'api/v1',
    
})

.run(['$rootScope', 'BASE', '$state', function ($rootScope, BASE, $state) {

}])

.directive('triggerEvent', ['$parse', '$timeout', function($parse, $timeout){
        var DELAY_TIME_BEFORE_POSTING = 10;
        return function(scope, elem, attrs) {
            var element = angular.element(elem)[0];
            var currentTimeout = null;

            element.onkeypress = function() {
                var model = $parse(attrs.runMethod);
                var poster = model(scope);
                
                if(currentTimeout) {
                    $timeout.cancel(currentTimeout)
                }
                
                currentTimeout = $timeout(function(){
                    poster();
                }, DELAY_TIME_BEFORE_POSTING)
            }
        }
}])

.service('blockUi', [function () {
    this.open = function(element, message){
        App.blockUI({
            target: element,
            boxed: true,
            message: message
        });
    }

    this.close = function(element){
        App.unblockUI(element);        
    }
}])

