angular.module('app')

.directive('selectPicker', function($parse, $timeout){
  return {
    restrict: 'A',
    priority: 1000,
    link: function (scope, element, attrs) {

      function refresh(newVal) {

        scope.$applyAsync(function () {

          element.selectpicker();
          
          element.selectpicker('refresh'); 
          
        });
      }

      attrs.$observe('spTheme', function (val) {
        $timeout(function () {
          element.data('selectpicker').$button.removeClass(function (i, c) {
            return (c.match(/(^|\s)?btn-\S+/g) || []).join(' ');
          });
          element.selectpicker('setStyle', val);
        });
      });

      $timeout(function () {
        element.selectpicker($parse(attrs.selectpicker)());
        element.selectpicker('refresh'); 
      });

      if (attrs.ngModel) {
        scope.$watch(attrs.ngModel, refresh, true);
      }

      if (attrs.ngDisabled) {
        scope.$watch(attrs.ngDisabled, refresh, true);
      }

      scope.$on('$destroy', function () {
        $timeout(function () {
          element.selectpicker('destroy');
        });
      });

      if (attrs.obj) {
        scope.$watch(attrs.obj, refresh, true);
      }
      
    }
  };
})


.directive('customSwitch', [function () {
  return {
    restrict: 'A',
    link: function (scope, iElement, iAttrs) {
      $(iElement).bootstrapSwitch();
    }
  };
}])

.directive('currency', [function () {
  return {
    restrict: 'A',
    scope: {
      val: '&'
    },
    link: function (scope, iElement, iAttrs) {
      let digits = scope.val;
      $(iElement).html(digits.toFixed().replace(/\d(?=(\d{3})+\.)/g, ','));
    }
  };
}])




.directive('customStatus', [function () {
  return {
    restrict: 'A',
    link: function (scope, iElement, iAttrs) {
      $(iElement).html('');

      // $(iElement).pulsate({
      //     color: "#36c6d3"
      // })

      if (iAttrs.status == 'active') {
        $(iElement).html('<span class="label label-success">Active</span>');
      }else if (iAttrs.status == 'inactive') {
        $(iElement).html('<span class="label label-default">Inactive</span>');
      }else if (iAttrs.status == 'yes') {
        $(iElement).html('<span class="label label-success">Default  ' + iAttrs.str + '</span>');
      }else if (iAttrs.status == 'no') {
        $(iElement).html('<span class="label label-default">Reserved ' + iAttrs.str + '</span>');
      }
    }
  };
}])

.directive('customStatusV2', ['$compile' ,function ($compile) {
  return {
    restrict: 'A',
    scope: {
      status: '='
    },
    link: function (scope, iElement, iAttrs) {

      function refreshElement() {

        var template = '';

        // $(iElement).pulsate({
        //     color: "#36c6d3"
        // })

        if (scope.status == 'active') {
          template = '<span class="label label-success">Active</span>';
        }else if (scope.status == 'inactive') {
          template = '<span class="label label-default">Inactive</span>';
        }else if (scope.status == 'yes') {
          template = '<span class="label label-success">Default  ' + scope.str + '</span>';
        }else if (scope.status == 'no') {
          template = '<span class="label label-default">Reserved ' + scope.str + '</span>';
        }


        var content = $compile(template)(scope);
        iElement.replaceWith(content);

        console.log(scope.status);

      }

      scope.$watch(scope.status, function(){
        refreshElement();
      });
    }
  };
}])

.directive('maskDate', [function () {
  return {
    restrict: 'A',
    link: function (scope, iElement, iAttrs) {
      // $().inputmask("https://", {
      //   autoUnmask: true
      // })

      Inputmask("url").mask(iElement);
    }
  };
}])

.directive('easyPieChart', [function () {
  return {
    restrict: 'EA',
    scope:{
      barcolor: '=',
    },
    link: function (scope, iElement, iAttrs) {
      // $().inputmask("https://", {
      //   autoUnmask: true
      // })

      iElement.easyPieChart({
          animate: 1000,
          size: 80,
          lineWidth: 5,
          barColor: App.getBrandColor(scope.barcolor)
      });
    }
  };
}])


.directive('multiSelect', [function () {
  return {
    restrict: 'A',
    link: function (scope, iElement, iAttrs) {
      // $().inputmask("https://", {
      //   autoUnmask: true
      // })

      scope.$watch(iAttrs.obj, refresh);

      function refresh() {
        $(iElement).multiSelect();
      }
      
      $(iElement).multiSelect();
    }
  };
}])


.directive('userStatus', ['$compile' ,function ($compile) {
  return {
    restrict: 'A',
    scope: {
      status: '@'
    },
    link: function (scope, iElement, iAttrs) {

      function refreshElement() {

        var template = '';

        // $(iElement).pulsate({
        //     color: "#36c6d3"
        // })

        if (scope.status == 'active') {
          template = '<span class="label label-success">Active</span>';
        }else if (scope.status == 'pending') {
          template = '<span class="label label-default">Pending</span>';
        }else if (scope.status == 'banned') {
          template = '<span class="label label-danger">Banned</span>';
        }else if (scope.status == 'rejected') {
          template = '<span class="label label-danger">Rejected</span>';
        }else if (scope.status == 'ended') {
          template = '<span class="label label-info">Ended</span>';
        }else if (scope.status == 'disabled') {
          template = '<span class="label label-warning">Disabled</span>';
        }else if (scope.status == 'approved') {
          template = '<span class="label label-success">Approved</span>';
        }else if (scope.status == 'declined') {
          template = '<span class="label label-default">Declined</span>';
        }else if (scope.status == 'in_review') {
          template = '<span class="label label-warning">In Review</span>';
        }else if (scope.status == 'penalized') {
          template = '<span class="label label-danger">Penalized</span>';
        }else if (scope.status == 'continued') {
          template = '<span class="label label-success">Continued</span>';
        }else if (scope.status <= 0) {
          template = '<span class="label label-warning">Out of Stock</span>';
        }else if (scope.status > 0) {
          template = '<span class="label label-success">In Stock</span>';
        }

        var content = $compile(template)(scope);
        iElement.replaceWith(content);
        console.log(scope.status);

      }

      scope.$watch(scope.status, function(){
        refreshElement();
      });
    }
  };
}])

.directive('fileType', ['$compile' ,function ($compile) {
  return {
    restrict: 'A',
    scope: {
      type: '@'
    },
    link: function (scope, iElement, iAttrs) {

      function refreshElement() {

        var template = '';

        // $(iElement).pulsate({
        //     color: "#36c6d3"
        // })

        if (scope.type == 'image') {
          template = '<span style="font-size: 15px;" class="fa fa-image"></span>';
        }else if (scope.type == 'document') {
          template = '<span style="font-size: 15px;" class="fa fa-file-text-o"></span>';
        }else if (scope.type == 'pdf') {
          template = '<span style="font-size: 15px;" class="fa fa-file-pdf-o"></span>';
        }else if (scope.type == 'video') {
          template = '<span style="font-size: 15px;" class="fa fa-file-video-o"></span>';
        }

        var content = $compile(template)(scope);
        iElement.replaceWith(content);
        console.log(scope.type);

      }

      scope.$watch(scope.type, function(){
        refreshElement();
      });
    }
  };
}])


.directive('userStatusV2', ['$compile' ,function ($compile) {
  return {
    restrict: 'A',
    scope: {
      status: '='
    },
    link: function (scope, iElement, iAttrs) {

      function refreshElement() {

        var template = '';

        // $(iElement).pulsate({
        //     color: "#36c6d3"
        // })

        if (scope.status == 'active') {
          template = '<span class="label label-success">Active</span>';
        }else if (scope.status == 'pending') {
          template = '<span class="label label-default">Pending</span>';
        }else if (scope.status == 'banned') {
          template = '<span class="label label-danger">Banned</span>';
        }else if (scope.status == 'rejected') {
          template = '<span class="label label-danger">Rejected</span>';
        }else if (scope.status == 'ended') {
          template = '<span class="label label-info">Ended</span>';
        }else if (scope.status == 'disabled') {
          template = '<span class="label label-warning">Disabled</span>';
        }

        var content = $compile(template)(scope);
        iElement.replaceWith(content);
        console.log('V2:' + scope.status);

      }

      scope.$watch(scope.status, function(newValue, oldValue){
        refreshElement();
        console.log(scope.status);
      });
    }
  };
}])


.directive('knobDial', ['$compile' ,function ($compile) {
  return {
    restrict: 'A',
    link: function (scope, iElement, iAttrs) {

      function refreshElement() {


        // $(iElement).pulsate({
        //     color: "#36c6d3"
        // })

        //knob does not support ie8 so skip it
        if (!jQuery().knob || App.isIE8()) {
            return;
        }

        // general knob
        iElement.knob({
            'thickness': 0.2,
            'tickColorizeValues': true,
            'skin': 'tron'
        }); 


      }

      
       refreshElement();
      
    }
  };
}])

.directive('customPopover', ['$compile' ,function ($compile) {
  return {
    restrict: 'A',
    link: function (scope, iElement, iAttrs) {

      function refreshElement() {


        // $(iElement).pulsate({
        //     color: "#36c6d3"
        // })

        iElement.popover();

        // close last displayed popover

        $(document).on('click.bs.popover.data-api', function(e) {
            if (lastPopedPopover) {
                lastPopedPopover.popover('hide');
            }
        });

      }

      refreshElement();
      
    }
  };
}])

.directive('myEnter', function () {

  return function(scope, element, attrs){
    element.bind('keydown keypress', function(event){
      if (event.which === 13) {
        scope.$apply(function(){
          scope.$eval(attrs.myEnter);
        });

        event.preventDefault();
      }
    });
  };
})
