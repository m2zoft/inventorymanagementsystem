<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return view('app.login');
})->name('web-login')->middleware('guest');

Route::post('authenticate', 'Auth\CustomLogin@login')->name('api-login');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function() {

    Route::get('/', function () { return view('app.dashboard'); })->name('admin-dashboard');

    Route::get('/dashboard', function () { return view('app.dashboard'); })->name('admin-dashboard');

    Route::group(['prefix' => 'sales'], function() {
        
        Route::get('/', function () { return view('app.sales.index'); })->name('admin-sales');

        Route::get('/invoices', function () { return view('app.invoice.index'); })->name('admin-sales-invoices');

    });

    Route::get('/item', function () { return view('app.item.index'); })->name('admin-item');

    Route::get('/inventory', function () { return view('app.inventory.index'); })->name('admin-inventory');

    Route::get('/supplier', function () { return view('app.supplier.index'); })->name('admin-supplier');

    Route::get('/customer', function () { return view('app.customer.index'); })->name('admin-customer');

    Route::get('/user', function () { return view('app.user.index'); })->name('admin-user');

    Route::get('/transaction', function () { return view('app.transaction.index'); })->name('admin-transaction');

    Route::group(['prefix' => 'report'], function() {

        Route::resource('sales', 'Report\Sales', [
            'only' => ['index'],
            'as' => 'report'
        ]);

        Route::resource('inventory', 'Report\Inventory', [
            'only' => ['index'],
            'as' => 'report'
        ]);

        Route::group(['prefix' => 'sales'], function() {

            Route::resource('inventory', 'Report\InventorySales', [
                'only' => ['index'],
                'as' => 'report.sales',
            ]);            

        });

        

        Route::group(['prefix' => 'revenue'], function() {
            Route::resource('net-sales', 'Report\NetSalesRevenue', [
                'only' => ['index'],
                'as' => 'revenue',
            ]);
        });
        
    });

    Route::group(['prefix' => 'setting'], function() {
        
        Route::get('/account', function () { return view('app.setting.account-setting'); })->name('admin-account-setting');

        Route::get('/category', function () { return view('app.setting.category-setting'); })->name('admin-category-setting');

        Route::get('/group', function () { return view('app.setting.group-setting'); })->name('admin-group-setting');

        Route::get('/brand', function () { return view('app.setting.brand-setting'); })->name('admin-brand-setting');

        Route::get('/unit', function () { return view('app.setting.unit-setting'); })->name('admin-unit-setting');

        Route::get('/transaction', function () { return view('app.setting.transaction-setting'); })->name('admin-transaction-setting');

        Route::get('/notification', function () { return view('app.setting.notification-setting'); })->name('admin-notification-setting');
       
    });

    Route::get('report/revenue/net-sales/export-to-excel/{date_from}/{date_to}', 'Api\Report\NetSalesRevenue@export');

    Route::get('report/sales/inventory-summary/export-to-excel/{date_from}/{date_to}', 'Api\Report\InventorySales@export');

    Route::get('report/inventory-summary/export-to-excel', 'Api\Report\Inventory@export');

    Route::get('/internal/transaction-entry', 'InternalScript\TransactionEntry@index');

    Route::get('/internal/artisan-migrate', 'InternalScript\Artisan@migrate');

    Route::get('/internal/transaction-entry-migrate', 'InternalScript\Artisan@processTransactionEntry');
});

Auth::routes();

