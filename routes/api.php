<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('manipulate/item-variation-history', 'Api\Manipulate\ItemVariationManipulation@item_variation_history');
Route::get('manipulate/transaction-entries', 'Api\Manipulate\ItemVariationManipulation@transaction_entries_for_base_price');

Route::group(['middleware' => 'auth:api'], function() {

    Route::post('item/create-item-and-variation', 'Inventory\ItemController@createItemAndVariation');

	Route::get('inventory', 'Inventory\InventoryController@index');
	Route::post('inventory/add-stocks', 'Inventory\InventoryController@addStock');
	Route::post('inventory/receive-stock', 'Inventory\InventoryController@receiveStock');
	Route::post('inventory/adjust-stock', 'Inventory\InventoryController@adjustStock');
	Route::get('inventory/{id}/view', 'Inventory\InventoryController@view');

	Route::post('inventory/update-price', 'Inventory\InventoryController@updatePrice');

	Route::get('supplier', 'Relationship\SupplierController@index');
	Route::post('supplier', 'Relationship\SupplierController@create');

	// Customers
	Route::get('customer', 'Relationship\CustomerController@paginate');
	Route::get('customer/all', 'Relationship\CustomerController@index');
	Route::post('customer', 'Relationship\CustomerController@create');

	// Sales
	Route::get('sales/paginate-sales-order', 'Sales\SalesController@paginateSalesOrder');
	Route::post('sales/create-order-transaction', 'Sales\SalesController@createOrderTransaction');

	// Transactions
	Route::get('transaction', 'Transaction\TransactionController@index');


	// Settings
	Route::post('unit', 'Inventory\UnitController@create');
	Route::get('unit/all', 'Inventory\UnitController@index');

	Route::group(['prefix' => 'item'], function() {
	    Route::resource('variation', 'Inventory\ItemVariation', [
	    	'only' => ['index', 'create', 'update', 'show'],
	    	'as'   => 'item',
	    ]);
	});

	// System
	Route::group(['prefix' => 'system'], function() {
	    Route::resource('update', 'Api\System\SystemUpdate', [
	    	'only' => ['index'],
	    	'as' => 'system',
	    ]);
	});

	Route::group(['prefix' => 'invoice'], function() {
	    Route::resource('/', 'Api\Invoice\Invoice', [
	    	'only' => ['index', 'store', 'update', 'show']
	    ]);

	    Route::resource('pay', 'Api\Invoice\Pay', [
	    	'only' => ['index', 'store', 'update', 'show']
	    ]);
	});

    

    


	// Report
	Route::group(['prefix' => 'report'], function() {
	    Route::resource('sales', 'Api\Report\Sales', [
	    	'only' => ['index'],
	    	'as' => 'report',
	    ]);

        Route::resource('inventory', 'Api\Report\Inventory', [
            'only' => ['index'],
            'as' => 'report.sales',
        ]);

        Route::get('inventory/export-to-excel/{date_from}/{date_to}', 'Api\Report\Inventory@export');

	    Route::group(['prefix' => 'sales'], function() {

            Route::resource('inventory', 'Api\Report\InventorySales', [
                'only' => ['index'],
                'as' => 'report.sales',
            ]);

            Route::get('inventory-summary/export-to-excel/{date_from}/{date_to}', 'Api\Report\InventorySales@export');

        });

	    Route::group(['prefix' => 'revenue'], function() {

	    	Route::get('net-sales/export-to-excel/{date_from}/{date_to}', 'Api\Report\NetSalesRevenue@export');

            Route::resource('net-sales', 'Api\Report\NetSalesRevenue', [
                'only' => ['index'],
                'as' => 'revenue',
            ]);
        });

	});

});
