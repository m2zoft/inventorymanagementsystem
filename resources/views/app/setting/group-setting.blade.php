@extends('layouts.app')

@section('title')
    Group Settings
@endsection

@section('setting-active')
     active open selected
@endsection

@section('group-setting-active')
     active
@endsection

@section('page-level-styles')

@endsection


@section('content')

<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Group Settings</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li class="active">Group Settings</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    
</div>


@endsection



@section('page-level-plugins')

@endsection

@section('theme-global-scripts')
    <script src="{{ asset('assets/global/scripts/app.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts')
    
@endsection
