@extends('layouts.login')

@section('title')
    Login
@endsection

@section('dashboard-active')
    
@endsection

@section('page-level-styles')
    <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css" />
@endsection


@section('content')

        <div class="logo">
            <a href="">
                <h4>Inventory Management System</h4>
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <div login></div>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="index.html" method="post">
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
        </div>
        <div class="copyright"> {{ date('Y') }} &copy; Inventory Management System by <a target="_blank" href="https://innovativesprout.com">InnovativeSprout</a></div>

@endsection



@section('page-level-plugins')
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>    
@endsection

@section('theme-global-scripts')
    <script src="{{ asset('assets/global/scripts/app.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts')
    <script src="{{ asset('assets/pages/scripts/login.min.js') }}" type="text/javascript"></script>
@endsection
