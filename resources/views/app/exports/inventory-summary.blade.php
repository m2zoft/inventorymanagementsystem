<table class="table table-condensed table-striped table-bordered table-header-fixed">
    <thead>
        <tr class="">
            <th> Item </th>
            <th style="text-align: right;"> Beginning Stock</th>
            <th style="text-align: right;"> Total Quantity Sold</th>
            <th style="text-align: right;"> Total Remaining Stock</th>
            <th style="text-align: right;"> Total Discount </th>
            <th style="text-align: right;"> Total Sales</th>
        </tr>
    </thead>
    @foreach($data['rows'] as $obj)
    <tbody>

        <tr class="{{ $obj['element_class'] }}">
            <td>{{ $obj['item_name'] }} (<span>{{ $obj['item_variation_name'] }}</span>) </td>
            <td style="text-align: right;"> {{ $obj['readable']['total_beginning_stock'] }} {{ $obj['base_unit']['name'] }}</td>
            <td style="text-align: right;"> {{ $obj['readable']['total_quantity'] }} {{ $obj['base_unit']['name'] }} </td>
            <td style="text-align: right;"> {{ $obj['readable']['total_remaining_stock'] }} {{ $obj['base_unit']['name'] }}</td>
            <td style="text-align: right;"> {{ $obj['readable']['total_discount'] }} </td>
            <td align="right"> <span class="bold">{{ $obj['readable']['total_price'] }}</span> </td>
        </tr>

    </tbody>
    @endforeach
    <tbody>
        <tr class="bg-grey-steel bg-font-grey-steel">
            <td style="text-align: right;" colspan="5"> <span class="bold">TOTAL SALES</span> </td>
            <td style="text-align: right;"><span class="bold"> {{ $data['total_sales'] }}</span></td>
        </tr>
    </tbody>
</table>
