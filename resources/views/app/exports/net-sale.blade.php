<table class="table table-condensed table-striped table-bordered table-header-fixed">
    <thead>
        <tr class="">
            <th> Date </th>
            <th> Transaction #/O.R. </th>                            
            <th> Item </th>
            <th style="text-align: right;"> Base Price </th>                            
            <th style="text-align: right;"> Selling Price </th>
            <th style="text-align: right;"> Quantity Sold</th>
            <th style="text-align: right;"> Discount </th>
            <th style="text-align: right;"> Net Sale</th>
        </tr>
    </thead>
    @foreach($data['rows'] as $sales)
    <tbody>
        @foreach($sales['entries'] as $entry)
        <tr class="{{ $sales['element_class'] }}">
            <td> {{ $sales['readable']['transaction_date'] }}</td>
            <!-- <td> <% sales.to_customer.fullname %> </td> -->
            <td> {{ $sales['transaction_number'] }} / {{ $sales['or_number'] }}</td>
            <td> {{ $entry['item_name'] }} </td>
            <td style="text-align: right;">  {{ $entry['readable']['base_price'] }} </td>
            <td style="text-align: right;">  {{ $entry['readable']['price'] }} </td>
            <td style="text-align: right;"> {{ $entry['quantity'] }} {{ $entry['base_unit']['code'] }} </td>                            
            <td style="text-align: right;">   {{ $entry['discount'] }} </td>
            <td align="right"> <span class="bold"> {{ $entry['readable']['net_sales'] }}</span> </td>
        </tr>
        @endforeach
    </tbody>
    @endforeach
    <tbody>
        <tr class="bg-grey-steel bg-font-grey-steel">
            <td style="text-align: right;" colspan="7"> <span class="bold">TOTAL NET SALE</span> </td>
            <td style="text-align: right;"><span class="bold"> {{ $data['total_net_sales'] }}</span></td>
        </tr>
    </tbody>
</table>