<table class="table table-condensed table-striped table-bordered table-header-fixed">
    <thead>
    <tr class="">
        <th> Item Name </th>
        <th> Description </th>
        <th> Unit </th>
        <th style="text-align: right;"> Quantity </th>
        <th style="text-align: right;"> Base Price </th>
        <th style="text-align: right;"> Base Price Total </th>
        <th style="text-align: right;"> Selling Price </th>
        <th style="text-align: right;"> SRP Total</th>
    </tr>
    </thead>

    @foreach($data['rows'] as $obj)
        <tbody>

        <tr class="{{ $obj['element_class'] }}">
            <td>{{ $obj['item']['name'] }}</td>
            <td>{{ $obj['name'] }}</td>
            <td>{{ $obj['base_unit']['name'] }}</td>
            <td style="text-align: right;"> {{ $obj['readable']['total_stocks'] }}</td>
            <td style="text-align: right;"> {{ $obj['base_price'] }} </td>
            <td style="text-align: right;"> {{ $obj['readable']['base_price_total'] }} </td>
            <td style="text-align: right;"> {{ $obj['selling_price'] }} </td>
            <td align="right"> <span class="bold">{{ $obj['readable']['srp_total'] }}</span> </td>
        </tr>

        </tbody>

    @endforeach
    <tbody>
    <tr class="bg-grey-steel bg-font-grey-steel">
        <td style="text-align: right;"> <span class="bold">TOTAL</span> </td>
        <td style="text-align: right;" colspan="4"></td>
        <td style="text-align: right;"><span class="bold">{{ $data['base_price_total'] }}</span></td>
        <td></td>
        <td style="text-align: right;"><span class="bold">{{ $data['srp_total'] }}</span></td>
    </tr>
    </tbody>
</table>
