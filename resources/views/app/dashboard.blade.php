@extends('layouts.app')

@section('title')
    Admin Dashboard
@endsection

@section('dashboard-active')
     active open selected
@endsection

@section('dashboard-summary-active')
     active
@endsection

@section('page-level-styles')
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
@endsection


@section('content')

<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>{{ env('APP_NAME') }} Dashboard</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li class="active">Dashboard</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row stacked">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div  style="padding:5px;"> 
                
                <div class="alert alert-success alert-md pulsate-success">
                    <span class="fa fa-check"></span>
                    System New Updates.
                </div>

            </div>

            <div system-update></div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
            <div  style="padding:5px;"> 
                
                <div class="alert alert-warning alert-md pulsate-warning">
                    <span class="fa fa-warning"></span>
                    Next System Update Not Available For This User
                </div>

            </div>

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="icon-bubbles font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Next System Updates</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#portlet_comments_1" data-toggle="tab"> Next Update </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="portlet_comments_1">

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>




@endsection



@section('page-level-plugins')
    <script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
@endsection

@section('theme-global-scripts')
    <script src="{{ asset('assets/global/scripts/app.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts')
    <script src="{{ asset('assets/pages/scripts/ui-general.js') }}" type="text/javascript"></script>
@endsection
