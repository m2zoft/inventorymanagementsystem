<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown dropdown-fw dropdown-fw-disabled @yield('dashboard-active')">
            <a href="{{ route('admin-dashboard') }}" class="text-uppercase">
                <i class="icon-home"></i> Dashboard 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('dashboard-summary-active')">
                    <a href="{{ route('admin-dashboard') }}">
                        <i class="icon-bar-chart"></i> Summary 
                    </a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled  @yield('sales-active')">
            <a href="{{ route('admin-sales') }}" class="text-uppercase">
                <i class="icon-puzzle"></i> Sales 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('sales-list-active')">
                    <a href="{{ route('admin-sales') }}">
                        <i class="icon-diamond"></i> Sales Dashboard 
                    </a>
                </li>
                <li class="@yield('sales-invoices-active')">
                    <a href="{{ route('admin-sales-invoices') }}">
                        <i class="icon-diamond"></i> Invoices
                    </a>
                </li>
            </ul>          
        </li>        
        <li class="dropdown dropdown-fw dropdown-fw-disabled  @yield('inventory-active')">
            <a href="{{ route('admin-inventory') }}" class="text-uppercase">
                <i class="icon-puzzle"></i> Inventory 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('inventory-list-active')">
                    <a href="{{ route('admin-inventory') }}">
                        <i class="icon-diamond"></i> Lists 
                    </a>
                </li>
                <li class="@yield('inventory-item-active')">
                    <a href="{{ route('admin-item') }}">
                        <i class="icon-diamond"></i> Items 
                    </a>
                </li>                
            </ul>          
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled  @yield('relationship-active')">
            <a href="{{ route('admin-supplier') }}" class="text-uppercase">
                <i class="icon-puzzle"></i> Relationships 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('supplier-active')">
                    <a href="{{ route('admin-supplier') }}">
                        <i class="icon-diamond"></i> Suppliers
                    </a>
                </li>
                <li class="@yield('customer-active')">
                    <a href="{{ route('admin-customer') }}">
                        <i class="icon-diamond"></i> Customers 
                    </a>
                </li>
                <li class="@yield('user-active')">
                    <a href="{{ route('admin-user') }}">
                        <i class="icon-diamond"></i> Users 
                    </a>
                </li>                
            </ul>
        </li>
        
        <li class="dropdown dropdown-fw dropdown-fw-disabled  @yield('transaction-active')">
            <a href="{{ route('admin-transaction') }}" class="text-uppercase">
                <i class="icon-puzzle"></i> Transactions 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('transaction-list-active')">
                    <a href="{{ route('admin-transaction') }}">
                        <i class="icon-diamond"></i> Lists 
                    </a>
                </li>
            </ul>
        </li>

        <li class="dropdown dropdown-fw dropdown-fw-disabled  @yield('report-active')">
            <a href="{{ route('report.sales.index') }}" class="text-uppercase">
                <i class="icon-puzzle"></i> Reports 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('report-sales-active')">
                    <a href="{{ route('report.sales.index') }}">
                        <i class="icon-diamond"></i> Sales Report 
                    </a>
                </li>

                <li class="@yield('net-sales-report-revenue-active')">
                    <a href="{{ route('revenue.net-sales.index') }}">
                        <i class="icon-diamond"></i> Net Sales Revenue Report 
                    </a>
                </li>

                <li class="@yield('inventory-sales-report-active')">
                    <a href="{{ route('report.sales.inventory.index') }}">
                        <i class="icon-diamond"></i> Inventory Sales Report 
                    </a>
                </li>

                <li class="@yield('inventory-report-active')">
                    <a href="{{ route('report.inventory.index') }}">
                        <i class="icon-diamond"></i> Inventory Report
                    </a>
                </li>

            </ul>
        </li>

        <li class="dropdown dropdown-fw dropdown-fw-disabled  @yield('setting-active')">
            <a href="{{ route('admin-account-setting') }}" class="text-uppercase">
                <i class="icon-settings"></i> 
            </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="@yield('account-setting-active')">
                    <a href="{{ route('admin-account-setting') }}">
                        <i class="icon-diamond"></i> Account Settings 
                    </a>
                </li>

                <li class="@yield('category-setting-active')">
                    <a href="{{ route('admin-category-setting') }}">
                        <i class="icon-diamond"></i> Category Settings 
                    </a>
                </li>

                <li class="@yield('group-setting-active')">
                    <a href="{{ route('admin-group-setting') }}">
                        <i class="icon-diamond"></i> Group Settings 
                    </a>
                </li>

                <li class="@yield('brand-setting-active')">
                    <a href="{{ route('admin-brand-setting') }}">
                        <i class="icon-diamond"></i> Brand Settings 
                    </a>
                </li>

                <li class="@yield('unit-setting-active')">
                    <a href="{{ route('admin-unit-setting') }}">
                        <i class="icon-diamond"></i> Unit Settings 
                    </a>
                </li>

                <li class="@yield('transaction-setting-active')">
                    <a href="{{ route('admin-transaction-setting') }}">
                        <i class="icon-diamond"></i> Transaction Settings 
                    </a>
                </li>

                <li class="@yield('notification-setting-active')">
                    <a href="{{ route('admin-notification-setting') }}">
                        <i class="icon-diamond"></i> Notification Settings 
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>