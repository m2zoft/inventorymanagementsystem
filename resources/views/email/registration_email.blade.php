<!DOCTYPE html>
<html>
<head>

</head>
<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">

<div id="wrapper" class="clearfix">
  <!-- preloader -->
<!--   <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div> -->
  
  <!-- Header -->

  
  <!-- Start main-content -->
  <div class="main-content">

    <div class="icon-box mb-0 p-0">

	  <a href="#" class="icon icon-gray pull-left mb-0 mr-10">
	    <i class="pe-7s-users"></i>
	  </a>

	  <h3 class="icon-box-title pt-15 mt-0 mb-40">Hi {{$data['first_name'] . ' ' . $data['last_name']}},</h3>
	  <hr>

	  <p class="text-gray">You are now successfully registered to CountryCause. Activate your account to access your user panel.</p>

	  <a class="btn btn-dark btn-sm mt-15" href="{{$data['activation_url']}}">Activate Now</a>

	</div>
    
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <footer id="footer" class="footer pb-0" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container-fluid bg-theme-colored p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="text-white font-11 m-0">Copyright &copy;2018 CountryCause. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{ asset('theme/js/custom.js') }}"></script>

</body>
</html>

