<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForSalesInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number', 255);
            $table->string('description', 255);
            $table->dateTime('invoiced_at');
            $table->dateTime('due_at');
            $table->decimal('amount');
            $table->integer('customer_id')->unsigned();
            $table->enum('status_code', ['approved', 'partial', 'paid']);
            $table->timestamps();
        });

        Schema::create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->integer('item_variation_id')->unsigned();
            $table->string('name', 255);
            $table->string('description', 255);
            $table->integer('unit_id')->unsigned();
            $table->double('quantity');
            $table->decimal('base_price');
            $table->decimal('selling_price');
            $table->decimal('total_price');
            $table->timestamps();
        });

        Schema::create('invoice_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->integer('account_id')->unsigned();
            $table->dateTime('paid_at');
            $table->decimal('amount');
            $table->string('description', 255);
            $table->timestamps();
        });

        Schema::create('invoice_totals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->string('code', 255);
            $table->string('name', 255);
            $table->decimal('amount');
            $table->integer('sort_order');
            $table->timestamps();
        });

        Schema::create('invoice_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->string('status_code', 255);
            $table->string('description', 255);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
        Schema::drop('invoice_items');
        Schema::drop('invoice_payments');
        Schema::drop('invoice_totals');
        Schema::drop('invoice_histories');
    }
}
