<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->enum('type', ['debit', 'credit'])->default('credit');
            $table->timestamps();
        });

        // Category
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
        });

        // Types
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
        });

        Schema::create('brand_manufacturers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
        });        

        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_manufacturer_id')->unsigned();
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
            $table->foreign('brand_manufacturer_id')->references('id')->on('brand_manufacturers');
        });

        // Units
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
        });                  

        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->timestamps();
        });

        Schema::create('item_variations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned();
            $table->integer('base_unit_id')->unsigned();
            $table->decimal('base_price')->default(0);
            $table->decimal('selling_price')->default(0);
            $table->decimal('price_margin')->default(0);

            $table->text('name', 255);
            $table->timestamps();

            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('base_unit_id')->references('id')->on('units');
        });

        Schema::create('item_variations_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_variation_id')->unsigned();
            $table->integer('base_unit_id')->unsigned();
            $table->decimal('base_price')->default(0);
            $table->decimal('selling_price')->default(0);
            $table->decimal('price_margin')->default(0);

            $table->timestamps();

            $table->foreign('item_variation_id')->references('id')->on('item_variations');
            $table->foreign('base_unit_id')->references('id')->on('units');
        });        

        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_variation_id')->unsigned();
            $table->decimal('stocks')->default(0);
            $table->decimal('reorder_point')->default(0);
            $table->timestamps();

            $table->foreign('item_variation_id')->references('id')->on('item_variations');
        });

        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('code', 255);
            $table->enum('status', ['active', 'banned'])->default('active');
            $table->timestamps();
        });     

        Schema::create('transaction_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
        });         

        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->timestamps();
        }); 

        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_number', 255);
            $table->integer('account_id')->unsigned();
            $table->integer('transaction_type_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('to')->unsigned();
            $table->decimal('total_debit')->default(0);
            $table->decimal('total_credit')->default(0);
            $table->decimal('total_balance')->default(0);
            $table->dateTime('transaction_date');
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        }); 

        Schema::create('transaction_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->integer('item_variation_id')->nullable()->unsigned();
            $table->integer('base_unit_id')->unsigned();
            $table->decimal('quantity')->default(0);
            $table->decimal('price')->default(0);
            $table->decimal('total_price')->default(0);
            $table->timestamps();
            $table->foreign('item_variation_id')->references('id')->on('item_variations');
        });           

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
