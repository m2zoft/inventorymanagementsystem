<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListOfUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->longText('description')->nullable();
            $table->enum('type', ['update', 'bug', 'issue'])->default('update');
            $table->enum('status', ['installed', 'fixed', 'updated'])->default('updated'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_updates');
    }
}
