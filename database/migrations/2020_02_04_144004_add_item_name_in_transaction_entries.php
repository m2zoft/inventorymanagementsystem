<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemNameInTransactionEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_entries', function (Blueprint $table) {
            $table->longText('item_name')->after('item_variation_id')->nullable();
            $table->longText('item_variation_name')->after('item_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_entries', function (Blueprint $table) {
            $table->dropColumn('item_name');
            $table->dropColumn('item_variation_name');
        });
    }
}
