/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.22-0ubuntu0.16.04.1 : Database - inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`inventory` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `inventory`;

/*Table structure for table `accounts` */

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('debit','credit') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'credit',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `accounts` */

insert  into `accounts`(`id`,`code`,`name`,`type`,`created_at`,`updated_at`) values (1,'S','Sales','credit',NULL,NULL),(2,'EX','Expense','debit',NULL,NULL);

/*Table structure for table `activations` */

DROP TABLE IF EXISTS `activations`;

CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `activations` */

/*Table structure for table `brand_manufacturers` */

DROP TABLE IF EXISTS `brand_manufacturers`;

CREATE TABLE `brand_manufacturers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `brand_manufacturers` */

/*Table structure for table `brands` */

DROP TABLE IF EXISTS `brands`;

CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand_manufacturer_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brands_brand_manufacturer_id_foreign` (`brand_manufacturer_id`),
  CONSTRAINT `brands_brand_manufacturer_id_foreign` FOREIGN KEY (`brand_manufacturer_id`) REFERENCES `brand_manufacturers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `brands` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `mobile_number` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `customers` */

insert  into `customers`(`id`,`first_name`,`last_name`,`address`,`mobile_number`,`created_at`,`updated_at`) values (1,'Glory','TwoMoonsMom',NULL,NULL,'2018-09-29 13:08:17','2018-09-29 13:08:17'),(2,'Ken','Bata',NULL,NULL,'2018-09-29 14:50:42','2018-09-29 14:50:42'),(3,'Isabel','Ramos',NULL,NULL,'2018-09-29 15:34:36','2018-09-29 15:34:36'),(4,'Be','Tabi',NULL,NULL,'2018-09-29 15:42:42','2018-09-29 15:42:42'),(5,'Jerson','Ramos',NULL,NULL,'2018-09-29 15:47:41','2018-09-29 15:47:41'),(6,'Tina','Son',NULL,NULL,'2018-09-29 16:16:50','2018-09-29 16:16:50'),(7,'Joshua','Baby',NULL,NULL,'2018-09-30 09:56:54','2018-09-30 09:56:54'),(8,'Guard','Sa Likod',NULL,NULL,'2018-09-30 09:57:02','2018-09-30 09:57:02'),(9,'Gilo','Baby',NULL,NULL,'2018-09-30 09:57:09','2018-09-30 09:57:09'),(10,'Zeny','Ramos',NULL,NULL,'2018-09-30 10:01:34','2018-09-30 10:01:34'),(11,'Kids','Likod',NULL,NULL,'2018-09-30 10:30:38','2018-09-30 10:30:38');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `groups` */

/*Table structure for table `inventories` */

DROP TABLE IF EXISTS `inventories`;

CREATE TABLE `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_variation_id` int(10) unsigned NOT NULL,
  `stocks` decimal(8,2) NOT NULL DEFAULT '0.00',
  `reorder_point` decimal(8,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inventories_item_variation_id_foreign` (`item_variation_id`),
  CONSTRAINT `inventories_item_variation_id_foreign` FOREIGN KEY (`item_variation_id`) REFERENCES `item_variations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `inventories` */

insert  into `inventories`(`id`,`item_variation_id`,`stocks`,`reorder_point`,`created_at`,`updated_at`) values (1,1,'4.00','0.00','2018-09-29 11:15:54','2018-09-30 10:01:43'),(2,2,'4.00','0.00','2018-09-29 11:16:35','2018-09-30 10:01:43'),(3,3,'5.00','0.00','2018-09-29 11:17:55','2018-09-30 10:10:25'),(4,4,'9.00','0.00','2018-09-29 11:22:47','2018-09-30 10:01:43'),(5,5,'3.00','0.00','2018-09-29 11:22:47','2018-09-29 11:22:47'),(6,6,'5.00','0.00','2018-09-29 11:35:14','2018-09-30 10:01:43'),(7,7,'4.00','0.00','2018-09-29 11:37:20','2018-09-29 11:37:20'),(8,8,'8.00','0.00','2018-09-29 11:44:31','2018-09-29 11:44:36'),(9,10,'4.00','0.00','2018-09-29 11:44:36','2018-09-29 11:44:36'),(10,11,'20.00','0.00','2018-09-29 11:50:12','2018-09-29 11:50:12'),(11,12,'21.00','0.00','2018-09-29 11:51:26','2018-09-29 13:09:33'),(12,13,'3.00','0.00','2018-09-29 11:57:29','2018-09-29 11:57:29'),(13,14,'8.00','0.00','2018-09-29 11:58:26','2018-09-29 11:58:26'),(14,15,'15.00','0.00','2018-09-29 11:59:20','2018-09-29 11:59:20'),(15,16,'2.00','0.00','2018-09-29 12:04:34','2018-09-29 12:04:34'),(16,17,'1.00','0.00','2018-09-29 12:05:15','2018-09-29 12:05:15'),(17,18,'13.00','0.00','2018-09-29 12:10:19','2018-09-29 13:25:39'),(18,19,'6.00','0.00','2018-09-29 12:10:19','2018-09-29 12:10:19'),(19,20,'2.00','0.00','2018-09-29 12:12:13','2018-09-29 12:12:13'),(20,21,'1.00','0.00','2018-09-29 12:12:13','2018-09-29 12:12:13'),(21,22,'2.00','0.00','2018-09-29 12:12:13','2018-09-29 12:12:13'),(22,23,'8.00','0.00','2018-09-29 12:13:27','2018-09-29 12:45:29'),(23,24,'5.00','0.00','2018-09-29 12:15:03','2018-09-29 12:15:03'),(24,25,'18.00','0.00','2018-09-29 12:17:32','2018-09-30 10:30:43'),(25,26,'35.00','0.00','2018-09-29 12:17:57','2018-09-30 09:58:44'),(26,27,'24.00','0.00','2018-09-29 12:23:44','2018-09-29 15:28:35'),(27,28,'28.00','0.00','2018-09-29 12:24:39','2018-09-29 16:25:56'),(28,29,'12.00','0.00','2018-09-29 12:48:15','2018-09-29 12:48:15'),(29,30,'6.00','0.00','2018-09-29 12:49:07','2018-09-29 12:49:07'),(30,31,'10.00','0.00','2018-09-29 12:50:01','2018-09-29 12:50:01'),(31,32,'5.00','0.00','2018-09-29 12:51:31','2018-09-29 13:02:30'),(32,33,'1.00','0.00','2018-09-29 12:53:35','2018-09-29 16:25:56'),(33,34,'8.00','0.00','2018-09-29 12:54:49','2018-09-29 12:54:49'),(34,35,'3.00','0.00','2018-09-29 12:58:18','2018-09-29 12:58:18'),(35,36,'3.00','0.00','2018-09-29 13:01:06','2018-09-29 13:01:06'),(36,37,'4.00','0.00','2018-09-29 13:01:06','2018-09-29 13:01:06'),(37,38,'2.00','0.00','2018-09-29 13:01:06','2018-09-29 13:01:06'),(38,39,'4.00','0.00','2018-09-29 13:01:06','2018-09-29 13:01:06'),(39,40,'2.00','0.00','2018-09-29 13:01:49','2018-09-29 13:01:49'),(40,41,'3.00','0.00','2018-09-29 13:03:53','2018-09-29 13:03:53'),(41,42,'1.00','0.00','2018-09-29 13:15:45','2018-09-29 13:15:45'),(42,43,'2.00','0.00','2018-09-29 13:42:51','2018-09-29 13:42:51'),(43,44,'1.00','0.00','2018-09-29 13:42:51','2018-09-29 13:42:51'),(44,45,'2.00','0.00','2018-09-29 13:44:16','2018-09-29 13:44:16'),(45,46,'1.00','0.00','2018-09-29 13:44:16','2018-09-29 13:44:16'),(46,47,'2.00','0.00','2018-09-29 13:46:12','2018-09-29 13:46:12'),(47,48,'2.00','0.00','2018-09-29 13:46:55','2018-09-29 13:46:55'),(48,49,'2.00','0.00','2018-09-29 13:47:49','2018-09-29 13:47:49'),(49,50,'2.00','0.00','2018-09-29 13:48:30','2018-09-29 13:48:30'),(50,51,'20.00','0.00','2018-09-29 13:50:28','2018-09-29 13:50:28'),(51,52,'17.00','0.00','2018-09-29 13:52:07','2018-09-29 13:52:07'),(52,53,'12.00','0.00','2018-09-29 13:53:03','2018-09-29 14:55:46'),(53,54,'1.00','0.00','2018-09-29 13:54:29','2018-09-29 13:54:29'),(54,55,'3.00','0.00','2018-09-29 13:54:29','2018-09-29 13:54:29'),(55,56,'8.00','0.00','2018-09-29 13:55:07','2018-09-29 13:55:07'),(56,57,'7.00','0.00','2018-09-29 13:56:12','2018-09-29 13:56:12'),(57,58,'1.00','0.00','2018-09-29 13:56:34','2018-09-29 13:56:34'),(58,59,'1.00','0.00','2018-09-29 13:56:53','2018-09-29 13:56:53'),(59,60,'2.00','0.00','2018-09-29 13:57:11','2018-09-29 13:57:11'),(60,61,'8.00','0.00','2018-09-29 13:57:55','2018-09-29 13:57:55'),(61,62,'1.00','0.00','2018-09-29 13:58:48','2018-09-29 13:58:48'),(62,63,'6.00','0.00','2018-09-29 14:01:23','2018-09-29 14:01:23'),(63,64,'21.00','0.00','2018-09-29 14:01:23','2018-09-29 14:01:23'),(64,65,'21.00','0.00','2018-09-29 14:01:23','2018-09-29 14:01:23'),(65,66,'1.00','0.00','2018-09-29 14:03:55','2018-09-29 14:03:55'),(66,67,'61.00','0.00','2018-09-29 14:03:55','2018-09-29 14:03:55'),(67,68,'2.00','0.00','2018-09-29 14:05:59','2018-09-29 14:05:59'),(68,69,'15.00','0.00','2018-09-29 14:05:59','2018-09-29 14:05:59'),(69,70,'2.00','0.00','2018-09-29 14:07:37','2018-09-29 14:07:37'),(70,71,'9.00','0.00','2018-09-29 14:07:37','2018-09-29 14:07:37'),(71,72,'2.00','0.00','2018-09-29 14:08:18','2018-09-29 14:08:18'),(72,73,'1.00','0.00','2018-09-29 14:09:11','2018-09-29 14:09:11'),(73,74,'16.00','0.00','2018-09-29 14:12:25','2018-09-29 14:12:25'),(74,75,'2.00','0.00','2018-09-29 14:12:25','2018-09-29 14:12:25'),(75,76,'2.00','0.00','2018-09-29 14:12:25','2018-09-29 14:12:25'),(76,77,'6.00','0.00','2018-09-29 14:12:25','2018-09-29 14:12:25'),(77,78,'11.00','0.00','2018-09-29 14:15:16','2018-09-29 14:15:16'),(78,79,'3.00','0.00','2018-09-29 14:15:16','2018-09-29 14:15:16'),(79,80,'1.00','0.00','2018-09-29 14:15:16','2018-09-29 14:15:16'),(80,81,'6.00','0.00','2018-09-29 14:16:02','2018-09-29 14:16:02'),(81,82,'20.00','0.00','2018-09-29 14:28:17','2018-09-30 09:59:42'),(82,83,'25.00','0.00','2018-09-29 14:28:17','2018-09-29 14:28:17'),(83,84,'25.00','0.00','2018-09-29 14:28:17','2018-09-29 14:28:17'),(84,85,'25.00','0.00','2018-09-29 14:28:17','2018-09-29 14:28:17'),(85,86,'2.00','0.00','2018-09-29 14:28:17','2018-09-29 16:25:56'),(86,87,'24.00','0.00','2018-09-29 14:28:18','2018-09-29 14:28:18'),(87,88,'19.00','0.00','2018-09-29 14:28:18','2018-09-30 10:30:43'),(88,89,'7.00','0.00','2018-09-29 14:28:18','2018-09-29 14:28:18'),(89,90,'1.00','0.00','2018-09-29 14:39:11','2018-09-29 15:34:55'),(90,91,'0.00','0.00','2018-09-29 14:39:11','2018-09-30 10:02:25'),(91,92,'0.00','0.00','2018-09-29 14:39:11','2018-09-29 15:48:07'),(92,93,'4.00','0.00','2018-09-29 14:39:11','2018-09-30 10:00:11'),(93,94,'18.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(94,95,'6.00','0.00','2018-09-29 14:45:24','2018-09-30 09:58:10'),(95,96,'5.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(96,97,'1.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(97,98,'0.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(98,99,'20.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(99,100,'20.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(100,101,'20.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(101,102,'20.00','0.00','2018-09-29 14:45:24','2018-09-29 14:45:24'),(102,103,'13.00','0.00','2018-09-29 14:47:20','2018-09-30 10:30:43'),(103,104,'21.00','0.00','2018-09-29 14:47:20','2018-09-30 10:30:43'),(104,105,'24.00','0.00','2018-09-29 14:55:23','2018-09-30 10:00:11'),(105,106,'30.00','0.00','2018-09-29 14:55:23','2018-09-29 14:55:23'),(106,107,'30.00','0.00','2018-09-29 14:55:23','2018-09-29 14:55:23'),(107,108,'2.00','0.00','2018-09-29 15:04:11','2018-09-29 15:04:11'),(108,109,'1.00','0.00','2018-09-29 15:04:11','2018-09-29 15:04:11'),(109,110,'8.00','0.00','2018-09-29 15:04:11','2018-09-29 15:44:58'),(110,111,'10.00','0.00','2018-09-29 15:06:17','2018-09-29 15:06:17'),(111,112,'2.00','0.00','2018-09-29 15:06:17','2018-09-29 15:06:17');

/*Table structure for table `item_variations` */

DROP TABLE IF EXISTS `item_variations`;

CREATE TABLE `item_variations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `base_unit_id` int(10) unsigned NOT NULL,
  `base_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `selling_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `price_margin` decimal(8,2) DEFAULT '0.00',
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `brand_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_variations_item_id_foreign` (`item_id`),
  KEY `item_variations_base_unit_id_foreign` (`base_unit_id`),
  CONSTRAINT `item_variations_base_unit_id_foreign` FOREIGN KEY (`base_unit_id`) REFERENCES `units` (`id`),
  CONSTRAINT `item_variations_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `item_variations` */

insert  into `item_variations`(`id`,`item_id`,`base_unit_id`,`base_price`,`selling_price`,`price_margin`,`name`,`created_at`,`updated_at`,`category_id`,`group_id`,`brand_id`) values (1,1,1,'4.80','7.00','0.00','Fabcon Powder - Cherry Blossom','2018-09-29 11:15:54','2018-09-29 11:25:02',NULL,NULL,NULL),(2,2,1,'4.90','7.00','0.00','Powder Detergent - Rose Garden','2018-09-29 11:16:35','2018-09-29 11:25:20',NULL,NULL,NULL),(3,3,1,'6.65','9.00','0.00','Antibac - Safeguard Protection - 20ml','2018-09-29 11:17:55','2018-09-29 11:25:51',NULL,NULL,NULL),(4,4,1,'4.63','6.00','0.00','Bareta - Blue - 400 Grams','2018-09-29 11:22:47','2018-09-29 11:26:59',NULL,NULL,NULL),(5,4,1,'4.00','6.00','0.00','Bareta - Blue Speckles - 400 Grams','2018-09-29 11:22:47','2018-09-29 11:27:44',NULL,NULL,NULL),(6,5,1,'10.00','14.00','0.00','Original - 250 ml','2018-09-29 11:35:14','2018-09-29 11:35:14',NULL,NULL,NULL),(7,6,1,'10.00','13.00','0.00','Todo Tipid Pack - 145g/40g','2018-09-29 11:37:20','2018-09-29 11:37:20',NULL,NULL,NULL),(8,7,1,'4.00','6.00','0.00','Antibac - Safeguard - 18ml','2018-09-29 11:44:31','2018-09-29 11:44:36',NULL,NULL,NULL),(10,7,1,'4.00','6.00','0.00','Lemon 20ml','2018-09-29 11:44:36','2018-09-29 11:44:36',NULL,NULL,NULL),(11,8,1,'9.40','12.00','0.00','White - Twin Pack - 50g','2018-09-29 11:50:12','2018-09-29 11:50:12',NULL,NULL,NULL),(12,9,1,'9.70','12.00','0.00','Blanca - Twin Pack - 52g','2018-09-29 11:51:26','2018-09-29 13:09:33',NULL,NULL,NULL),(13,10,1,'5.50','7.00','0.00','Shampoo - Cool Menthol - 12ml','2018-09-29 11:57:29','2018-09-29 11:57:29',NULL,NULL,NULL),(14,11,1,'6.50','7.00','0.00','Naturals - Pink - Shampoo - 13ml','2018-09-29 11:58:26','2018-09-29 11:58:26',NULL,NULL,NULL),(15,12,1,'6.50','8.00','0.00','Conditioner - Pink - 12ml','2018-09-29 11:59:20','2018-09-29 11:59:20',NULL,NULL,NULL),(16,13,1,'6.50','8.00','0.00','TwinPack - Toothpaste - Green - 10g','2018-09-29 12:04:34','2018-09-29 12:04:34',NULL,NULL,NULL),(17,14,1,'16.00','18.00','0.00','Soap - White - 60g','2018-09-29 12:05:15','2018-09-29 12:05:15',NULL,NULL,NULL),(18,15,1,'1.00','3.00','0.00','Red','2018-09-29 12:10:19','2018-09-29 13:25:39',NULL,NULL,NULL),(19,15,1,'16.80','19.00','0.00','Green','2018-09-29 12:10:19','2018-09-29 12:10:19',NULL,NULL,NULL),(20,16,1,'15.80','17.00','0.00','Karne Norte - 100g','2018-09-29 12:12:13','2018-09-29 12:12:13',NULL,NULL,NULL),(21,16,1,'12.20','14.00','0.00','Meat Loaf - 100g','2018-09-29 12:12:13','2018-09-29 12:12:13',NULL,NULL,NULL),(22,16,1,'0.00','0.00','0.00','ML Classic - 100g','2018-09-29 12:12:13','2018-09-29 12:12:13',NULL,NULL,NULL),(23,17,1,'33.75','36.00','0.00','Cooking Oil - 348ml','2018-09-29 12:13:27','2018-09-29 12:45:29',NULL,NULL,NULL),(24,18,1,'33.75','36.00','0.00','Cooking Oil - 348ml','2018-09-29 12:15:03','2018-09-29 12:15:03',NULL,NULL,NULL),(25,19,1,'1.00','3.00','0.00','Red','2018-09-29 12:17:32','2018-09-29 12:17:32',NULL,NULL,NULL),(26,20,1,'2.00','4.00','0.00','Blue','2018-09-29 12:17:57','2018-09-29 12:19:06',NULL,NULL,NULL),(27,21,1,'1.50','2.50','0.00','Blue Star','2018-09-29 12:23:44','2018-09-29 13:07:04',NULL,NULL,NULL),(28,21,1,'1.50','2.50','0.00','White Original','2018-09-29 12:24:39','2018-09-29 12:24:39',NULL,NULL,NULL),(29,22,1,'2.50','4.00','0.00','Magic Sarap','2018-09-29 12:48:15','2018-09-29 12:48:15',NULL,NULL,NULL),(30,23,1,'16.45','19.00','0.00','Orange','2018-09-29 12:49:07','2018-09-29 12:49:07',NULL,NULL,NULL),(31,24,1,'7.20','10.00','0.00','Plus King Orange','2018-09-29 12:50:01','2018-09-29 12:50:01',NULL,NULL,NULL),(32,25,1,'19.70','22.00','0.00','Classic - 25g','2018-09-29 12:51:31','2018-09-29 13:02:30',NULL,NULL,NULL),(33,24,1,'7.20','10.00','0.00','Plus King Apple','2018-09-29 12:53:35','2018-09-29 12:53:35',NULL,NULL,NULL),(34,26,1,'10.00','13.00','0.00','Suka - 350ml','2018-09-29 12:54:49','2018-09-29 12:54:49',NULL,NULL,NULL),(35,27,1,'6.30','10.00','0.00','Mami Beef - 50g','2018-09-29 12:58:18','2018-09-29 12:58:18',NULL,NULL,NULL),(36,28,1,'7.50','11.00','0.00','Beef - 50g','2018-09-29 13:01:06','2018-09-29 13:01:06',NULL,NULL,NULL),(37,28,1,'11.25','13.00','0.00','Pancit Canton - Calamansi','2018-09-29 13:01:06','2018-09-29 13:01:06',NULL,NULL,NULL),(38,28,1,'7.78','12.00','0.00','Beef Spicy','2018-09-29 13:01:06','2018-09-29 13:01:06',NULL,NULL,NULL),(39,28,1,'7.30','11.00','0.00','Chicken Mami','2018-09-29 13:01:06','2018-09-29 13:41:56',NULL,NULL,NULL),(40,29,1,'14.05','18.00','0.00','Soy Sauce - 340ml','2018-09-29 13:01:49','2018-09-29 13:01:49',NULL,NULL,NULL),(41,30,1,'16.75','20.00','0.00','Bagoong - 320ml','2018-09-29 13:03:53','2018-09-29 13:03:53',NULL,NULL,NULL),(42,23,1,'16.45','19.00','0.00','Melon','2018-09-29 13:15:45','2018-09-29 13:15:45',NULL,NULL,NULL),(43,31,1,'12.00','14.00','0.00','Green - Onion and Garlic - 25g','2018-09-29 13:42:51','2018-09-29 13:42:51',NULL,NULL,NULL),(44,31,1,'12.00','14.00','0.00','Red - Spicy - 25g','2018-09-29 13:42:51','2018-09-29 13:42:51',NULL,NULL,NULL),(45,32,1,'5.00','7.00','0.00','Green - 26g','2018-09-29 13:44:16','2018-09-29 13:44:16',NULL,NULL,NULL),(46,32,1,'5.00','7.00','0.00','Brown - 25g','2018-09-29 13:44:16','2018-09-29 13:44:16',NULL,NULL,NULL),(47,33,1,'5.00','7.00','0.00','Plain Salted - Blue - 26g','2018-09-29 13:46:12','2018-09-29 13:46:12',NULL,NULL,NULL),(48,34,1,'5.00','7.00','0.00','Cheese - White - 26g','2018-09-29 13:46:55','2018-09-29 13:46:55',NULL,NULL,NULL),(49,35,1,'5.00','7.00','0.00','Potato Fries - Yellow - 21g','2018-09-29 13:47:49','2018-09-29 13:47:49',NULL,NULL,NULL),(50,36,1,'10.00','14.00','0.00','Nova - Orange - 40g','2018-09-29 13:48:30','2018-09-29 13:48:30',NULL,NULL,NULL),(51,37,1,'1.00','2.00','0.00','Milkystick','2018-09-29 13:50:28','2018-09-29 13:50:28',NULL,NULL,NULL),(52,38,3,'2.00','3.00','0.00','Posporo','2018-09-29 13:52:07','2018-09-29 13:52:07',NULL,NULL,NULL),(53,39,1,'1.00','1.00','0.00','Milk Choco Junior','2018-09-29 13:53:03','2018-09-29 14:52:55',NULL,NULL,NULL),(54,40,1,'10.70','13.00','0.00','Sweet Chili - 90g','2018-09-29 13:54:29','2018-09-29 13:54:29',NULL,NULL,NULL),(55,40,1,'7.10','10.00','0.00','Banana Catsup - 90g','2018-09-29 13:54:29','2018-09-29 13:54:29',NULL,NULL,NULL),(56,41,1,'6.90','9.00','0.00','Milo HMALT 22g','2018-09-29 13:55:07','2018-09-29 13:55:07',NULL,NULL,NULL),(57,42,1,'1.00','2.00','0.00','Blue Magic Lollipop','2018-09-29 13:56:12','2018-09-29 13:56:12',NULL,NULL,NULL),(58,43,1,'1.00','2.00','0.00','Pintura Gum Ball','2018-09-29 13:56:34','2018-09-29 13:56:34',NULL,NULL,NULL),(59,44,1,'1.00','2.00','0.00','Chocomix','2018-09-29 13:56:53','2018-09-29 13:56:53',NULL,NULL,NULL),(60,45,1,'1.00','2.00','0.00','Classic','2018-09-29 13:57:11','2018-09-29 13:57:11',NULL,NULL,NULL),(61,46,1,'13.05','16.00','0.00','Raw Sugar - 1/4 Kilo','2018-09-29 13:57:55','2018-09-29 13:57:55',NULL,NULL,NULL),(62,47,1,'8.60','10.00','0.00','Sulit Pack - 25 + 8g','2018-09-29 13:58:48','2018-09-29 13:58:48',NULL,NULL,NULL),(63,48,1,'0.50','0.50','0.00','Durog','2018-09-29 14:01:23','2018-09-29 14:01:23',NULL,NULL,NULL),(64,48,1,'0.50','0.50','0.00','Buo','2018-09-29 14:01:23','2018-09-29 14:01:23',NULL,NULL,NULL),(65,48,1,'0.50','0.50','0.00','Crack','2018-09-29 14:01:23','2018-09-29 14:01:23',NULL,NULL,NULL),(66,49,1,'0.50','1.00','0.00','Buo','2018-09-29 14:03:55','2018-09-29 14:03:55',NULL,NULL,NULL),(67,49,1,'0.50','1.00','0.00','1 dahon','2018-09-29 14:03:55','2018-09-29 14:03:55',NULL,NULL,NULL),(68,17,1,'10.00','13.00','0.00','100ml','2018-09-29 14:05:59','2018-09-29 14:05:59',NULL,NULL,NULL),(69,17,1,'5.00','6.00','0.00','40ml','2018-09-29 14:05:59','2018-09-29 14:05:59',NULL,NULL,NULL),(70,50,1,'5.00','7.00','0.00','Vetsin - 24g','2018-09-29 14:07:37','2018-09-29 14:07:37',NULL,NULL,NULL),(71,50,1,'3.00','5.00','0.00','Vetsin - 12','2018-09-29 14:07:37','2018-09-29 14:07:37',NULL,NULL,NULL),(72,41,1,'11.00','13.00','0.00','Bearbrand - 33g','2018-09-29 14:08:18','2018-09-29 14:08:18',NULL,NULL,NULL),(73,51,1,'1.00','3.00','0.00','Rock Salt 1/4','2018-09-29 14:09:11','2018-09-29 14:09:11',NULL,NULL,NULL),(74,52,1,'2.00','4.00','0.00','Vinegar - 60ml','2018-09-29 14:12:25','2018-09-29 14:12:25',NULL,NULL,NULL),(75,52,1,'4.00','6.00','0.00','Vinegar - 100ml','2018-09-29 14:12:25','2018-09-29 14:12:25',NULL,NULL,NULL),(76,52,1,'6.00','10.00','0.00','Vinegar - 200ml','2018-09-29 14:12:25','2018-09-29 14:12:25',NULL,NULL,NULL),(77,52,1,'3.00','5.00','0.00','Soy Sauce - 60ml','2018-09-29 14:12:25','2018-09-29 14:12:25',NULL,NULL,NULL),(78,53,1,'4.00','6.00','0.00','Sinigang Sampalok Mix - 10g','2018-09-29 14:15:16','2018-09-29 14:15:16',NULL,NULL,NULL),(79,53,1,'4.00','6.00','0.00','Sinigang Isda Mix - 10g','2018-09-29 14:15:16','2018-09-29 14:15:16',NULL,NULL,NULL),(80,53,1,'4.00','6.00','0.00','Sinigang Gabi Mix - 10g','2018-09-29 14:15:16','2018-09-29 14:15:16',NULL,NULL,NULL),(81,36,1,'5.00','7.00','0.00','Skyflakes','2018-09-29 14:16:02','2018-09-29 14:16:02',NULL,NULL,NULL),(82,54,1,'0.50','1.00','0.00','Snowbear','2018-09-29 14:28:17','2018-09-29 14:28:17',NULL,NULL,NULL),(83,54,1,'0.50','1.00','0.00','Mentos','2018-09-29 14:28:17','2018-09-29 14:28:17',NULL,NULL,NULL),(84,54,1,'0.50','1.00','0.00','Dynamite Choco Mint','2018-09-29 14:28:17','2018-09-29 14:28:17',NULL,NULL,NULL),(85,54,1,'0.50','1.00','0.00','Max Lemon','2018-09-29 14:28:17','2018-09-29 14:28:17',NULL,NULL,NULL),(86,54,1,'1.00','1.00','0.00','Krim Stix','2018-09-29 14:28:17','2018-09-29 16:23:15',NULL,NULL,NULL),(87,54,1,'1.00','1.00','0.00','Panutsa','2018-09-29 14:28:17','2018-09-29 14:28:17',NULL,NULL,NULL),(88,54,1,'1.00','1.00','0.00','Pastillas - Mahaba','2018-09-29 14:28:18','2018-09-30 10:29:43',NULL,NULL,NULL),(89,54,1,'0.50','1.00','0.00','Pastillas - Maikli','2018-09-29 14:28:18','2018-09-29 14:28:18',NULL,NULL,NULL),(90,55,1,'5.00','6.00','0.00','Biscocho','2018-09-29 14:39:11','2018-09-29 14:39:11',NULL,NULL,NULL),(91,55,1,'5.00','6.00','0.00','Otap','2018-09-29 14:39:11','2018-09-29 14:39:11',NULL,NULL,NULL),(92,55,1,'5.00','6.00','0.00','Kupeng','2018-09-29 14:39:11','2018-09-29 14:39:11',NULL,NULL,NULL),(93,55,1,'4.00','5.00','0.00','Monay','2018-09-29 14:39:11','2018-09-29 14:39:11',NULL,NULL,NULL),(94,56,1,'1.00','1.00','0.00','Bangus','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(95,56,1,'1.00','2.00','0.00','Bida','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(96,56,1,'1.00','2.00','0.00','Mani','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(97,56,1,'1.00','2.00','0.00','Boy Bawang','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(98,56,1,'1.00','1.00','0.00','Vinegar Pusit','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(99,56,1,'1.00','1.00','0.00','Sweet Corn','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(100,56,1,'1.00','1.00','0.00','Onion Rings','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(101,56,1,'1.00','1.00','0.00','Patata','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(102,56,1,'1.00','2.00','0.00','Bee Sweet','2018-09-29 14:45:24','2018-09-29 14:45:24',NULL,NULL,NULL),(103,54,1,'1.00','1.00','0.00','Krim Stix - Ube','2018-09-29 14:47:20','2018-09-29 14:47:20',NULL,NULL,NULL),(104,54,1,'1.00','1.00','0.00','Gummy Candies','2018-09-29 14:47:20','2018-09-30 10:29:50',NULL,NULL,NULL),(105,57,1,'1.00','2.00','0.00','Peanut Butter in Tinapay','2018-09-29 14:55:23','2018-09-29 14:55:23',NULL,NULL,NULL),(106,57,1,'2.00','3.00','0.00','Cheeze Whiz in Tinapay','2018-09-29 14:55:23','2018-09-29 14:55:23',NULL,NULL,NULL),(107,57,1,'2.00','3.00','0.00','Lady\'s Choice in Tinapay','2018-09-29 14:55:23','2018-09-29 14:55:23',NULL,NULL,NULL),(108,58,1,'55.00','65.00','0.00','Coca-cola 1.5 Litres','2018-09-29 15:04:11','2018-09-29 15:04:11',NULL,NULL,NULL),(109,58,1,'55.00','65.00','0.00','Sprite 1.5 Litres','2018-09-29 15:04:11','2018-09-29 15:04:11',NULL,NULL,NULL),(110,58,1,'12.00','14.00','0.00','Coke Sakto','2018-09-29 15:04:11','2018-09-29 15:04:11',NULL,NULL,NULL),(111,54,1,'1.00','2.00','0.00','Ice Candy - Cola','2018-09-29 15:06:17','2018-09-29 15:06:17',NULL,NULL,NULL),(112,54,1,'1.00','2.00','0.00','Ice Candy - Yogurt','2018-09-29 15:06:17','2018-09-29 15:06:17',NULL,NULL,NULL);

/*Table structure for table `item_variations_history` */

DROP TABLE IF EXISTS `item_variations_history`;

CREATE TABLE `item_variations_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_variation_id` int(10) unsigned NOT NULL,
  `base_unit_id` int(10) unsigned NOT NULL,
  `base_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `selling_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `price_margin` decimal(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_variations_history_item_variation_id_foreign` (`item_variation_id`),
  KEY `item_variations_history_base_unit_id_foreign` (`base_unit_id`),
  CONSTRAINT `item_variations_history_base_unit_id_foreign` FOREIGN KEY (`base_unit_id`) REFERENCES `units` (`id`),
  CONSTRAINT `item_variations_history_item_variation_id_foreign` FOREIGN KEY (`item_variation_id`) REFERENCES `item_variations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `item_variations_history` */

insert  into `item_variations_history`(`id`,`item_variation_id`,`base_unit_id`,`base_price`,`selling_price`,`price_margin`,`created_at`,`updated_at`) values (1,1,1,'4.80','7.00','0.00','2018-09-29 11:25:02','2018-09-29 11:25:02'),(2,2,1,'4.90','7.00','0.00','2018-09-29 11:25:20','2018-09-29 11:25:20'),(3,3,1,'6.65','9.00','0.00','2018-09-29 11:25:51','2018-09-29 11:25:51'),(4,4,1,'4.63','6.00','0.00','2018-09-29 11:26:59','2018-09-29 11:26:59'),(5,5,1,'0.00','6.00','0.00','2018-09-29 11:27:44','2018-09-29 11:27:44'),(6,39,1,'7.30','11.00','0.00','2018-09-29 13:41:56','2018-09-29 13:41:56'),(7,53,1,'1.00','1.00','0.00','2018-09-29 14:52:55','2018-09-29 14:52:55'),(8,86,1,'1.00','1.00','0.00','2018-09-29 16:23:15','2018-09-29 16:23:15'),(9,88,1,'1.00','1.00','0.00','2018-09-30 10:29:43','2018-09-30 10:29:43'),(10,104,1,'1.00','1.00','0.00','2018-09-30 10:29:50','2018-09-30 10:29:50');

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`created_at`,`updated_at`) values (1,'Surf','2018-09-29 11:15:54','2018-09-29 11:15:54'),(2,'Calla','2018-09-29 11:16:35','2018-09-29 11:16:35'),(3,'Downy','2018-09-29 11:17:55','2018-09-29 11:17:55'),(4,'Speed','2018-09-29 11:22:47','2018-09-29 11:22:47'),(5,'Zonrox','2018-09-29 11:35:14','2018-09-29 11:35:14'),(6,'Champion','2018-09-29 11:37:20','2018-09-29 11:37:20'),(7,'Joy','2018-09-29 11:44:31','2018-09-29 11:44:31'),(8,'Great Taste','2018-09-29 11:50:12','2018-09-29 11:50:12'),(9,'Kopiko','2018-09-29 11:51:26','2018-09-29 11:51:26'),(10,'Head and Shoulders','2018-09-29 11:57:29','2018-09-29 11:57:29'),(11,'Palmolive','2018-09-29 11:58:26','2018-09-29 11:58:26'),(12,'Cream Silk','2018-09-29 11:59:20','2018-09-29 11:59:20'),(13,'Closeup','2018-09-29 12:04:34','2018-09-29 12:04:34'),(14,'Safeguard','2018-09-29 12:05:15','2018-09-29 12:05:15'),(15,'Ligo','2018-09-29 12:10:19','2018-09-29 12:10:19'),(16,'CDO','2018-09-29 12:12:13','2018-09-29 12:12:13'),(17,'Puro','2018-09-29 12:13:27','2018-09-29 12:13:27'),(18,'Spring','2018-09-29 12:15:03','2018-09-29 12:15:03'),(19,'Marvels','2018-09-29 12:17:32','2018-09-29 12:17:32'),(20,'Fortune','2018-09-29 12:17:57','2018-09-29 12:17:57'),(21,'Two Moon','2018-09-29 12:23:44','2018-09-29 12:23:44'),(22,'Maggi','2018-09-29 12:48:15','2018-09-29 12:48:15'),(23,'Tang','2018-09-29 12:49:07','2018-09-29 12:49:07'),(24,'Zesto','2018-09-29 12:50:01','2018-09-29 12:50:01'),(25,'Nescafe','2018-09-29 12:51:31','2018-09-29 12:51:31'),(26,'Mang Lito','2018-09-29 12:54:49','2018-09-29 12:54:49'),(27,'Payless','2018-09-29 12:58:18','2018-09-29 12:58:18'),(28,'Lucky Me','2018-09-29 13:01:06','2018-09-29 13:01:06'),(29,'Select','2018-09-29 13:01:49','2018-09-29 13:01:49'),(30,'MM','2018-09-29 13:03:53','2018-09-29 13:03:53'),(31,'Vcut','2018-09-29 13:42:51','2018-09-29 13:42:51'),(32,'Mang Juan','2018-09-29 13:44:16','2018-09-29 13:44:16'),(33,'Martys Cracklings','2018-09-29 13:46:12','2018-09-29 13:46:12'),(34,'Clover','2018-09-29 13:46:55','2018-09-29 13:46:55'),(35,'Oishi','2018-09-29 13:47:49','2018-09-29 13:47:49'),(36,'Jack and Jill','2018-09-29 13:48:30','2018-09-29 13:48:30'),(37,'Pran','2018-09-29 13:50:28','2018-09-29 13:50:28'),(38,'Commando','2018-09-29 13:52:07','2018-09-29 13:52:07'),(39,'Hany','2018-09-29 13:53:03','2018-09-29 13:53:03'),(40,'UFC','2018-09-29 13:54:29','2018-09-29 13:54:29'),(41,'Nestle','2018-09-29 13:55:07','2018-09-29 13:55:07'),(42,'Lipps','2018-09-29 13:56:12','2018-09-29 13:56:12'),(43,'Columbias','2018-09-29 13:56:34','2018-09-29 13:56:34'),(44,'Chubby','2018-09-29 13:56:53','2018-09-29 13:56:53'),(45,'XO','2018-09-29 13:57:10','2018-09-29 13:57:10'),(46,'Elizabeth','2018-09-29 13:57:55','2018-09-29 13:57:55'),(47,'Alaska','2018-09-29 13:58:48','2018-09-29 13:58:48'),(48,'Paminta','2018-09-29 14:01:23','2018-09-29 14:01:23'),(49,'Bayleaf','2018-09-29 14:03:55','2018-09-29 14:03:55'),(50,'Ajinomoto','2018-09-29 14:07:37','2018-09-29 14:07:37'),(51,'Salt','2018-09-29 14:09:11','2018-09-29 14:09:11'),(52,'Datu Puti','2018-09-29 14:12:25','2018-09-29 14:12:25'),(53,'Knorr','2018-09-29 14:15:16','2018-09-29 14:15:16'),(54,'Candies','2018-09-29 14:28:17','2018-09-29 14:28:17'),(55,'Tinapay','2018-09-29 14:39:11','2018-09-29 14:39:11'),(56,'Chichirya','2018-09-29 14:45:24','2018-09-29 14:45:24'),(57,'Palaman','2018-09-29 14:55:23','2018-09-29 14:55:23'),(58,'Soda','2018-09-29 15:04:11','2018-09-29 15:04:11');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_07_02_230147_migration_cartalyst_sentinel',1),(2,'2018_09_04_105437_create_inventory_tables',1),(3,'2018_09_14_121505_modify_tables',2),(4,'2018_09_14_143505_modify_transaction_table',3),(5,'2018_09_16_152356_create_table_for_customers',4),(6,'2018_09_16_233003_add_discount_column_in_transaction_entry_table',5);

/*Table structure for table `persistences` */

DROP TABLE IF EXISTS `persistences`;

CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `persistences` */

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reminders` */

/*Table structure for table `role_users` */

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_users` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

/*Table structure for table `statuses` */

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `statuses` */

/*Table structure for table `suppliers` */

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','banned') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `suppliers` */

/*Table structure for table `throttle` */

DROP TABLE IF EXISTS `throttle`;

CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `throttle` */

/*Table structure for table `transaction_entries` */

DROP TABLE IF EXISTS `transaction_entries`;

CREATE TABLE `transaction_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL,
  `item_variation_id` int(10) unsigned DEFAULT NULL,
  `base_unit_id` int(10) unsigned NOT NULL,
  `quantity` decimal(8,2) NOT NULL DEFAULT '0.00',
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `discount` decimal(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `transaction_entries_item_variation_id_foreign` (`item_variation_id`),
  CONSTRAINT `transaction_entries_item_variation_id_foreign` FOREIGN KEY (`item_variation_id`) REFERENCES `item_variations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `transaction_entries` */

insert  into `transaction_entries`(`id`,`transaction_id`,`item_variation_id`,`base_unit_id`,`quantity`,`price`,`total_price`,`created_at`,`updated_at`,`discount`) values (1,1,27,1,'6.00','2.50','15.00','2018-09-29 13:10:08','2018-09-29 13:10:08','0.00'),(2,2,93,1,'1.00','5.00','5.00','2018-09-29 14:55:46','2018-09-29 14:55:46','0.00'),(3,2,33,1,'1.00','10.00','10.00','2018-09-29 14:55:46','2018-09-29 14:55:46','0.00'),(4,2,53,1,'3.00','1.00','3.00','2018-09-29 14:55:46','2018-09-29 14:55:46','0.00'),(5,2,105,1,'1.00','2.00','2.00','2018-09-29 14:55:46','2018-09-29 14:55:46','0.00'),(6,3,27,1,'10.00','2.50','25.00','2018-09-29 15:28:35','2018-09-29 15:28:35','0.00'),(7,4,90,1,'1.00','6.00','6.00','2018-09-29 15:34:55','2018-09-29 15:34:55','0.00'),(8,4,92,1,'1.00','6.00','6.00','2018-09-29 15:34:55','2018-09-29 15:34:55','0.00'),(9,5,110,1,'4.00','14.00','56.00','2018-09-29 15:44:58','2018-09-29 15:44:58','0.00'),(10,6,92,1,'1.00','6.00','6.00','2018-09-29 15:48:07','2018-09-29 15:48:07','0.00'),(11,7,28,1,'12.00','2.50','30.00','2018-09-29 16:25:56','2018-09-29 16:25:56','0.00'),(12,7,33,1,'1.00','10.00','10.00','2018-09-29 16:25:56','2018-09-29 16:25:56','0.00'),(13,7,103,1,'10.00','1.00','10.00','2018-09-29 16:25:56','2018-09-29 16:25:56','0.00'),(14,7,86,1,'5.00','1.00','5.00','2018-09-29 16:25:56','2018-09-29 16:25:56','0.00'),(15,8,105,1,'1.00','2.00','2.00','2018-09-30 09:58:10','2018-09-30 09:58:10','0.00'),(16,8,93,1,'1.00','5.00','5.00','2018-09-30 09:58:10','2018-09-30 09:58:10','0.00'),(17,8,95,1,'2.00','2.00','4.00','2018-09-30 09:58:10','2018-09-30 09:58:10','0.00'),(18,9,26,1,'2.00','4.00','8.00','2018-09-30 09:58:44','2018-09-30 09:58:44','0.00'),(19,9,25,1,'1.00','3.00','3.00','2018-09-30 09:58:44','2018-09-30 09:58:44','0.00'),(20,10,93,1,'3.00','5.00','15.00','2018-09-30 09:59:42','2018-09-30 09:59:42','0.00'),(21,10,105,1,'3.00','2.00','6.00','2018-09-30 09:59:42','2018-09-30 09:59:42','0.00'),(22,10,91,1,'1.00','6.00','6.00','2018-09-30 09:59:42','2018-09-30 09:59:42','0.00'),(23,10,82,1,'5.00','1.00','5.00','2018-09-30 09:59:42','2018-09-30 09:59:42','0.00'),(24,11,93,1,'1.00','5.00','5.00','2018-09-30 10:00:11','2018-09-30 10:00:11','0.00'),(25,11,105,1,'1.00','2.00','2.00','2018-09-30 10:00:11','2018-09-30 10:00:11','0.00'),(26,12,1,1,'2.00','7.00','14.00','2018-09-30 10:01:43','2018-09-30 10:01:43','0.00'),(27,12,2,1,'2.00','7.00','14.00','2018-09-30 10:01:43','2018-09-30 10:01:43','0.00'),(28,12,4,1,'2.00','6.00','12.00','2018-09-30 10:01:43','2018-09-30 10:01:43','0.00'),(29,12,6,1,'2.00','14.00','28.00','2018-09-30 10:01:43','2018-09-30 10:01:43','0.00'),(30,13,91,1,'1.00','6.00','6.00','2018-09-30 10:02:25','2018-09-30 10:02:25','0.00'),(31,14,3,1,'1.00','9.00','9.00','2018-09-30 10:10:25','2018-09-30 10:10:25','0.00'),(32,15,25,1,'4.00','3.00','12.00','2018-09-30 10:30:43','2018-09-30 10:30:43','0.00'),(33,15,103,1,'2.00','1.00','2.00','2018-09-30 10:30:43','2018-09-30 10:30:43','0.00'),(34,15,88,1,'3.00','1.00','3.00','2018-09-30 10:30:43','2018-09-30 10:30:43','0.00'),(35,15,104,1,'3.00','1.00','3.00','2018-09-30 10:30:43','2018-09-30 10:30:43','0.00');

/*Table structure for table `transaction_types` */

DROP TABLE IF EXISTS `transaction_types`;

CREATE TABLE `transaction_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `transaction_types` */

insert  into `transaction_types`(`id`,`code`,`name`,`created_at`,`updated_at`) values (1,'MIG','Migrate',NULL,NULL),(2,'O','Order',NULL,NULL),(3,'R','Return',NULL,NULL),(4,'SA','Stock Adjustment',NULL,NULL),(5,'RE','Received',NULL,NULL),(6,'D','Discounts',NULL,NULL);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `or_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `transaction_type_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `to` int(10) unsigned DEFAULT NULL,
  `from` int(10) unsigned DEFAULT NULL,
  `total_debit` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_credit` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_balance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `transaction_date` date NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `transactions` */

insert  into `transactions`(`id`,`or_number`,`transaction_number`,`account_id`,`transaction_type_id`,`status_id`,`to`,`from`,`total_debit`,`total_credit`,`total_balance`,`transaction_date`,`created_by`,`created_at`,`updated_at`) values (1,'1','180929011008',1,1,NULL,1,NULL,'0.00','15.00','15.00','2018-09-29',NULL,'2018-09-29 13:10:08','2018-09-29 13:10:08'),(2,'2','180929025546',1,1,NULL,2,NULL,'0.00','20.00','20.00','2018-09-29',NULL,'2018-09-29 14:55:46','2018-09-29 14:55:46'),(3,'3','180929032835',1,1,NULL,1,NULL,'0.00','25.00','25.00','2018-09-29',NULL,'2018-09-29 15:28:35','2018-09-29 15:28:35'),(4,'4','180929033455',1,1,NULL,3,NULL,'0.00','12.00','12.00','2018-09-29',NULL,'2018-09-29 15:34:55','2018-09-29 15:34:55'),(5,'5','180929034458',1,1,NULL,4,NULL,'0.00','56.00','56.00','2018-09-29',NULL,'2018-09-29 15:44:58','2018-09-29 15:44:58'),(6,'6','180929034807',1,1,NULL,5,NULL,'0.00','6.00','6.00','2018-09-29',NULL,'2018-09-29 15:48:07','2018-09-29 15:48:07'),(7,'7','180929042556',1,1,NULL,6,NULL,'0.00','55.00','55.00','2018-09-29',NULL,'2018-09-29 16:25:56','2018-09-29 16:25:56'),(8,'10','180930095810',1,1,NULL,7,NULL,'0.00','11.00','11.00','2018-09-30',NULL,'2018-09-30 09:58:10','2018-09-30 09:58:10'),(9,'11','180930095844',1,1,NULL,8,NULL,'0.00','11.00','11.00','2018-09-30',NULL,'2018-09-30 09:58:44','2018-09-30 09:58:44'),(10,'12','180930095942',1,1,NULL,9,NULL,'0.00','32.00','32.00','2018-09-30',NULL,'2018-09-30 09:59:42','2018-09-30 09:59:42'),(11,'12','180930100011',1,1,NULL,3,NULL,'0.00','7.00','7.00','2018-09-30',NULL,'2018-09-30 10:00:11','2018-09-30 10:00:11'),(12,'13','180930100143',1,1,NULL,10,NULL,'0.00','68.00','68.00','2018-09-30',NULL,'2018-09-30 10:01:43','2018-09-30 10:01:43'),(13,'14','180930100225',1,1,NULL,10,NULL,'0.00','6.00','6.00','2018-09-30',NULL,'2018-09-30 10:02:25','2018-09-30 10:02:25'),(14,'15','180930101025',1,1,NULL,10,NULL,'0.00','9.00','9.00','2018-09-30',NULL,'2018-09-30 10:10:25','2018-09-30 10:10:25'),(15,'15','180930103043',1,1,NULL,11,NULL,'0.00','20.00','20.00','2018-09-30',NULL,'2018-09-30 10:30:43','2018-09-30 10:30:43');

/*Table structure for table `units` */

DROP TABLE IF EXISTS `units`;

CREATE TABLE `units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `units` */

insert  into `units`(`id`,`code`,`name`,`created_at`,`updated_at`) values (1,'pc','Piece',NULL,NULL),(2,'roll','Roll','2018-09-27 00:28:37','2018-09-27 00:28:37'),(3,'box','Box','2018-09-29 13:51:28','2018-09-29 13:51:28');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
